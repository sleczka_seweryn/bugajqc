package com.bugaj.qc.app.provider.room

import androidx.room.Embedded
import androidx.room.Relation

class ReportWithImages constructor(
    @Embedded val report: ReportEntity,
    @Relation(
        parentColumn = "id",
        entityColumn = "reportId",
        entity = ImageEntity::class
    ) val images: List<ImageEntity>
)