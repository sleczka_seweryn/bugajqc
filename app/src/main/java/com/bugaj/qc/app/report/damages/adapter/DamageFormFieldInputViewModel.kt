package com.bugaj.qc.app.report.damages.adapter

import android.content.Intent
import androidx.lifecycle.*
import com.bugaj.qc.app.application.arch.BaseViewModel
import com.bugaj.qc.app.application.navigation.DialogResultAction
import com.bugaj.qc.app.application.navigation.DismissDialogAction
import com.bugaj.qc.app.application.navigation.RequestCodes
import com.bugaj.qc.app.report.damages.navigation.DamagesFormArgs
import dagger.hilt.android.lifecycle.HiltViewModel
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*
import javax.inject.Inject

@HiltViewModel
class DamageFormFieldInputViewModel @Inject constructor(
    private val savedStateHandle: SavedStateHandle
) : BaseViewModel() {

    private val fieldId: String = savedStateHandle[DamagesFormArgs.ARG_FIELD_ID] ?: throw java.lang.IllegalStateException("ARG_FIELD_ID not found")

    val title: LiveData<String> = MutableLiveData<String>().apply {
        value = savedStateHandle[DamagesFormArgs.ARG_FIELD_LABEL]  ?: throw java.lang.IllegalStateException("ARG_FIELD_LABEL not found")
    }

    val inputValue = MutableLiveData<String>().apply {
        val floatValue: Float? = savedStateHandle[DamagesFormArgs.ARG_ACTUAL_VALUE]
        floatValue?.let {
            val decimalFormat = DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH))
            decimalFormat.maximumFractionDigits = 1
            value =  decimalFormat.format(it).replace('.',',')
        }
    }

    val error = MutableLiveData<String>().apply { value =  null }

    init {
        hideLoader()
    }

    fun onClickSave() {
        showLoader()
        navigate(DialogResultAction(
            requestCode = RequestCodes.EDIT_DAMAGE_FIELD,
            resultIntent = createResult()
        ))
    }

    private fun createResult(): Intent {
        return Intent().apply {
            putExtra(DamagesFormArgs.ARG_FIELD_ID, fieldId)
            putExtra(DamagesFormArgs.ARG_ACTUAL_VALUE, inputValue.value?.replace(',','.')?.toFloatOrNull())
        }
    }


    fun onClickCancel() {
        showLoader()
        navigate(DismissDialogAction())
    }

}