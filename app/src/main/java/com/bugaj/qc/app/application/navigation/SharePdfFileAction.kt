package com.bugaj.qc.app.application.navigation

import android.content.Context
import android.content.Intent
import androidx.core.content.FileProvider
import com.bugaj.qc.app.application.files.FileProviderConst
import java.io.File

class SharePdfFileAction constructor (
    private val file: File
) : StartActivityAction(closeParent = false) {

    override fun createIntent(context: Context) = Intent().apply {

        val uri = FileProvider.getUriForFile(context, FileProviderConst.AUTHORITY, file)

        action = Intent.ACTION_SEND
        type = "application/pdf"
        putExtra(Intent.EXTRA_STREAM, uri)
        addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
    }

}