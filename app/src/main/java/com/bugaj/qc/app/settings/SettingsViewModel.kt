package com.bugaj.qc.app.settings

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import com.bugaj.qc.app.R
import com.bugaj.qc.app.admin.navigation.DisplayAdminAuthAction
import com.bugaj.qc.app.application.arch.BaseViewModel
import com.bugaj.qc.app.application.common.StringRepository
import com.bugaj.qc.app.application.navigation.FinishActivityAction
import com.bugaj.qc.app.service.SettingsRepository
import com.bugaj.qc.app.settings.edit.navigation.DisplayEditSettingsAction
import com.bugaj.qc.app.settings.host.navigation.DisplayHostSettingsAction
import com.bugaj.qc.app.settings.maintenance.navigation.DisplayMaintenanceReportAction
import com.bugaj.qc.app.settings.model.ImageResolutionTypes
import com.bugaj.qc.app.settings.model.Settings
import com.bugaj.qc.app.settings.option.change.navigation.DisplayOptionChangeAction
import com.bugaj.qc.app.settings.switchable.settings.navigation.DisplaySwitchSettingsAction
import com.bugaj.qc.app.templates.navigation.DisplayTemplatesListAction
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SettingsViewModel @Inject constructor(
    private val strings: StringRepository,
    private val settingsRepository: SettingsRepository
) : BaseViewModel() {

    val imgResolutionLabel: LiveData<String> = MutableLiveData<String>().apply { value = strings[R.string.setting_item_img_resolution_label] }
    val imgResolutionValue: LiveData<String> = settingsRepository.observeSettingValue(Settings.IMAGE_RESOLUTION.name).map {
        strings[ImageResolutionTypes.valueOf(it).nameResId]
    }

    val hostSettingsLabel: LiveData<String> = MutableLiveData<String>().apply { value = strings[R.string.setting_item_host_label] }
    val hostSettingsValue: LiveData<String> = settingsRepository.observeSettingValue(Settings.SERVER_HOST.name)

    val portSettingsLabel: LiveData<String> = MutableLiveData<String>().apply { value = strings[R.string.setting_item_port_label] }
    val portSettingsValue: LiveData<String> = settingsRepository.observeSettingValue(Settings.SERVER_PORT.name)

    val usernameSettingsLabel: LiveData<String> = MutableLiveData<String>().apply { value = strings[R.string.setting_item_username_label] }
    val usernameSettingsValue: LiveData<String> = settingsRepository.observeSettingValue(Settings.FTP_USERNAME.name)

    val passwordSettingsLabel: LiveData<String> = MutableLiveData<String>().apply { value = strings[R.string.setting_item_password_label] }
    val passwordSettingsValue: LiveData<String> = MutableLiveData<String>().apply { value = "********" }

    val templatesSettingsLabel: LiveData<String> = MutableLiveData<String>().apply { value = strings[R.string.setting_item_templates_label] }
    val templatesSettingsValue: LiveData<String> = MutableLiveData<String>().apply { value = strings[R.string.setting_item_templates_value] }

    val maintenanceSettingsLabel: LiveData<String> = MutableLiveData<String>().apply { value = strings[R.string.delete_old_reports_setting] }
    val maintenanceSettingsValue: LiveData<String> = MutableLiveData<String>().apply { value = strings[R.string.delete_old_reports_setting_value] }

    val saveImagesSettingsLabel: LiveData<String> = MutableLiveData<String>().apply { value = strings[R.string.setting_item_save_images_in_gallery] }
    val saveImagesSettingsValue: LiveData<String> = settingsRepository.observeSettingValue(Settings.STORE_IMAGES_IN_GALLERY.name).map {
        return@map when (it) {
            "TRUE" -> strings[R.string.settings_state_enabled]
            else -> strings[R.string.settings_state_disabled]
        }
    }

    fun onClickImgResolutionSettings() {
        navigate(DisplayOptionChangeAction(
            settingType = Settings.IMAGE_RESOLUTION
        ))
    }

    fun onClickHostSettings() {
        navigate(DisplayAdminAuthAction(
            redirectAction = DisplayHostSettingsAction()
        ))
    }

    fun onClickPortSettings() {
        navigate(DisplayAdminAuthAction(
            redirectAction = DisplayEditSettingsAction(
                settingType = Settings.SERVER_PORT
            )
        ))
    }

    fun onClickUsernameSettings() {
        navigate(DisplayAdminAuthAction(
            redirectAction = DisplayEditSettingsAction(
                settingType = Settings.FTP_USERNAME
            )
        ))
    }

    fun onClickPasswordSettings() {
        navigate(DisplayAdminAuthAction(
            redirectAction = DisplayEditSettingsAction(
                settingType = Settings.FTP_PASSWORD
            )
        ))
    }

    fun onClickTemplatesSettings() {
        navigate(DisplayAdminAuthAction(
            redirectAction = DisplayTemplatesListAction()
        ))
    }

    fun onClickMaintenanceSettings() {
        navigate(DisplayMaintenanceReportAction())
    }

    fun onClickSaveImagesSettings() {
        navigate(DisplaySwitchSettingsAction(
            settingType = Settings.STORE_IMAGES_IN_GALLERY
        ))
    }

    fun onClickBack() {
        navigate(FinishActivityAction())
    }

}