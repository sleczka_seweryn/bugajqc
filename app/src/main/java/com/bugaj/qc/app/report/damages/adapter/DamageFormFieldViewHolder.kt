package com.bugaj.qc.app.report.damages.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.bugaj.qc.app.R
import com.bugaj.qc.app.databinding.DamageFormFieldBinding
import com.bugaj.qc.app.report.damages.DamagesFormViewModel

class DamageFormFieldViewHolder private constructor(
    val binding: DamageFormFieldBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(viewModel: DamagesFormViewModel, item: DamageFormField) {
        binding.viewModel = viewModel
        binding.item = item

        val context = binding.root.context
        val lifecycleOwner = binding.root.context as LifecycleOwner

        binding.lifecycleOwner = lifecycleOwner
        item.getValueLiveData().observe(lifecycleOwner, Observer { value ->
            if(value == null) {
                binding.button.text = context.getString(R.string.add)
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    binding.button.backgroundTintList = (ContextCompat.getColorStateList(context, R.color.drawnstone_grey_100))
                }
            } else {
                binding.button.text = viewModel.formatDamageValue(value)
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    binding.button.backgroundTintList = (ContextCompat.getColorStateList(context, R.color.bugaj_green))
                }
            }
        })

        binding.executePendingBindings()
    }

    companion object {

        fun from(parent: ViewGroup): DamageFormFieldViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = DamageFormFieldBinding.inflate(layoutInflater, parent, false)
            return DamageFormFieldViewHolder(binding)
        }

    }

}