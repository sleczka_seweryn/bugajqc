package com.bugaj.qc.app.templates.list.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bugaj.qc.app.databinding.TemplateListItemBinding
import com.bugaj.qc.app.templates.list.TemplatesListViewModel

class TemplateViewHolder private constructor(
    val binding: TemplateListItemBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(viewModel: TemplatesListViewModel, item: TemplateItem) {
        val context = binding.root.context
        binding.viewModel = viewModel
        binding.item = item
        binding.executePendingBindings()
    }

    companion object {

        fun from(parent: ViewGroup): TemplateViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = TemplateListItemBinding.inflate(layoutInflater, parent, false)
            return TemplateViewHolder(binding)
        }

    }

}