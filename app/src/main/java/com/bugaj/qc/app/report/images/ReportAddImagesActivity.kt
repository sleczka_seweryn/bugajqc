package com.bugaj.qc.app.report.images

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.bugaj.qc.app.R
import com.bugaj.qc.app.application.arch.PhotoCaptureActivity
import com.bugaj.qc.app.application.navigation.ActivityNavigateManager
import com.bugaj.qc.app.application.navigation.DialogResultHandler
import com.bugaj.qc.app.databinding.ReportAddImagesActivityBinding
import com.bugaj.qc.app.loading.navigation.DismissLoadingDialogAction
import com.bugaj.qc.app.loading.navigation.DisplayLoadingDialogAction
import com.bugaj.qc.app.provider.room.ImageEntity
import com.bugaj.qc.app.report.details.images.ImageDecoder
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ReportAddImagesActivity : PhotoCaptureActivity() {

    private lateinit var binding: ReportAddImagesActivityBinding

    private val viewModel by viewModels<ReportAddImagesViewModel>()

    private lateinit var navigateManager: ActivityNavigateManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(
            this, R.layout.report_add_images_activity
        )
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        navigateManager = ActivityNavigateManager(this)

        viewModel.navigateAction.observe(this) {
            it?.accept(navigateManager)
        }

        viewModel.images.observe(this, Observer { images ->
            updateImages(images)
        })

        binding.addPhotoButton.setOnClickListener {
            onClickAddPhoto()
        }

        binding.pickImageButton.setOnClickListener {
            onClickPickFromGallery()
        }

        viewModel.isLoading.observe(this, Observer {
            if (it == true) {
                viewModel.navigate(DisplayLoadingDialogAction())
            } else {
                viewModel.navigate(DismissLoadingDialogAction())
            }
        })
    }

    override fun onSaveImageEntity(imageEntity: ImageEntity) {
        viewModel.onSaveImageEntity(imageEntity)
    }

    override fun getReportId(): String = viewModel.reportId

    private fun updateImages(images: List<ImageEntity>) {

        val decoder = ImageDecoder(baseContext)

        if(images.isEmpty()) {
            binding.imageView1Layout.visibility = View.GONE
            binding.imageView2Layout.visibility = View.GONE
            binding.imageView3Layout.visibility = View.GONE
            binding.imageView4Layout.visibility = View.GONE
        }
        if(images.size == 1) {
            binding.imageView1Layout.visibility = View.VISIBLE
            decoder.loadImageForView(binding.imageView1, images[0])
            binding.imageView2Layout.visibility = View.INVISIBLE
            binding.imageView3Layout.visibility = View.GONE
            binding.imageView4Layout.visibility = View.GONE
        }
        if(images.size == 2) {
            binding.imageView1Layout.visibility = View.VISIBLE
            decoder.loadImageForView(binding.imageView1, images[0])
            binding.imageView2Layout.visibility = View.VISIBLE
            decoder.loadImageForView(binding.imageView2, images[1])
            binding.imageView3Layout.visibility = View.GONE
            binding.imageView4Layout.visibility = View.GONE
        }
        if(images.size == 3) {
            binding.imageView1Layout.visibility = View.VISIBLE
            decoder.loadImageForView(binding.imageView1, images[0])
            binding.imageView2Layout.visibility = View.VISIBLE
            decoder.loadImageForView(binding.imageView2, images[1])
            binding.imageView3Layout.visibility = View.VISIBLE
            decoder.loadImageForView(binding.imageView3, images[2])
            binding.imageView4Layout.visibility = View.INVISIBLE
        }
        if(images.size >= 4) {
            binding.imageView1Layout.visibility = View.VISIBLE
            decoder.loadImageForView(binding.imageView1, images[0])
            binding.imageView2Layout.visibility = View.VISIBLE
            decoder.loadImageForView(binding.imageView2, images[1])
            binding.imageView3Layout.visibility = View.VISIBLE
            decoder.loadImageForView(binding.imageView3, images[2])
            binding.imageView4Layout.visibility = View.VISIBLE
            decoder.loadImageForView(binding.imageView4, images[3])
        }
    }

    override fun onBackPressed() {
        viewModel.onClickBackButton()
    }

}