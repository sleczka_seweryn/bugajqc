package com.bugaj.qc.app.application.navigation

import android.content.Intent

interface DialogResultHandler {

    fun onDialogResult(requestCode: Int, data: Intent)

}