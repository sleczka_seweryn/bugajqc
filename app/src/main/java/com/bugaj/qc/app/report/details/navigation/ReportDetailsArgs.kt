package com.bugaj.qc.app.report.details.navigation

object ReportDetailsArgs {

    const val ARG_REPORT_ID: String = "reportId"

}