package com.bugaj.qc.app.application.validation

import com.bugaj.qc.app.R
import com.bugaj.qc.app.application.common.StringRepository

class IntegerValidator constructor (
    private val validator: (Validator<String>)? = null
) : Validator<String> {

    override fun validate(strings: StringRepository, value: String?): String? {
        val numeric = value?.matches("\\d+".toRegex()) ?: false
        if(numeric == false) {
            return strings[R.string.error_invalid_value]
        } else {
            return validator?.validate(strings, value)
        }
    }
}