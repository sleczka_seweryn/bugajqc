package com.bugaj.qc.app.report.document.navigation

import android.content.Context
import android.os.Bundle
import com.bugaj.qc.app.application.navigation.DisplayDialogAction
import com.bugaj.qc.app.report.document.CreateDocumentDialog

class DisplayCreateDocumentDialogAction constructor (
    private val reportId: String
) : DisplayDialogAction() {

    override fun createDialog(context: Context) = CreateDocumentDialog().apply {
        arguments = Bundle().apply {
            putString(CreateDocumentArgs.ARG_REPORT_ID, reportId)
        }
    }
}