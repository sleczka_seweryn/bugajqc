package com.bugaj.qc.app.settings.option.change.adapter

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView

@BindingAdapter("items")
fun setItems(listView: RecyclerView, items: List<SettingsOptionItem>?) {
    items?.let {
        (listView.adapter as SettingsOptionAdapter).submitList(items)
    }
}