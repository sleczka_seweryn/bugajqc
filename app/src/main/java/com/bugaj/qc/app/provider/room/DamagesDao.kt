package com.bugaj.qc.app.provider.room

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
abstract class DamagesDao {

    @Query("SELECT * FROM damages WHERE id = :id")
    abstract suspend fun find(id: String): DamageEntity?

    @Query("SELECT * FROM damages WHERE reportId = :reportId ORDER BY `order` ASC")
    abstract suspend fun listByReportId(reportId: String): List<DamageEntity>

    @Query("SELECT * FROM damages WHERE reportId = :reportId ORDER BY `order` ASC")
    abstract fun observeWhereReportId(reportId: String): LiveData<List<DamageEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insert(entity: DamageEntity)

    @Update
    abstract suspend fun update(entity: DamageEntity)

    @Query("DELETE FROM damages WHERE id = :id")
    abstract suspend fun delete(id: String)

    @Transaction
    open suspend fun insertAll(dataList: List<DamageEntity>) {
        dataList.forEach { entity ->
            insert(entity)
        }
    }

    @Transaction
    open suspend fun updateAll(dataList: List<DamageEntity>) {
        dataList.forEach { entity ->
            update(entity)
        }
    }

}