package com.bugaj.qc.app.report.document

import javax.inject.Inject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.bugaj.qc.app.application.arch.BaseViewModel
import com.bugaj.qc.app.application.navigation.DismissDialogAction
import com.bugaj.qc.app.report.details.navigation.ReportDetailsArgs
import com.bugaj.qc.app.service.DocumentService
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.lang.IllegalArgumentException

@HiltViewModel
class CreateDocumentViewModel @Inject constructor(
    private val documentService: DocumentService,
    private val savedStateHandle: SavedStateHandle
) : BaseViewModel() {

    val reportId: String = savedStateHandle.get<String>(ReportDetailsArgs.ARG_REPORT_ID) ?: throw IllegalArgumentException("reportId not found")

    fun onClickConfirm() {
        viewModelScope.launch {
            showLoader()
            documentService.generateReport(reportId)
            hideLoader()
            navigate(DismissDialogAction())
        }
    }

    fun onClickCancel() {
        navigate(DismissDialogAction())
    }

}