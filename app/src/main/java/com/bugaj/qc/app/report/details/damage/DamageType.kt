package com.bugaj.qc.app.report.details.damage

import com.bugaj.qc.app.R

enum class DamageType constructor(
    val labelResId: Int
) {
    MECHANICAL_DAMAGE(R.string.report_param_mechanical_damage),
    GREENING(R.string.report_param_greening),
    GROWTH_CRACKS(R.string.report_param_growth_cracks),
    PEST_DAMAGES(R.string.report_param_pest_damages),
    COMMON_SCARB(R.string.report_param_common_scarb),
    MIS_SHAPES(R.string.report_param_mis_shapes),
    BRUISING(R.string.report_param_bruising),
    INTERNAL(R.string.report_param_internal),
    COATING_DAMAGES(R.string.report_param_white_coating_damages),
    ROT(R.string.report_param_rot),
    OTHER(R.string.report_param_other)
}