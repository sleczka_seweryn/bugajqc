package com.bugaj.qc.app.application.common

data class AutoCompleteValue constructor(
    val text: String = "",
    val item: SpinnerItem? = null
)