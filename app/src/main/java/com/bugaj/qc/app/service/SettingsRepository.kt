package com.bugaj.qc.app.service

import androidx.lifecycle.LiveData
import com.bugaj.qc.app.provider.room.SettingEntity

interface SettingsRepository {

    fun observeSettings(): LiveData<List<SettingEntity>>

    suspend fun getSettings(): List<SettingEntity>

    fun observeSetting(name: String): LiveData<SettingEntity>

    fun observeSettingValue(name: String): LiveData<String>

    suspend fun getSetting(name: String): SettingEntity?

    suspend fun saveSetting(settingEntity: SettingEntity)

}