package com.bugaj.qc.app.report.details.navigation

import android.content.Context
import android.content.Intent
import androidx.fragment.app.Fragment
import com.bugaj.qc.app.application.navigation.FragmentTransactionAction
import com.bugaj.qc.app.application.navigation.StartActivityAction
import com.bugaj.qc.app.report.details.ReportDetailsActivity

class DisplayReportDetailsAction constructor(
    private val reportId: String,
    private val shouldCloseParent: Boolean = false
) : StartActivityAction(closeParent = shouldCloseParent) {

    override fun createIntent(context: Context): Intent = Intent(context, ReportDetailsActivity::class.java).apply {
        putExtra(ReportDetailsArgs.ARG_REPORT_ID, reportId)
    }

}