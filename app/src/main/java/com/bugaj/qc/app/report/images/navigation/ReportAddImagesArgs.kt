package com.bugaj.qc.app.report.images.navigation

object ReportAddImagesArgs {

    const val ARG_REPORT_ID: String = "reportId"

}