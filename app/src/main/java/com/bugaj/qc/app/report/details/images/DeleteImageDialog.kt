package com.bugaj.qc.app.report.details.images

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import com.bugaj.qc.app.application.navigation.DialogNavigateManager
import com.bugaj.qc.app.databinding.ImageDeleteDialogFragmentBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DeleteImageDialog constructor() : DialogFragment() {

    private val viewModel by viewModels<DeleteImageViewModel>()

    private lateinit var binding: ImageDeleteDialogFragmentBinding

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let { activity ->

            val inflater = requireActivity().layoutInflater;
            binding = ImageDeleteDialogFragmentBinding.inflate(inflater)
            binding.lifecycleOwner = this
            binding.viewModel = viewModel

            viewModel.navigateAction.observe(this) {
                it?.accept(DialogNavigateManager(this, activity))
            }

            val builder = AlertDialog.Builder(activity)
            builder.setView(binding.root)
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }


}