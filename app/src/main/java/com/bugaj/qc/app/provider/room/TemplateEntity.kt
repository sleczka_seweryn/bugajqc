package com.bugaj.qc.app.provider.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "templates")
class TemplateEntity constructor (
    @PrimaryKey @ColumnInfo(name = "id") var id: String = UUID.randomUUID().toString(),
    @ColumnInfo(name = "labelPl") var labelPl: String,
    @ColumnInfo(name = "labelEn") var labelEn: String,
    @ColumnInfo(name = "order") var order: Int
)