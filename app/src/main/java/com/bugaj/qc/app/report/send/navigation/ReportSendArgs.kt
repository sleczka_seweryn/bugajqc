package com.bugaj.qc.app.report.send.navigation

object ReportSendArgs {

    const val ARG_REPORT_ID: String = "reportId"

}