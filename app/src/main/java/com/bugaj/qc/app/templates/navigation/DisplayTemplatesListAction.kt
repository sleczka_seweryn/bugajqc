package com.bugaj.qc.app.templates.navigation

import android.content.Context
import android.content.Intent
import com.bugaj.qc.app.application.navigation.StartActivityAction
import com.bugaj.qc.app.templates.list.TemplatesListActivity

class DisplayTemplatesListAction constructor() : StartActivityAction(closeParent = false) {

    override fun createIntent(context: Context): Intent = Intent(context, TemplatesListActivity::class.java)

}