package com.bugaj.qc.app.application.common

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.bugaj.qc.app.R

class SpinnerAdapter constructor (
    private val context: Context
) : BaseAdapter() {

    private val dataList: ArrayList<SpinnerItem> = arrayListOf()

    private var hasError: Boolean = false

    override fun getCount(): Int = dataList.size

    override fun getItem(position: Int): SpinnerItem = dataList[position]

    override fun getItemId(position: Int): Long = getItem(position).hashCode().toLong()

    fun update(data: List<SpinnerItem>) {
        dataList.clear()
        dataList.addAll(data)
        notifyDataSetChanged()
    }

    fun seSelectedItemPosition(position: Int) {
        dataList.forEach {
            it.isSelected = false
        }
        dataList[position].isSelected = true
    }

    fun getItemPosition(item: SpinnerItem): Int {
        dataList.forEachIndexed { index, data ->
            if(item.code == data.code) {
                return index
            }
        }
        return 0
    }

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        val inflater: LayoutInflater = LayoutInflater.from(context)
        val row: View = inflater.inflate(
            R.layout.spinner_view_item, parent,
            false
        )
        val label = row.findViewById(R.id.label) as TextView
        val item = getItem(position)
        label.text = item.text

        if(hasError) {
            label.setTextColor(ContextCompat.getColor(context, R.color.error_red))
            label.setBackgroundResource(R.drawable.text_input_background_error)
        } else {
            label.setTextColor(ContextCompat.getColor(context, R.color.text_dark))
            label.setBackgroundResource(R.drawable.text_input_background)
        }

        return row
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        val inflater: LayoutInflater = LayoutInflater.from(context)
        val row: View = inflater.inflate(
            R.layout.spinner_dropdown_item, parent,
            false
        )
        val label = row.findViewById(R.id.label) as TextView
        val icon = row.findViewById(R.id.icon) as ImageView
        val item = getItem(position)
        label.text = item.text
        if(item.isSelected) {
            label.setTextColor(ContextCompat.getColor(context, R.color.text_dark))
            label.typeface = Typeface.DEFAULT_BOLD
            icon.setColorFilter(ContextCompat.getColor(context, R.color.bugaj_green))
            icon.visibility = View.VISIBLE
        } else {
            label.setTextColor(ContextCompat.getColor(context, R.color.text_dark))
            label.typeface = Typeface.DEFAULT
            icon.visibility = View.INVISIBLE
        }
        if(!item.isEnabled) {
            label.typeface = Typeface.DEFAULT
            label.setTextColor(ContextCompat.getColor(context, R.color.drawnstone_grey))
            icon.setColorFilter(ContextCompat.getColor(context, R.color.drawnstone_grey))
        }
        return row
    }

    override fun isEnabled(position: Int): Boolean {
        return getItem(position).isEnabled
    }

    fun setHasError(hasError: Boolean) {
        this.hasError = hasError
        notifyDataSetChanged()
    }

}