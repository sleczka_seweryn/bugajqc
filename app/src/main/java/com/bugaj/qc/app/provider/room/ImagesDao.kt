package com.bugaj.qc.app.provider.room

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface ImagesDao {

    @Query("SELECT * FROM images WHERE id = :id")
    suspend fun find(id: String): ImageEntity?

    @Query("SELECT * FROM images WHERE reportId = :reportId")
    fun observeReportImages(reportId: String): LiveData<List<ImageEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(image: ImageEntity)

    @Query("DELETE FROM images WHERE id = :id")
    suspend fun delete(id: String)

    @Query("SELECT * FROM images WHERE reportId = :reportId")
    suspend fun findWhereReportId(reportId: String): List<ImageEntity>

}