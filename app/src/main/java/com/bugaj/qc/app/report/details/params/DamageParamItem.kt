package com.bugaj.qc.app.report.details.params

data class DamageParamItem constructor (
    val label: String,
    val value: String,
    val bold: Boolean = false
)