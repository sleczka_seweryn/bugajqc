package com.bugaj.qc.app.application.validation

import com.bugaj.qc.app.R
import com.bugaj.qc.app.application.common.StringRepository

class NotEmptyValidator constructor(
    private val validator: (Validator<String>)? = null
) : Validator<String> {

    override fun validate(strings: StringRepository, value: String?): String? {
        if(value?.isNotEmpty() == true) {
            return validator?.validate(strings,value)
        }
        return strings[R.string.error_required_field]
    }
}