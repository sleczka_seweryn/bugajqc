package com.bugaj.qc.app.error

import javax.inject.Inject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import com.bugaj.qc.app.application.arch.BaseViewModel
import com.bugaj.qc.app.application.navigation.DismissDialogAction
import com.bugaj.qc.app.error.navigation.ErrorDialogArgs
import dagger.hilt.android.lifecycle.HiltViewModel

@HiltViewModel
class ErrorViewModel @Inject constructor(
    private val savedStateHandle: SavedStateHandle
) : BaseViewModel() {

    val errorText: LiveData<String> = MutableLiveData<String>().apply {
        value = savedStateHandle.get<Throwable>(ErrorDialogArgs.ARG_ERROR)?.message ?: "error"
    }

    fun onClickClose() {
        navigate(DismissDialogAction())
    }

}