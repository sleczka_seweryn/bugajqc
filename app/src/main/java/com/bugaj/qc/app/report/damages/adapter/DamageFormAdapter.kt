package com.bugaj.qc.app.report.damages.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.bugaj.qc.app.report.damages.DamagesFormViewModel

class DamageFormAdapter constructor (
    private val viewModel: DamagesFormViewModel
) : ListAdapter<DamageFormField, DamageFormFieldViewHolder>(DamageFormFieldDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DamageFormFieldViewHolder {
        return DamageFormFieldViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: DamageFormFieldViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(viewModel, item)
    }

}