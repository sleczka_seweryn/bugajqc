package com.bugaj.qc.app.templates.list.adapter

import androidx.recyclerview.widget.DiffUtil

class TemplateDiffCallback : DiffUtil.ItemCallback<TemplateItem>() {

    override fun areItemsTheSame(oldItem: TemplateItem, newItem: TemplateItem): Boolean {
        return oldItem.fieldId == newItem.fieldId
    }

    override fun areContentsTheSame(oldItem: TemplateItem, newItem: TemplateItem): Boolean {
        return oldItem == newItem
    }

}