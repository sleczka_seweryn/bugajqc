package com.bugaj.qc.app.application.navigation

import android.content.Context
import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog
import com.bugaj.qc.app.R

class DisplayAlertDialogAction constructor(
    val title: String,
    val buttonLabel: String,
    val onClickPositive: (()->Unit)
) : NavigateAction {

    fun onDisplayDialog(context: Context): AlertDialog {
        return AlertDialog.Builder(context)
            .setTitle(title)
            .setCancelable(false)
            .setPositiveButton(buttonLabel, DialogInterface.OnClickListener { dialog, which ->
                onClickPositive.invoke()
                dialog.dismiss()
            })
            .setNegativeButton(context.getString(R.string.cancel), DialogInterface.OnClickListener { dialog, which ->
                dialog.dismiss()
            })
            .create()
    }

    override fun accept(navigation: NavigateManager) {
        navigation.visit(this)
    }

}