package com.bugaj.qc.app.provider.app

import android.util.Log
import com.bugaj.qc.app.service.DocumentsRepository
import com.bugaj.qc.app.service.ImagesRepository
import com.bugaj.qc.app.service.MaintenanceService
import com.bugaj.qc.app.service.ReportsRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import java.io.File
import java.util.*
import javax.inject.Inject

class MaintenanceServiceImpl @Inject constructor(
    private val reportsRepository: ReportsRepository,
    private val imagesRepository: ImagesRepository,
    private val documentsRepository: DocumentsRepository,
    private val ioDispatcher: CoroutineDispatcher
) : MaintenanceService {

    override suspend fun deleteReport(reportId: String) = withContext(ioDispatcher) {
        imagesRepository.findWhereReportId(reportId).let { images ->
            images.forEach {
                deleteFile(File(it.imageUrl))
            }
        }
        documentsRepository.findByReportId(reportId)?.let {
            deleteFile(File(it.documentUrl))
        }
        reportsRepository.deleteReportWhereId(reportId)
    }

    override suspend fun deleteOldReports() {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.DAY_OF_YEAR, -7)
        val to = calendar.time
        calendar.add(Calendar.YEAR, -5)
        val from = calendar.time
        reportsRepository.findWhereCreationDateBetween(from, to).let { reports ->
            reports.forEach { report ->
                deleteReport(report.id)
            }
        }
        Thread.sleep(500)
    }

    private fun deleteFile(file: File) {
        Log.d("MaintenanceService", "deleteFile = ${file.absolutePath}")
        if(file.delete()) {
            Log.d("MaintenanceService", "fileDeleted")
        } else {
            Log.d("MaintenanceService", "file not deleted")
        }
    }

}