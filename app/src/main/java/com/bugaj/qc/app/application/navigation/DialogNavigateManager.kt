package com.bugaj.qc.app.application.navigation

import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentActivity
import com.bugaj.qc.app.loading.navigation.DismissLoadingDialogAction
import com.bugaj.qc.app.loading.navigation.DisplayLoadingDialogAction

class DialogNavigateManager constructor(
    private val dialog: DialogFragment,
    private val activity: FragmentActivity
) : ActivityNavigateManager(activity) {

    override fun visit(action: DismissDialogAction) {
        dialog.dismiss()
    }

    override fun visit(action: DialogResultAction) {
        if(activity is DialogResultHandler) {
            activity.onDialogResult(action.requestCode, action.resultIntent)
        }
        dialog.dismiss()
    }

    override fun visit(action: DisplayLoadingDialogAction) { }

    override fun visit(action: DismissLoadingDialogAction) { }

}