package com.bugaj.qc.app.report.delete

import javax.inject.Inject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.bugaj.qc.app.application.arch.BaseViewModel
import com.bugaj.qc.app.application.navigation.DismissDialogAction
import com.bugaj.qc.app.report.delete.navigation.ReportDeleteArgs
import com.bugaj.qc.app.service.MaintenanceService
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.lang.IllegalArgumentException

@HiltViewModel
class ReportDeleteViewModel @Inject constructor (
    private val maintenanceService: MaintenanceService,
    private val savedStateHandle: SavedStateHandle
) : BaseViewModel() {

    val reportId: String = savedStateHandle.get<String>(ReportDeleteArgs.ARG_REPORT_ID) ?: throw IllegalArgumentException("reportId not found")

    fun onClickConfirm() {
        viewModelScope.launch {
            showLoader()
            maintenanceService.deleteReport(reportId)
            hideLoader()
            navigate(DismissDialogAction())
        }
    }

    fun onClickCancel() {
        navigate(DismissDialogAction())
    }

}