package com.bugaj.qc.app.settings.maintenance

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import com.bugaj.qc.app.application.navigation.DialogNavigateManager
import com.bugaj.qc.app.databinding.MaintenanceReportsDialogBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MaintenanceReportsDialog constructor() : DialogFragment() {

    private val viewModel by viewModels<MaintenanceReportsViewModel>()

    private lateinit var binding: MaintenanceReportsDialogBinding

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let { activity ->

            val inflater = requireActivity().layoutInflater;
            binding = MaintenanceReportsDialogBinding.inflate(inflater)
            binding.lifecycleOwner = this
            binding.viewModel = viewModel

            viewModel.navigateAction.observe(this) {
                it?.accept(DialogNavigateManager(this, activity))
            }

            val builder = AlertDialog.Builder(activity)
            builder.setView(binding.root)
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }


}