package com.bugaj.qc.app.service

import com.bugaj.qc.app.provider.room.ControllerEntity

interface ControllerRepository {

    suspend fun load(name: String): ControllerEntity?

    suspend fun list(): List<ControllerEntity>

    suspend fun save(entity: ControllerEntity)

}