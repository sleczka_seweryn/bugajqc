package com.bugaj.qc.app.settings.option.change.adapter

import androidx.recyclerview.widget.DiffUtil

class SettingsOptionDiffCallback : DiffUtil.ItemCallback<SettingsOptionItem>() {

    override fun areItemsTheSame(oldItem: SettingsOptionItem, newItem: SettingsOptionItem): Boolean {
        return oldItem.value == newItem.value
    }

    override fun areContentsTheSame(oldItem: SettingsOptionItem, newItem: SettingsOptionItem): Boolean {
        return oldItem == newItem
    }

}