package com.bugaj.qc.app.settings.model

import android.text.InputType
import com.bugaj.qc.app.R
import com.bugaj.qc.app.application.validation.*


enum class Settings constructor(
    val defaultValue: String,
    val labelResId: Int,
    val hintResId: Int,
    val validator: Validator<String>,
    val inputType: Int,
    val options: List<Option>
) {

    SERVER_HOST(
        defaultValue = "31.42.4.178",
        labelResId = R.string.setting_item_host_label,
        hintResId = R.string.server_address_example,
        validator = NotEmptyValidator(UrlValidator()),
        inputType = InputType.TYPE_TEXT_VARIATION_URI,
        options = arrayListOf()
    ),

    SERVER_PORT(
        defaultValue = "21",
        hintResId = R.string.server_port_example,
        labelResId = R.string.setting_item_port_label,
        validator = NotEmptyValidator(IntegerValidator(NumberValidator(
            min = 0f,
            max = 9999f
        ))),
        inputType = InputType.TYPE_NUMBER_FLAG_DECIMAL,
        options = arrayListOf()
    ),

    FTP_USERNAME(
        defaultValue = "USERPTF",
        hintResId = R.string.server_username_example,
        labelResId = R.string.setting_item_username_label,
        validator = NotEmptyValidator(LengthValidator(
            min = 1,
            max = 12
        )),
        inputType = InputType.TYPE_TEXT_VARIATION_PERSON_NAME,
        options = arrayListOf()
    ),

    FTP_PASSWORD(
        defaultValue = "1!2#FTsp",
        hintResId = R.string.server_password_example,
        labelResId = R.string.setting_item_password_label,
        validator = NotEmptyValidator(LengthValidator(
            min = 4,
            max = 12
        )),
        inputType = InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD,
        options = arrayListOf()
    ),

    IMAGE_RESOLUTION(
        defaultValue = ImageResolutionTypes.X426.name,
        hintResId = R.string.empty,
        labelResId = R.string.setting_item_img_resolution_label,
        validator = NotEmptyValidator(),
        inputType = InputType.TYPE_NULL,
        options = arrayListOf(
            Option(
                labelResId = R.string.setting_item_img_resolution_label_x256,
                value = ImageResolutionTypes.X256.name
            ),
            Option(
                labelResId = R.string.setting_item_img_resolution_label_x426,
                value = ImageResolutionTypes.X426.name
            ),
            Option(
                labelResId = R.string.setting_item_img_resolution_label_x640,
                value = ImageResolutionTypes.X640.name
            ),
            Option(
                labelResId = R.string.setting_item_img_resolution_label_x800,
                value = ImageResolutionTypes.X800.name
            ),
            Option(
                labelResId = R.string.setting_item_img_resolution_label_x1024,
                value = ImageResolutionTypes.X1024.name
            ),
            Option(
                labelResId = R.string.setting_item_img_resolution_label_x1280,
                value = ImageResolutionTypes.X1280.name
            ),
            Option(
                labelResId = R.string.setting_item_img_resolution_label_x1920,
                value = ImageResolutionTypes.X1920.name
            )
        )
    ),

    STORE_IMAGES_IN_GALLERY(
        defaultValue = "TRUE",
        labelResId = R.string.setting_item_save_images_in_gallery,
        hintResId = 0,
        validator = OptionalValidator(),
        inputType = InputType.TYPE_NULL,
        options = arrayListOf()
    )

}