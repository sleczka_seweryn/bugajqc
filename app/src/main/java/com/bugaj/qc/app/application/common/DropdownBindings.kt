package com.bugaj.qc.app.application.common

import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import androidx.appcompat.widget.AppCompatSpinner
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener


@BindingAdapter("spinnerData")
fun setData(view: DropdownView, value: List<SpinnerItem>?) {
    value?.let { view.setData(it) }
}

@BindingAdapter("selectedItem")
fun setItem(view: DropdownView, value: SpinnerItem?) {
    value?.let {
        if(view.selectedItem.code != value.code) {
            view.setItem(value)
        }
    }
}

@InverseBindingAdapter(attribute = "selectedItem", event = "selectedItemAttrChanged")
fun getItem(view: DropdownView): SpinnerItem? {
    return view.selectedItem
}

@BindingAdapter(value = ["selectedItemAttrChanged"], requireAll = false)
fun bindSelectedItemAttrChanged(view: DropdownView, listener: InverseBindingListener) {
    view.setOnItemChangeListener{ listener.onChange() }
}
