package com.bugaj.qc.app.dashboard.adapter

import com.bugaj.qc.app.application.common.StringRepository
import com.bugaj.qc.app.provider.room.ReportEntity

class ReportMapper constructor(
    private val strings: StringRepository
) {

    fun map(entity: ReportEntity) : ReportItem {
        return ReportItem(
            id = entity.id,
            deliveryNumber = "LP" + entity.deliveryNumber,
            date = entity.displayDate(),
            supplier = entity.supplier,
            variety = entity.variety + " " + entity.displaySize(),
            controller = entity.controller,
            state = ReportState(
                isDocumentSend = entity.isDocumentSend,
                isDocumentGenerated = entity.isDocumentCreated,
                isDamageFormComplete = true
            )
        )
    }

}