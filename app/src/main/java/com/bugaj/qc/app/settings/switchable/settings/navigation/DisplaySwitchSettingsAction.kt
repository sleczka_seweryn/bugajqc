package com.bugaj.qc.app.settings.switchable.settings.navigation

import android.content.Context
import android.content.Intent
import com.bugaj.qc.app.application.navigation.StartActivityAction
import com.bugaj.qc.app.settings.model.Settings
import com.bugaj.qc.app.settings.switchable.settings.SwitchSettingsActivity

class DisplaySwitchSettingsAction constructor(
    private val settingType: Settings
) : StartActivityAction(closeParent = false) {

    override fun createIntent(context: Context): Intent = Intent(context, SwitchSettingsActivity::class.java).apply {
        putExtra(DisplaySwitchSettingsArgs.SettingTypeArg, settingType)
    }

}