package com.bugaj.qc.app.report.details.images

import javax.inject.Inject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.bugaj.qc.app.application.arch.BaseViewModel
import com.bugaj.qc.app.application.navigation.DismissDialogAction
import com.bugaj.qc.app.service.ImagesRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.lang.IllegalArgumentException

@HiltViewModel
class DeleteImageViewModel @Inject constructor (
    private val imagesRepository: ImagesRepository,
    private val savedStateHandle: SavedStateHandle
) : BaseViewModel() {

    private val imageId = savedStateHandle.get<String>(DeleteImageArgs.ARG_IMAGE_ID_TO_DELETE)
        ?: throw IllegalArgumentException("imageId not found")

    fun onClickConfirmDelete() {
        viewModelScope.launch {
            showLoader()
            imagesRepository.deleteImage(imageId)
            hideLoader()
            navigate(DismissDialogAction())
        }
    }

    fun onClickCancel() {
        navigate(DismissDialogAction())
    }

}