package com.bugaj.qc.app.application.navigation

class FinishActivityAction : NavigateAction {

    override fun accept(navigation: NavigateManager) {
        navigation.visit(this)
    }
}