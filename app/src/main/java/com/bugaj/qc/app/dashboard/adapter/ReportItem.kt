package com.bugaj.qc.app.dashboard.adapter

data class ReportItem constructor (
    val id: String,
    val deliveryNumber: String = "",
    val date: String = "",
    val supplier: String = "",
    val variety: String = "",
    val controller: String = "",
    val state: ReportState = ReportState(false,false, false)
)