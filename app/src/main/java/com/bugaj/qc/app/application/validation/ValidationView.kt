package com.bugaj.qc.app.application.validation

import android.widget.Button
import android.widget.EditText
import androidx.core.content.ContextCompat
import com.bugaj.qc.app.R
import com.bugaj.qc.app.application.common.DropdownView

interface ValidationView {

    fun setError(message: String?)

    fun focus()

}

class EditTextValidationView constructor (
    private val editText: EditText
) : ValidationView {

    override fun setError(message: String?) {
        if(message?.isNotEmpty() == true) {
            editText.setBackgroundResource(R.drawable.text_input_background_error)
            editText.setTextColor(ContextCompat.getColor(editText.context, R.color.error_red))
        } else {
            editText.setBackgroundResource(R.drawable.text_input_background)
            editText.setTextColor(ContextCompat.getColor(editText.context, R.color.text_dark))
        }
    }

    override fun focus() {
        editText.requestFocus()
    }
}

class DropdownValidationView constructor(
    private val dropdownView: DropdownView
) : ValidationView {

    override fun setError(message: String?) {
        dropdownView.setHasError(!message.isNullOrEmpty())
    }

    override fun focus() { /*...*/ }
}

class ButtonValidationView constructor(
    private val button: Button
) : ValidationView {

    override fun setError(message: String?) {
        button.isEnabled = message == null
    }

    override fun focus() { /*...*/ }
}