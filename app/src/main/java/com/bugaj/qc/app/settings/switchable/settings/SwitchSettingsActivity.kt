package com.bugaj.qc.app.settings.switchable.settings

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.bugaj.qc.app.R
import com.bugaj.qc.app.application.navigation.ActivityNavigateManager
import com.bugaj.qc.app.databinding.SwitchableSettingsActivityBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SwitchSettingsActivity : AppCompatActivity() {

    private lateinit var binding: SwitchableSettingsActivityBinding

    private val viewModel by viewModels<SwitchSettingsViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(
            this, R.layout.switchable_settings_activity)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        viewModel.navigateAction.observe(this) {
            it?.accept(ActivityNavigateManager(this))
        }

    }

}