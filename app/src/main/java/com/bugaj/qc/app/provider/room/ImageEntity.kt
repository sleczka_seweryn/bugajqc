package com.bugaj.qc.app.provider.room

import androidx.room.*
import androidx.room.ForeignKey.CASCADE
import java.util.*

@Entity(tableName = "images", foreignKeys = [ForeignKey(
    entity = ReportEntity::class,
    parentColumns = ["id"],
    childColumns = ["reportId"],
    onDelete = CASCADE
)], indices = [Index(value = ["reportId"], unique = false)])
class ImageEntity constructor (
    @PrimaryKey @ColumnInfo(name = "id") var id: String = UUID.randomUUID().toString(),
    @ColumnInfo(name = "reportId")  var reportId: String,
    @ColumnInfo(name = "imageUrl") var imageUrl: String
)