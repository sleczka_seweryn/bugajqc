package com.bugaj.qc.app.report.images

import javax.inject.Inject
import androidx.lifecycle.*
import com.bugaj.qc.app.application.arch.BaseViewModel
import com.bugaj.qc.app.application.navigation.FinishActivityAction
import com.bugaj.qc.app.provider.room.ImageEntity
import com.bugaj.qc.app.report.details.images.DisplayDeleteImageDialogAction
import com.bugaj.qc.app.report.details.navigation.DisplayReportDetailsAction
import com.bugaj.qc.app.report.details.navigation.ReportDetailsArgs
import com.bugaj.qc.app.report.images.navigation.ReportAddImagesArgs
import com.bugaj.qc.app.service.DocumentService
import com.bugaj.qc.app.service.ImagesRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.lang.IllegalArgumentException

@HiltViewModel
class ReportAddImagesViewModel @Inject constructor (
    private val imagesRepository: ImagesRepository,
    private val documentService: DocumentService,
    private val savedStateHandle: SavedStateHandle
) : BaseViewModel() {

    val reportId: String = savedStateHandle.get<String>(ReportAddImagesArgs.ARG_REPORT_ID) ?: throw IllegalArgumentException("reportId not found")

    val error: LiveData<String> = MutableLiveData<String>().apply { value = null }

    val images: LiveData<List<ImageEntity>> = imagesRepository.observeReportImages(reportId)

    val arePhotosFilled: LiveData<Boolean> = images.map { it.size >= 3 }

    val canAddMorePhotos: LiveData<Boolean> = images.map { it.size < 4 }

    fun onClickCreateDocument() {
        viewModelScope.launch {
            showLoader()
            documentService.generateReport(reportId)
            hideLoader()
            navigate(DisplayReportDetailsAction(
                reportId = reportId,
                shouldCloseParent = true
            ))
        }
    }

    fun onClickBackButton() {
        navigate(FinishActivityAction())
    }

    fun onSaveImageEntity(imageEntity: ImageEntity) {
        viewModelScope.launch {
            showLoader()
            imagesRepository.save(imageEntity)
            hideLoader()
        }
    }

    fun onClickRemoveImage(position: Int) {
        images.value?.get(position)?.let { imageEntity ->
            navigate(
                DisplayDeleteImageDialogAction(
                imageId = imageEntity.id
            )
            )
        }
    }

}