package com.bugaj.qc.app.report.create.navigation

object CreateReportArgs {

    const val ARG_REPORT_ID = "reportId"

}