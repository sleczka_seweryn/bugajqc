package com.bugaj.qc.app.application.common;

import android.view.View;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Collections;

public class AdapterLayoutDelegate {

    private RecyclerView.Adapter mAdapter;
    private AdaptableViewGroup mViewGroup;

    private RecyclerView.AdapterDataObserver mObserver = new RecyclerView.AdapterDataObserver() {
        @Override
        public void onChanged() {
            super.onChanged();
            recreateViews();
        }

        @Override
        public void onItemRangeChanged(int positionStart, int itemCount) {
            super.onItemRangeChanged(positionStart, itemCount);
            updateViews(positionStart, itemCount, null);
        }

        @Override
        public void onItemRangeChanged(int positionStart, int itemCount, Object payload) {
            super.onItemRangeChanged(positionStart, itemCount, payload);
            updateViews(positionStart, itemCount, payload);
        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            super.onItemRangeInserted(positionStart, itemCount);
            addViews(positionStart, itemCount);
        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            super.onItemRangeRemoved(positionStart, itemCount);
            removeViews(positionStart, itemCount);
        }

        @Override
        public void onItemRangeMoved(int fromPosition, int toPosition, int itemCount) {
            super.onItemRangeMoved(fromPosition, toPosition, itemCount);
            recreateViews();
        }
    };

    /**
     * Create a new delegate, acting as the bridge between the adapter and the ViewGroup
     *
     * @param viewGroup the ViewGroup which will have views added and removed from
     */
    public AdapterLayoutDelegate(AdaptableViewGroup viewGroup) {
        mViewGroup = viewGroup;
    }

    /**
     * Set the adapter which will add and remove views from this layout
     *
     * @param adapter the adapter
     */
    public void setAdapter(@Nullable RecyclerView.Adapter adapter) {
        if (mAdapter != null) {
            try {
                mAdapter.unregisterAdapterDataObserver(mObserver);
            } catch (Exception ignored) {
            }
        }

        mAdapter = adapter;
        if (mAdapter != null) {
            mAdapter.registerAdapterDataObserver(mObserver);
        }
        recreateViews();
    }

    public RecyclerView.Adapter getAdapter() {
        return mAdapter;
    }

    @Nullable
    public RecyclerView.ViewHolder getViewHolderAt(int index) {
        View view = mViewGroup.getView(index);
        if (view == null) {
            return null;
        }
        return (RecyclerView.ViewHolder) view.getTag(AdapterLayoutConstants.adapter_layout_list_holder);
    }

    private void addViews(int positionStart, int itemCount) {
        final int end = positionStart + itemCount;
        for (int i = positionStart; i < end; i++) {
            addViewAt(i);
        }
    }

    private void addViewAt(int index) {
        addViewAt(mAdapter.getItemViewType(index), index);
    }

    private void addViewAt(int viewType, int index) {
        RecyclerView.ViewHolder viewHolder = mAdapter.onCreateViewHolder(mViewGroup.viewGroup(), viewType);
        //setting the lib to min 4.0 to avoid leaks from doing this
        viewHolder.itemView.setTag(AdapterLayoutConstants.adapter_layout_list_holder, viewHolder);
        viewHolder.itemView.setTag(AdapterLayoutConstants.adapter_layout_list_view_type, viewType);
        viewHolder.itemView.setTag(AdapterLayoutConstants.adapter_layout_list_position, index);
        mViewGroup.addView(viewHolder.itemView);
        mAdapter.onBindViewHolder(viewHolder, index);
    }

    private void updateViews(int positionStart, int itemCount, @Nullable Object payload) {
        final int end = positionStart + itemCount;
        for (int i = positionStart; i < end; i++) {
            RecyclerView.ViewHolder viewHolder = getViewHolderAt(i);
            mAdapter.onBindViewHolder(viewHolder, i, Collections.singletonList(payload));
        }
    }

    private void removeViews(int positionStart, int itemCount) {
        mViewGroup.removeViews(positionStart, itemCount);
        updateViewPosition();
    }

    private void updateViewPosition() {
        for (int i = 0; i < mAdapter.getItemCount(); i++) {
            View child = mViewGroup.getView(i);
            child.setTag(AdapterLayoutConstants.adapter_layout_list_position, i);
        }
    }

    /**
     * Updates all the views to match the dataset changing. Its kinda a last resort since we would
     * prefer to just adjust the views that were changed or removed
     */
    private void recreateViews() {
        if (mAdapter == null) {
            mViewGroup.removeAllViews();
            return;
        }
        int i;
        for (i = 0; i < mAdapter.getItemCount(); i++) {
            int viewType = mAdapter.getItemViewType(i);
            //This means the view could already exist
            if (i < mViewGroup.getChildCount()) {
                View child = mViewGroup.getView(i);
                Integer savedViewType = (Integer) child.getTag(AdapterLayoutConstants.adapter_layout_list_view_type);
                RecyclerView.ViewHolder savedViewHolder = (RecyclerView.ViewHolder) child.getTag(AdapterLayoutConstants.adapter_layout_list_holder);

                if (savedViewType != null && savedViewType == viewType && savedViewHolder != null) {
                    //perfect, it exists and is the right type, so just bind it
                    mAdapter.onBindViewHolder(savedViewHolder, i);
                } else {
                    //it already existed, but something was wrong. So remove it and recreate it
                    addViewAt(viewType, i);
                    mViewGroup.removeView(child);
                }
            } else {
                //Creating a brand new view
                addViewAt(viewType, i);
            }
        }

        //Outside the bounds of the dataset, so remove it
        if (i < mViewGroup.getChildCount()) {
            mViewGroup.removeViews(i, mViewGroup.getChildCount() - i);
        }
    }
}
