package com.bugaj.qc.app.application.validation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.bugaj.qc.app.application.common.SpinnerItem
import com.bugaj.qc.app.application.common.StringRepository

class ValidationLiveData constructor (
    private val strings: StringRepository
) : MediatorLiveData<ValidationData>(), ValidationRunner {

    private lateinit var runner: ValidationRunner

    fun createStringValidation(liveData: LiveData<String>, validator: Validator<String>) {
        runner = ValidationRunnerString(strings, liveData, validator)
        addSource(liveData, Observer { clearError() })
    }

    fun createSpinnerValidation(liveData: LiveData<SpinnerItem>, validator: Validator<SpinnerItem>) {
        runner = ValidationRunnerSpinner(strings, liveData, validator)
        addSource(liveData, Observer { clearError() })
    }

    private fun clearError() {
        value = ValidationData(null)
    }

    override fun isValid(): Boolean {
        return validate()?.isEmpty() ?: true
    }

    override fun validate(): String? {
        val message = runner.validate()
        value =  ValidationData(message)
        return message;
    }

    fun bind(): MutableLiveData<Boolean> {
        return MutableLiveData<Boolean>().also { event ->
            addSource(event, Observer { if(it == false) validate() })
        }
    }

    fun focus() {
        value = value?.focus()
    }

}