package com.bugaj.qc.app.templates.navigation

import android.content.Context
import android.content.Intent
import com.bugaj.qc.app.application.navigation.StartActivityAction
import com.bugaj.qc.app.templates.create.CreateTemplateActivity

class DisplayCreateTemplateAction constructor (
    val fieldId: String? = null
) : StartActivityAction(closeParent = false) {

    override fun createIntent(context: Context): Intent = Intent(context, CreateTemplateActivity::class.java).apply {
        fieldId?.let { value ->
            putExtra(CreateTemplateArgs.ARG_FIELD_ID, value)
        }
    }

}