package com.bugaj.qc.app.splash

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.bugaj.qc.app.application.arch.BaseViewModel
import com.bugaj.qc.app.dashboard.navigation.DisplayDashboardAction
import com.bugaj.qc.app.service.SplashService
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SplashViewModel @Inject constructor(
    private val splashService: SplashService
) : BaseViewModel() {

    val appVersion: LiveData<String> = MutableLiveData<String>(splashService.getAppVersion())

    init {
        viewModelScope.launch {
            splashService.loadApplication()
            navigate(DisplayDashboardAction())
        }
    }

}