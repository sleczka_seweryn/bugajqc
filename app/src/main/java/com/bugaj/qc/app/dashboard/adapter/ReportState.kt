package com.bugaj.qc.app.dashboard.adapter

data class ReportState constructor (
    val isDocumentGenerated: Boolean,
    val isDocumentSend: Boolean,
    val isDamageFormComplete: Boolean
)