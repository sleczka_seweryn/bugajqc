package com.bugaj.qc.app.settings.option.change.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bugaj.qc.app.databinding.SettingOptionItemBinding
import com.bugaj.qc.app.settings.option.change.OptionChangeViewModel

class SettingsOptionViewHolder private constructor(
    val binding: SettingOptionItemBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(viewModel: OptionChangeViewModel, item: SettingsOptionItem) {
        binding.viewModel = viewModel
        binding.item = item
        binding.executePendingBindings()
    }

    companion object {

        fun from(parent: ViewGroup): SettingsOptionViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = SettingOptionItemBinding.inflate(layoutInflater, parent, false)
            return SettingsOptionViewHolder(binding)
        }

    }

}