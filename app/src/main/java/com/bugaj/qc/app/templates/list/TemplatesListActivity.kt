package com.bugaj.qc.app.templates.list

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.bugaj.qc.app.R
import com.bugaj.qc.app.application.navigation.ActivityNavigateManager
import com.bugaj.qc.app.dashboard.adapter.ReportListAdapter
import com.bugaj.qc.app.databinding.TemplatesListActivityBinding
import com.bugaj.qc.app.loading.navigation.DismissLoadingDialogAction
import com.bugaj.qc.app.loading.navigation.DisplayLoadingDialogAction
import com.bugaj.qc.app.templates.list.adapter.TemplateListAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TemplatesListActivity : AppCompatActivity() {

    private lateinit var binding: TemplatesListActivityBinding

    private val viewModel by viewModels<TemplatesListViewModel>()

    private lateinit var adapter: TemplateListAdapter

    private lateinit var navigateManager: ActivityNavigateManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(
            this, R.layout.templates_list_activity)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        navigateManager = ActivityNavigateManager(this)

        viewModel.navigateAction.observe(this) {
            it?.accept(navigateManager)
        }

        adapter = TemplateListAdapter(viewModel)
        binding.list.adapter = adapter

        viewModel.isLoading.observe(this, Observer {
            if(it == true) {
                viewModel.navigate(DisplayLoadingDialogAction())
            } else {
                viewModel.navigate(DismissLoadingDialogAction())
            }
        })
    }

    override fun onBackPressed() {
        viewModel.onClickBack()
    }

}