package com.bugaj.qc.app.report.damages.navigation

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.bugaj.qc.app.application.navigation.DisplayDialogAction
import com.bugaj.qc.app.report.damages.adapter.DamageFormFieldInputDialog

class DisplayDamageInputDialogAction constructor (
    private val fieldId: String,
    private val label: String,
    private val actualValue: Float?
) : DisplayDialogAction() {

    override fun createDialog(context: Context): DialogFragment = DamageFormFieldInputDialog().apply {
        arguments = Bundle().apply {
            putString(DamagesFormArgs.ARG_FIELD_ID, fieldId)
            putString(DamagesFormArgs.ARG_FIELD_LABEL, label)
            if(actualValue != null) {
                putFloat(DamagesFormArgs.ARG_ACTUAL_VALUE, actualValue)
            }
        }
    }

}