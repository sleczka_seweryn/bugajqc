package com.bugaj.qc.app.report.create.navigation

import android.content.Context
import android.content.Intent
import com.bugaj.qc.app.application.navigation.StartActivityAction
import com.bugaj.qc.app.report.create.CreateReportActivity

class DisplayCreateReportAction constructor (
    private val reportId: String? = null
) : StartActivityAction(closeParent = false) {

    override fun createIntent(context: Context) = Intent(context, CreateReportActivity::class.java).apply {
        reportId?.let {
            putExtra(CreateReportArgs.ARG_REPORT_ID, it)
        }
    }

}