package com.bugaj.qc.app.report.details.images

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.bugaj.qc.app.application.navigation.DisplayDialogAction

class DisplayDeleteImageDialogAction constructor(
    private val imageId:  String
) : DisplayDialogAction() {

    override fun createDialog(context: Context): DialogFragment = DeleteImageDialog().apply {
        arguments = Bundle().apply {
            putString(DeleteImageArgs.ARG_IMAGE_ID_TO_DELETE, imageId)
        }
    }
}