package com.bugaj.qc.app.application.navigation

import android.content.Context
import androidx.fragment.app.DialogFragment

abstract class DisplayDialogAction: NavigateAction {

    abstract fun createDialog(context: Context) : DialogFragment

    override fun accept(navigation: NavigateManager) {
        navigation.visit(this)
    }
}