package com.bugaj.qc.app.settings.model

import com.bugaj.qc.app.R

enum class ImageResolutionTypes constructor(
    val nameResId: Int,
    val width: Int,
    val height: Int
){
    X256(R.string.setting_item_img_resolution_label_x256, 256, 144),
    X426(R.string.setting_item_img_resolution_label_x426, 426, 240),
    X640(R.string.setting_item_img_resolution_label_x640, 640, 360),
    X800(R.string.setting_item_img_resolution_label_x800, 800, 450),
    X1024(R.string.setting_item_img_resolution_label_x1024, 1024, 576),
    X1280(R.string.setting_item_img_resolution_label_x1280, 1280, 720),
    X1920(R.string.setting_item_img_resolution_label_x1920, 1920, 1080)
}