package com.bugaj.qc.app.report.damages.navigation

import android.content.Context
import android.content.Intent
import com.bugaj.qc.app.application.navigation.StartActivityAction
import com.bugaj.qc.app.report.damages.DamagesFormActivity

class DisplayEditDamagesFormAction constructor(
    val reportId: String
) : StartActivityAction(closeParent = false) {

    override fun createIntent(context: Context) = Intent(context, DamagesFormActivity::class.java).apply {
        putExtra(DamagesFormArgs.ARG_REPORT_ID, reportId)
        putExtra(DamagesFormArgs.ARG_OPENED_FOR_EDIT, true)
        putExtra(DamagesFormArgs.ARG_FINISH_AFTER_COMMIT, true)
    }

}