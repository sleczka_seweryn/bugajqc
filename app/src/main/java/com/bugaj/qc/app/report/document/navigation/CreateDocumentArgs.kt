package com.bugaj.qc.app.report.document.navigation

object CreateDocumentArgs {

    const val ARG_REPORT_ID: String = "reportId"

}