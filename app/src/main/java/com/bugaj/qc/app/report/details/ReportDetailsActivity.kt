package com.bugaj.qc.app.report.details

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.bugaj.qc.app.R
import com.bugaj.qc.app.application.arch.PhotoCaptureActivity
import com.bugaj.qc.app.application.navigation.ActivityNavigateManager
import com.bugaj.qc.app.databinding.ReportDetailsActivityBinding
import com.bugaj.qc.app.loading.navigation.DismissLoadingDialogAction
import com.bugaj.qc.app.loading.navigation.DisplayLoadingDialogAction
import com.bugaj.qc.app.provider.room.ImageEntity
import com.bugaj.qc.app.report.details.images.ImageDecoder
import com.bugaj.qc.app.report.details.params.DamageParamsAdapter
import com.bugaj.qc.app.report.details.state.ReportState
import com.google.android.material.appbar.AppBarLayout
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ReportDetailsActivity : PhotoCaptureActivity() {

    private lateinit var binding: ReportDetailsActivityBinding

    private val viewModel by viewModels<ReportDetailsViewModel>()

    private lateinit var damageAdapter: DamageParamsAdapter

    private lateinit var navigateManager: ActivityNavigateManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(
            this, R.layout.report_details_activity
        )
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        navigateManager = ActivityNavigateManager(this)

        viewModel.navigateAction.observe(this) {
            it?.accept(navigateManager)
        }

        viewModel.images.observe(this, Observer { images ->
            updateImages(images)
        })

        binding.addPhotoButton.setOnClickListener {
            onClickAddPhoto()
        }

        binding.addPhotoButton2.setOnClickListener {
            onClickAddPhoto()
        }

        binding.pickPhotoButton.setOnClickListener {
            onClickPickFromGallery()
        }

        binding.pickPhotoButton2.setOnClickListener {
            onClickPickFromGallery()
        }

        setupListAdapter()
        setupAppBarCollapsingLayout()

        viewModel.isLoading.observe(this, Observer {
            if(it == true) {
                viewModel.navigate(DisplayLoadingDialogAction())
            } else {
                viewModel.navigate(DismissLoadingDialogAction())
            }
        })

        viewModel.reportState.observe(this, Observer {
            when(it) {
                ReportState.DAMAGE_REPORT_NOT_FILLED -> {
                    binding.stateIcon.setImageResource(R.drawable.ic_round_warning_24)
                    binding.stateLabel.text = getString(R.string.damages_fill_form_title)
                    binding.buttonGeneratePdf.visibility = View.GONE
                    binding.buttonSend.visibility = View.GONE
                    binding.buttonShare.visibility = View.GONE
                }
                ReportState.PHOTOS_NOT_ATTACHED -> {
                    binding.stateIcon.setImageResource(R.drawable.ic_round_warning_24)
                    binding.stateLabel.text = getString(R.string.warrning_add_photos)
                    binding.buttonGeneratePdf.visibility = View.GONE
                    binding.buttonSend.visibility = View.GONE
                    binding.buttonShare.visibility = View.GONE
                }
                ReportState.DOCUMENT_NOT_CREATED -> {
                    binding.stateIcon.setImageResource(R.drawable.ic_check_24)
                    binding.stateLabel.text = getString(R.string.report_filled)
                    binding.buttonGeneratePdf.visibility = View.VISIBLE
                    binding.buttonSend.visibility = View.GONE
                    binding.buttonShare.visibility = View.GONE
                }
                ReportState.DOCUMENT_NOT_SEND -> {
                    binding.stateIcon.setImageResource(R.drawable.ic_check_24)
                    binding.stateLabel.text = getString(R.string.report_filled)
                    binding.buttonGeneratePdf.visibility = View.GONE
                    binding.buttonSend.visibility = View.VISIBLE
                    binding.buttonShare.visibility = View.GONE
                }
                ReportState.DOCUMENT_SEND -> {
                    binding.stateIcon.setImageResource(R.drawable.ic_check_24)
                    binding.stateLabel.text = getString(R.string.report_state_send)
                    binding.buttonGeneratePdf.visibility = View.GONE
                    binding.buttonSend.visibility = View.GONE
                    binding.buttonShare.visibility = View.VISIBLE
                }
                else -> {}
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.report_details_menu, menu);
        return true;
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        menu?.findItem(R.id.action_share)?.isVisible = viewModel.isDocumentCreated()
        menu?.findItem(R.id.action_view)?.isVisible = viewModel.isDocumentCreated()
        menu?.findItem(R.id.action_send)?.isVisible = viewModel.isDocumentCreated()
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.action_share -> {
                viewModel.onClickShare()
                return true
            }
            R.id.action_send -> {
                viewModel.onClickSend()
                return true
            }
            R.id.action_view -> {
                viewModel.onClickView()
                return true
            }
            R.id.action_delete -> {
                viewModel.onClickDelete()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSaveImageEntity(imageEntity: ImageEntity) {
        viewModel.onSaveImageEntity(imageEntity)
    }

    override fun getReportId(): String = viewModel.reportId

    private fun setupAppBarCollapsingLayout() {
        setSupportActionBar(binding.toolbar);
        binding.appBar.addOnOffsetChangedListener(
            object : AppBarLayout.OnOffsetChangedListener {
                override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {
                    binding.parallax.alpha =
                        (appBarLayout.totalScrollRange + verticalOffset).toFloat() / appBarLayout.totalScrollRange
                }
            },
        )
    }

    private fun setupListAdapter() {
        damageAdapter = DamageParamsAdapter(viewModel)
        binding.damageAdapterView.adapter = damageAdapter

        viewModel.damageItems.observe(this, Observer {
            binding.damageAdapterView.adapter = null
            damageAdapter.submitList(it, Runnable {
                binding.damageAdapterView.adapter = damageAdapter
            })
        })
    }

    override fun onBackPressed() {
        viewModel.onClickBackButton()
    }

    private fun updateImages(images: List<ImageEntity>) {

        val decoder = ImageDecoder(baseContext)

        if(images.isEmpty()) {
            binding.imageView1Layout.visibility = View.GONE
            binding.imageView2Layout.visibility = View.GONE
            binding.imageView3Layout.visibility = View.GONE
            binding.imageView4Layout.visibility = View.GONE
        }
        if(images.size == 1) {
            binding.imageView1Layout.visibility = View.VISIBLE
            decoder.loadImageForView(binding.imageView1, images[0])
            binding.imageView2Layout.visibility = View.INVISIBLE
            binding.imageView3Layout.visibility = View.GONE
            binding.imageView4Layout.visibility = View.GONE
        }
        if(images.size == 2) {
            binding.imageView1Layout.visibility = View.VISIBLE
            decoder.loadImageForView(binding.imageView1, images[0])
            binding.imageView2Layout.visibility = View.VISIBLE
            decoder.loadImageForView(binding.imageView2, images[1])
            binding.imageView3Layout.visibility = View.GONE
            binding.imageView4Layout.visibility = View.GONE
        }
        if(images.size == 3) {
            binding.imageView1Layout.visibility = View.VISIBLE
            decoder.loadImageForView(binding.imageView1, images[0])
            binding.imageView2Layout.visibility = View.VISIBLE
            decoder.loadImageForView(binding.imageView2, images[1])
            binding.imageView3Layout.visibility = View.VISIBLE
            decoder.loadImageForView(binding.imageView3, images[2])
            binding.imageView4Layout.visibility = View.INVISIBLE
        }
        if(images.size >= 4) {
            binding.imageView1Layout.visibility = View.VISIBLE
            decoder.loadImageForView(binding.imageView1, images[0])
            binding.imageView2Layout.visibility = View.VISIBLE
            decoder.loadImageForView(binding.imageView2, images[1])
            binding.imageView3Layout.visibility = View.VISIBLE
            decoder.loadImageForView(binding.imageView3, images[2])
            binding.imageView4Layout.visibility = View.VISIBLE
            decoder.loadImageForView(binding.imageView4, images[3])
        }
    }

}