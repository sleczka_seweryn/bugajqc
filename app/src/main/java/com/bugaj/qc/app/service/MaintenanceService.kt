package com.bugaj.qc.app.service

interface MaintenanceService {

    suspend fun deleteReport(reportId: String)

    suspend fun deleteOldReports()

}