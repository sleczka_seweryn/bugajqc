package com.bugaj.qc.app.application.validation

import androidx.lifecycle.LiveData
import com.bugaj.qc.app.application.common.StringRepository
import javax.inject.Inject

class ValidationManager @Inject constructor (
    private val strings: StringRepository
) {

    private val validators = ArrayList<ValidationLiveData>()

    fun addValidator( value: LiveData<String>, validator: Validator<String> ): ValidationLiveData {
        return ValidationLiveData(strings).also { liveData ->
            liveData.createStringValidation(value, validator)
            validators.add(liveData)
        }
    }

    fun validate() : Boolean {
        var validationResult = true
        validators.forEach { validator ->
            if(!validator.isValid()) {
                if(validationResult) {
                    validator.focus()
                }
                validationResult = false
            }

        }
        return validationResult
    }

}