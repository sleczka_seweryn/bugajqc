package com.bugaj.qc.app.provider.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "suppliers")
class SupplierEntity constructor (
    @PrimaryKey @ColumnInfo(name = "name") var name: String,
    @ColumnInfo(name = "index") var index: Int = 0
) {

    fun incrementIndex() {
        index += 1
    }

}