package com.bugaj.qc.app.application.navigation

import android.util.Log
import androidx.fragment.app.FragmentActivity
import com.bugaj.qc.app.R
import com.bugaj.qc.app.loading.LoadingDialog
import com.bugaj.qc.app.loading.navigation.DismissLoadingDialogAction
import com.bugaj.qc.app.loading.navigation.DisplayLoadingDialogAction

open class ActivityNavigateManager constructor(
    private val activity: FragmentActivity
) : NavigateManager {

    private var loadingDialog: LoadingDialog? = null

    override fun visit(action: StartActivityAction) {
        activity.startActivity(action.createIntent(activity))
        if(action.closeParent) {
            activity.finish()
        }
    }

    override fun visit(action: FinishActivityAction) {
        activity.finish()
    }

    override fun visit(action: DisplayExceptionAction) {
        Log.e("DisplayExceptionAction", action.message)
        activity.finish()
    }

    override fun visit(action: FragmentTransactionAction) {
        activity.supportFragmentManager.run {
            val fragment = findFragmentByTag(action.getTag())
                ?: action.createFragment(activity)
            beginTransaction()
                .replace(R.id.container, fragment, action.getTag())
                .commit()
        }
    }

    override fun visit(action: DisplayDialogAction) {
        action.createDialog(activity).show(
            activity.supportFragmentManager, "dialog"
        )
    }

    override fun visit(action: DismissDialogAction) {
        //Do nothing
    }

    override fun visit(action: DisplayAlertDialogAction) {
        action.onDisplayDialog(activity).show()
    }

    override fun visit(action: DialogResultAction) { }

    override fun visit(action: DisplayLoadingDialogAction) {
        if(loadingDialog == null) {
            loadingDialog = LoadingDialog()
            loadingDialog?.show(
                activity.supportFragmentManager, "progressDialog"
            )
        }
    }

    override fun visit(action: DismissLoadingDialogAction) {
        loadingDialog?.dismiss()
        loadingDialog = null
    }
}