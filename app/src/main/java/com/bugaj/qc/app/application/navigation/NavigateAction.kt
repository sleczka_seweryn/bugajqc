package com.bugaj.qc.app.application.navigation

import java.io.Serializable

interface NavigateAction : Serializable {

    fun accept(navigation: NavigateManager)

}