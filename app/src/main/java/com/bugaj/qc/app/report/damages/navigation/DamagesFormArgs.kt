package com.bugaj.qc.app.report.damages.navigation

object DamagesFormArgs {

    const val ARG_REPORT_ID: String = "reportId"

    const val ARG_OPENED_FOR_EDIT: String = "opened_for_edit"

    const val ARG_ACTUAL_VALUE: String = "actual_value"

    const val ARG_FIELD_ID: String = "fieldId"

    const val ARG_FIELD_LABEL: String = "label"

    const val ARG_FINISH_AFTER_COMMIT: String = "finish_after_commit"

}