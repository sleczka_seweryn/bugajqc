package com.bugaj.qc.app.report.send

import javax.inject.Inject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.bugaj.qc.app.application.arch.BaseViewModel
import com.bugaj.qc.app.application.navigation.DismissDialogAction
import com.bugaj.qc.app.error.navigation.DisplayErrorDialogAction
import com.bugaj.qc.app.report.delete.navigation.ReportDeleteArgs
import com.bugaj.qc.app.service.MaintenanceService
import com.bugaj.qc.app.service.UploadService
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.lang.IllegalArgumentException

@HiltViewModel
class ReportSendViewModel @Inject constructor (
    private val uploadService: UploadService,
    private val savedStateHandle: SavedStateHandle
) : BaseViewModel() {

    val reportId: String = savedStateHandle.get<String>(ReportDeleteArgs.ARG_REPORT_ID) ?: throw IllegalArgumentException("reportId not found")

    init {
        viewModelScope.launch {
            try {
                uploadService.sendReport(reportId)
            } catch (ex: Throwable) {
                navigate(DismissDialogAction())
                navigate(DisplayErrorDialogAction(ex))
            } finally {
                navigate(DismissDialogAction())
            }
        }
    }

    fun onClickCancel() {
        navigate(DismissDialogAction())
    }

}