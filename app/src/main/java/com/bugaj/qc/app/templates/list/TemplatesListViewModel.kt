package com.bugaj.qc.app.templates.list

import androidx.lifecycle.*
import com.bugaj.qc.app.R
import com.bugaj.qc.app.application.arch.BaseViewModel
import com.bugaj.qc.app.application.common.StringRepository
import com.bugaj.qc.app.application.navigation.DisplayAlertDialogAction
import com.bugaj.qc.app.application.navigation.FinishActivityAction
import com.bugaj.qc.app.provider.room.TemplateEntity
import com.bugaj.qc.app.service.TemplateRepository
import com.bugaj.qc.app.templates.list.adapter.TemplateItem
import com.bugaj.qc.app.templates.list.adapter.TemplateMapper
import com.bugaj.qc.app.templates.navigation.DisplayCreateTemplateAction
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TemplatesListViewModel @Inject constructor (
    private val strings: StringRepository,
    private val templateRepository: TemplateRepository
) : BaseViewModel() {

    private val _items: LiveData<List<TemplateItem>> = templateRepository.observeList()
        .distinctUntilChanged()
        .switchMap { mapTemplates(it) }

    val items: LiveData<List<TemplateItem>> = _items

    fun onClickEditTemplate(item: TemplateItem) {
        navigate(DisplayCreateTemplateAction(item.fieldId))
    }

    fun onClickDeleteTemplate(item: TemplateItem) {
        navigate(DisplayAlertDialogAction(
            strings[R.string.field_template_delete_title],
            strings[R.string.delete],
        ) {
            viewModelScope.launch {
                templateRepository.delete(item.fieldId)
            }
        })
    }

    fun onClickCreateTemplate() {
        navigate(DisplayCreateTemplateAction())
    }

    private fun mapTemplates(data: List<TemplateEntity>): LiveData<List<TemplateItem>> {
        val mapper = TemplateMapper()
        val result = MutableLiveData<List<TemplateItem>>()
        result.value = data.map { mapper.map(it) }
        return result
    }

    fun onClickBack() {
        navigate(FinishActivityAction())
    }

}