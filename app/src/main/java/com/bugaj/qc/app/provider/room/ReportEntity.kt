package com.bugaj.qc.app.provider.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.bugaj.qc.app.report.create.samples.Samples
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.SimpleDateFormat
import java.util.*

@Entity(tableName = "reports")
class ReportEntity constructor (
    @PrimaryKey @ColumnInfo(name = "id") var id: String = UUID.randomUUID().toString(),
    @ColumnInfo(name = "date") var date: Date,
    @ColumnInfo(name = "creationDate") var creationDate: Date,
    @ColumnInfo(name = "controlledSamples") var controlledSamples: String,
    @ColumnInfo(name = "deliveryNumber") var deliveryNumber: String,
    @ColumnInfo(name = "supplier") var supplier: String,
    @ColumnInfo(name = "variety") var variety: String,
    @ColumnInfo(name = "controller") var controller: String,
    @ColumnInfo(name = "sizeMin") var sizeMin: Int,
    @ColumnInfo(name = "sizeMax") var sizeMax: Int,
    @ColumnInfo(name = "temperatureMin") var temperatureMin: Float,
    @ColumnInfo(name = "temperatureMax") var temperatureMax: Float,
    @ColumnInfo(name = "isDocumentCreated") var isDocumentCreated: Boolean = false,
    @ColumnInfo(name = "isDocumentSend") var isDocumentSend: Boolean = false
) {

    fun getSamplesCategory(): Samples {
        return Samples.valueOf(controlledSamples)
    }

    fun displaySize(): String {
        val decimalFormat = DecimalFormat("0", DecimalFormatSymbols(Locale("pl")))
        val min = if(sizeMin < 0) "(${decimalFormat.format(sizeMin)})" else decimalFormat.format(sizeMin)
        val max = if(sizeMax < 0) "(${decimalFormat.format(sizeMax)})" else decimalFormat.format(sizeMax)
        return "$min - ${max}mm"
    }

    fun displayTemp(): String {
        val decimalFormat = DecimalFormat("0", DecimalFormatSymbols(Locale("pl")))
        decimalFormat.maximumFractionDigits = 1
        val min = if(temperatureMin < 0)  "(${decimalFormat.format(temperatureMin)})" else decimalFormat.format(temperatureMin)
        val max = if(temperatureMax < 0)  "(${decimalFormat.format(temperatureMax)})" else decimalFormat.format(temperatureMax)
        return "$min - ${max}°C"
    }

    fun displayControlledSamplesNum(): String {
        return getSamplesCategory().number.toString()
    }

    fun displayDate(): String {
        return SimpleDateFormat("dd.MM.YYYY", Locale.getDefault() ).format(date)
    }

    fun formatDate(): String {
        return SimpleDateFormat("yyyy-MM-dd_HH-mm-ss", Locale.getDefault() ).format(date)
    }

    fun getControllerName(): String {
        return controller.split(" ")[0]
    }

    fun getControllerSurname(): String? {
        val names = controller.split(" ")
        if (names.size > 1) {
            return names[1]
        }
        return null
    }

}