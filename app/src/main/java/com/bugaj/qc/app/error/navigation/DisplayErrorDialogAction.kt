package com.bugaj.qc.app.error.navigation

import android.content.Context
import android.os.Bundle
import com.bugaj.qc.app.application.navigation.DisplayDialogAction
import com.bugaj.qc.app.error.ErrorDialog

class DisplayErrorDialogAction constructor(
    private val error: Throwable
) : DisplayDialogAction() {

    override fun createDialog(context: Context) = ErrorDialog().apply {
        arguments = Bundle().apply {
            putSerializable(ErrorDialogArgs.ARG_ERROR, error)
        }
    }
}
