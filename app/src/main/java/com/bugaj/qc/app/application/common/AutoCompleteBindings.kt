package com.bugaj.qc.app.application.common

import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener

@BindingAdapter("spinnerItems")
fun setData(view: AutoCompleteView, value: List<SpinnerItem>?) {
    value?.let { view.setData(it) }
}

@BindingAdapter("autoCompleteValue")
fun setItem(view: AutoCompleteView, value: AutoCompleteValue?) {
    value?.let {
        if(view.getSelectedItem().text != value.text) {
            view.setItem(value)
        }
    }
}

@InverseBindingAdapter(attribute = "autoCompleteValue", event = "autoCompleteValueAttrChanged")
fun getItem(view: AutoCompleteView): AutoCompleteValue? {
    return view.getSelectedItem()
}

@BindingAdapter(value = ["autoCompleteValueAttrChanged"], requireAll = false)
fun bindSelectedItemAttrChanged(view: AutoCompleteView, listener: InverseBindingListener) {
    view.setOnItemChangeListener{ listener.onChange() }
}
