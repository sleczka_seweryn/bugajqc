package com.bugaj.qc.app.dashboard

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.bugaj.qc.app.R
import com.bugaj.qc.app.application.navigation.ActivityNavigateManager
import com.bugaj.qc.app.dashboard.adapter.ReportFilterType
import com.bugaj.qc.app.dashboard.adapter.ReportListAdapter
import com.bugaj.qc.app.databinding.DashboardActivityBinding
import com.bugaj.qc.app.loading.navigation.DismissLoadingDialogAction
import com.bugaj.qc.app.loading.navigation.DisplayLoadingDialogAction
import com.google.android.material.tabs.TabLayout
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DashboardActivity : AppCompatActivity(), TabLayout.OnTabSelectedListener {

    private lateinit var binding: DashboardActivityBinding

    private val viewModel by viewModels<DashboardViewModel>()

    private lateinit var reportsAdapter: ReportListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(
            this, R.layout.dashboard_activity)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        viewModel.navigateAction.observe(this) {
            it?.accept(ActivityNavigateManager(this))
        }

        binding.tabs.addOnTabSelectedListener(this)

        reportsAdapter = ReportListAdapter(viewModel)
        binding.reportsList.adapter = reportsAdapter

        viewModel.isLoading.observe(this, Observer {
            if(it == true) {
                viewModel.navigate(DisplayLoadingDialogAction())
            } else {
                viewModel.navigate(DismissLoadingDialogAction())
            }
        })
    }

    override fun onTabSelected(tab: TabLayout.Tab?) {
        when(tab?.position) {
            0 -> { viewModel.setFilterType(ReportFilterType.NEWS) }
            1 -> { viewModel.setFilterType(ReportFilterType.SEND) }
            else -> {}
        }
    }

    override fun onTabUnselected(tab: TabLayout.Tab?) {  }

    override fun onTabReselected(tab: TabLayout.Tab?) { }

}