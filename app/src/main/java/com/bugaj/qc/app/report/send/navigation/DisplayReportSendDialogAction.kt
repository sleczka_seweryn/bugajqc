package com.bugaj.qc.app.report.send.navigation

import android.content.Context
import android.os.Bundle
import com.bugaj.qc.app.application.navigation.DisplayDialogAction
import com.bugaj.qc.app.report.send.ReportSendDialog

class DisplayReportSendDialogAction constructor(
    private val reportId: String
) : DisplayDialogAction() {

    override fun createDialog(context: Context) = ReportSendDialog().apply {
        arguments = Bundle().apply {
            putString(ReportSendArgs.ARG_REPORT_ID, reportId)
        }
    }
}