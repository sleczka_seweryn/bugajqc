package com.bugaj.qc.app.settings.switchable.settings

import javax.inject.Inject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.bugaj.qc.app.application.arch.BaseViewModel
import com.bugaj.qc.app.application.common.StringRepository
import com.bugaj.qc.app.application.navigation.FinishActivityAction
import com.bugaj.qc.app.application.validation.ValidationManager
import com.bugaj.qc.app.provider.room.SettingEntity
import com.bugaj.qc.app.service.SettingsRepository
import com.bugaj.qc.app.settings.edit.navigation.DisplayEditSettingsArgs
import com.bugaj.qc.app.settings.model.Settings
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.lang.IllegalStateException

@HiltViewModel
class SwitchSettingsViewModel @Inject constructor(
    private val strings: StringRepository,
    private val validationManager: ValidationManager,
    private val settingsRepository: SettingsRepository,
    private val savedStateHandle: SavedStateHandle
) : BaseViewModel() {

    private val settingType: Settings = savedStateHandle[DisplayEditSettingsArgs.SettingTypeArg]
        ?: throw IllegalStateException("SettingTypeArg action not found")

    val settingLabel = MutableLiveData<String>().apply {
        value = strings[settingType.labelResId]
    }

    val settingValue = MutableLiveData<Boolean>().apply {
        viewModelScope.launch {
            value = settingsRepository.getSetting(settingType.name)?.value == "TRUE"
        }
    }

    val inputType = MutableLiveData<Int>().apply {
        value = settingType.inputType
    }

    fun onClickChange() {
        if(validationManager.validate()) {
            viewModelScope.launch {
                showLoader()
                settingsRepository.saveSetting(SettingEntity(
                    name = settingType.name,
                    value = if(settingValue.value == true) "TRUE" else "FALSE"
                ))
                navigate(FinishActivityAction())
            }
        }
    }

    fun onClickBack() {
        navigate(FinishActivityAction())
    }

}