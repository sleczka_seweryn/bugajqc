package com.bugaj.qc.app.report.create

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import com.bugaj.qc.app.R
import com.bugaj.qc.app.application.navigation.ActivityNavigateManager
import com.bugaj.qc.app.databinding.ReportCreateActivityBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CreateReportActivity : AppCompatActivity() {

    private lateinit var binding: ReportCreateActivityBinding

    private val viewModel by viewModels<CreateReportViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(
            this, R.layout.report_create_activity)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        viewModel.navigateAction.observe(this) {
            it?.accept(ActivityNavigateManager(this))
        }
    }

    override fun onBackPressed() {
        viewModel.onClickBack()
    }

}