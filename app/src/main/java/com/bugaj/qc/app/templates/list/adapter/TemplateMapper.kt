package com.bugaj.qc.app.templates.list.adapter

import com.bugaj.qc.app.provider.room.TemplateEntity

class TemplateMapper {

    fun map(entity: TemplateEntity) : TemplateItem {
        return TemplateItem(
            entity.id,
            entity.labelPl,
            entity.labelEn
        )
    }

}