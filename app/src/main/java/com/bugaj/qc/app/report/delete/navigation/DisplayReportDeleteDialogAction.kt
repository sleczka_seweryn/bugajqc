package com.bugaj.qc.app.report.delete.navigation

import android.content.Context
import android.os.Bundle
import com.bugaj.qc.app.application.navigation.DisplayDialogAction
import com.bugaj.qc.app.report.delete.ReportDeleteDialog

class DisplayReportDeleteDialogAction constructor(
    private val reportId: String
) : DisplayDialogAction() {

    override fun createDialog(context: Context) = ReportDeleteDialog().apply {
        arguments = Bundle().apply {
            putString(ReportDeleteArgs.ARG_REPORT_ID, reportId)
        }
    }
}