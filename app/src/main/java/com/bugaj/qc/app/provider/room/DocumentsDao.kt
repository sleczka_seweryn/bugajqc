package com.bugaj.qc.app.provider.room

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface DocumentsDao {

    @Query("SELECT * FROM documents WHERE id = :id")
    suspend fun find(id: String): DocumentEntity?

    @Query("SELECT * FROM documents WHERE reportId = :reportId LIMIT 1")
    suspend fun findByReportId(reportId: String): DocumentEntity?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entity: DocumentEntity)

    @Query("DELETE FROM documents WHERE id = :id")
    suspend fun delete(id: String)

    @Query("SELECT * FROM documents WHERE reportId = :reportId")
    fun observeDocument(reportId: String): LiveData<DocumentEntity>

}