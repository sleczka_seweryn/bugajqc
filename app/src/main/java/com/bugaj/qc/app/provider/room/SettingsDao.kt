package com.bugaj.qc.app.provider.room

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface SettingsDao {

    @Query("SELECT * FROM settings")
    fun observeSettings(): LiveData<List<SettingEntity>>

    @Query("SELECT settings.value FROM settings WHERE name = :name LIMIT 1")
    fun observeSettingValue(name: String): LiveData<String>

    @Query("SELECT * FROM settings WHERE name = :name")
    fun observeSettingByName(name: String): LiveData<SettingEntity>

    @Query("SELECT * FROM settings")
    suspend fun getSettings(): List<SettingEntity>

    @Query("SELECT * FROM settings WHERE name = :name")
    suspend fun getSettingByName(name: String): SettingEntity?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSetting(setting: SettingEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertBlocking(setting: SettingEntity)

    @Update
    suspend fun updateSetting(setting: SettingEntity): Int

}