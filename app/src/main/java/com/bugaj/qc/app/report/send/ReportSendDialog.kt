package com.bugaj.qc.app.report.send

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import com.bugaj.qc.app.application.navigation.DialogNavigateManager
import com.bugaj.qc.app.databinding.DocumentCreateDialogFragmentBinding
import com.bugaj.qc.app.databinding.ReportDeleteDialogBinding
import com.bugaj.qc.app.databinding.ReportSendDialogBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ReportSendDialog constructor() : DialogFragment() {

    private val viewModel by viewModels<ReportSendViewModel>()

    private lateinit var binding: ReportSendDialogBinding

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let { activity ->

            val inflater = requireActivity().layoutInflater;
            binding = ReportSendDialogBinding.inflate(inflater)
            binding.lifecycleOwner = this
            binding.viewModel = viewModel

            viewModel.navigateAction.observe(this) {
                it?.accept(DialogNavigateManager(this, activity))
            }

            val builder = AlertDialog.Builder(activity)
            builder.setView(binding.root)
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }


}