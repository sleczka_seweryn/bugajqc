package com.bugaj.qc.app.application.validation

import com.bugaj.qc.app.R
import com.bugaj.qc.app.application.common.StringRepository

class NumberValidator constructor (
    private val min: Float,
    private val max: Float,
    private val validator: (Validator<String>)? = null
) : Validator<String> {

    override fun validate(strings: StringRepository, value: String?): String? {
        val intValue: Float? = value?.replace(",", ".")?.toFloatOrNull()
        if(intValue == null) {
            return strings[R.string.error_invalid_value]
        } else if(intValue < min) {
            return strings[R.string.error_min_value] + " " + min
        } else if(intValue > max) {
            return strings[R.string.error_max_value] + " " + max
        } else {
            return validator?.validate(strings, value)
        }
    }
}