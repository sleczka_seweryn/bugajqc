package com.bugaj.qc.app.application.common

import androidx.databinding.BindingAdapter

@BindingAdapter("label")
fun labelSettingView(view: SettingView, value: String?) {
    value?.let { view.setLabel(it) }
}

@BindingAdapter("value")
fun valueSettingView(view: SettingView, value: String?) {
    value?.let { view.setValue(it) }
}

@BindingAdapter("icon")
fun iconSettingView(view: SettingView, value: Int?) {
    view.setIcon(value)
}