package com.bugaj.qc.app.service

interface SplashService {

    suspend fun loadApplication(): Boolean

    fun getAppVersion(): String

    fun checkLicense(): Boolean

}