package com.bugaj.qc.app.report.delete.navigation

object ReportDeleteArgs {

    const val ARG_REPORT_ID: String = "reportId"

}