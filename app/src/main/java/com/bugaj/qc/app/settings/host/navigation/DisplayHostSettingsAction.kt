package com.bugaj.qc.app.settings.host.navigation

import android.content.Context
import android.content.Intent
import com.bugaj.qc.app.application.navigation.StartActivityAction
import com.bugaj.qc.app.settings.host.HostSettingsActivity

class DisplayHostSettingsAction constructor() : StartActivityAction(closeParent = false) {

    override fun createIntent(context: Context): Intent = Intent(context, HostSettingsActivity::class.java)

}