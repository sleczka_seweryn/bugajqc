package com.bugaj.qc.app.provider.ftp

import android.util.Log
import com.bugaj.qc.app.service.DocumentsRepository
import com.bugaj.qc.app.service.ReportsRepository
import com.bugaj.qc.app.service.SettingsRepository
import com.bugaj.qc.app.service.UploadService
import com.bugaj.qc.app.settings.model.Settings
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import org.apache.commons.net.ftp.FTP
import org.apache.commons.net.ftp.FTPClient
import org.apache.commons.net.ftp.FTPSClient
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.lang.Exception
import java.lang.RuntimeException
import java.net.InetAddress
import javax.inject.Inject

class FTPUploadService @Inject constructor(
    private val ioDispatcher: CoroutineDispatcher,
    private val documentsRepository: DocumentsRepository,
    private val reportsRepository: ReportsRepository,
    private val settingsRepository: SettingsRepository
) : UploadService {

    override suspend fun sendReport(reportId: String) = withContext(ioDispatcher) {
        val report = reportsRepository.getReport(reportId) ?: throw IllegalStateException("reportId not found")

        val serverHost = settingsRepository.getSetting(Settings.SERVER_HOST.name)?.value ?: throw RuntimeException("SERVER_HOST not found")
        val serverPort = settingsRepository.getSetting(Settings.SERVER_PORT.name)?.value?.toInt() ?: throw RuntimeException("SERVER_PORT not found")
        val username = settingsRepository.getSetting(Settings.FTP_USERNAME.name)?.value ?: throw RuntimeException("FTP_USERNAME not found")
        val password = settingsRepository.getSetting(Settings.FTP_PASSWORD.name)?.value ?: throw RuntimeException("FTP_PASSWORD not found")

        documentsRepository.findByReportId(reportId)?.let { documentEntity ->

            val ftpClient = FTPSClient()

            try {
                ftpClient.connect(InetAddress.getByName(serverHost), serverPort)
                if (ftpClient.login(username, password)) {
                    ftpClient.execPBSZ(0)
                    ftpClient.execPROT("P")
                    ftpClient.changeWorkingDirectory("./FTP/Raporty/")
                    Log.d("sendReport", "changeWorkingDirectory reply=" + ftpClient.replyString)
                    ftpClient.enterLocalPassiveMode()
                    ftpClient.setFileType(FTP.BINARY_FILE_TYPE)
                    val stream = FileInputStream(File(documentEntity.documentUrl))
                    val result = ftpClient.storeFile(documentEntity.getFileName(), stream)
                    Log.d("sendReport", "result=$result reply=" + ftpClient.replyString)
                    stream.close()
                    ftpClient.logout()
                    if(!result) {
                        throw Exception(ftpClient.replyString)
                    }
                    report.isDocumentSend = true
                }
                reportsRepository.updateReport(report)
            } catch (e: Throwable) {
                Log.e("sendReport", e.message ?: "empty")
                e.printStackTrace()
                throw IOException(e.message)
            } finally {
                ftpClient.disconnect()
            }

        }

        return@withContext
    }
}