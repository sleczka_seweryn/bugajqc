package com.bugaj.qc.app.provider.room

import androidx.lifecycle.LiveData
import com.bugaj.qc.app.service.TemplateRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class RoomTemplateRepository @Inject constructor(
    private val templateDao: TemplateDao,
    private val ioDispatcher: CoroutineDispatcher
) : TemplateRepository {

    override fun observeList(): LiveData<List<TemplateEntity>> {
        return templateDao.observeList()
    }

    override suspend fun list(): List<TemplateEntity> = withContext(ioDispatcher) {
        templateDao.list()
    }

    override suspend fun load(id: String): TemplateEntity? = withContext(ioDispatcher) {
        templateDao.load(id)
    }

    override suspend fun save(entity: TemplateEntity) = withContext(ioDispatcher) {
        templateDao.insert(entity)
    }

    override suspend fun delete(entity: TemplateEntity) = withContext(ioDispatcher) {
        templateDao.delete(entity)
    }

    override suspend fun delete(id: String) = withContext(ioDispatcher) {
        templateDao.delete(id)
    }

    override suspend fun getLastOrder(): Int  = withContext(ioDispatcher) {
        templateDao.getLastOrder()
    }
}