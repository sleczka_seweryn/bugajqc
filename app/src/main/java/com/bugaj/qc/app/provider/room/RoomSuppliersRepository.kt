package com.bugaj.qc.app.provider.room

import com.bugaj.qc.app.service.SuppliersRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class RoomSuppliersRepository @Inject constructor(
    private val suppliersDao: SuppliersDao,
    private val ioDispatcher: CoroutineDispatcher
) : SuppliersRepository {

    override suspend fun load(name: String): SupplierEntity? = withContext(ioDispatcher) {
        suppliersDao.load(name)
    }

    override suspend fun list(): List<SupplierEntity> = withContext(ioDispatcher) {
        suppliersDao.list()
    }

    override suspend fun save(entity: SupplierEntity) = withContext(ioDispatcher) {
        suppliersDao.insert(entity)
    }
}