package com.bugaj.qc.app.application.navigation

class DismissDialogAction : NavigateAction {

    override fun accept(navigation: NavigateManager) {
        navigation.visit(this)
    }
}