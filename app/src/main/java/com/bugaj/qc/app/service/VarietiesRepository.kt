package com.bugaj.qc.app.service

import com.bugaj.qc.app.provider.room.VarietyEntity

interface VarietiesRepository {

    suspend fun load(name: String): VarietyEntity?

    suspend fun list(): List<VarietyEntity>

    suspend fun save(entity: VarietyEntity)

}