package com.bugaj.qc.app.service

import androidx.lifecycle.LiveData
import com.bugaj.qc.app.provider.room.TemplateEntity

interface TemplateRepository {

    fun observeList(): LiveData<List<TemplateEntity>>

    suspend fun list(): List<TemplateEntity>

    suspend fun load(id: String): TemplateEntity?

    suspend fun save(entity: TemplateEntity)

    suspend fun delete(entity: TemplateEntity)

    suspend fun delete(id: String)

    suspend fun getLastOrder(): Int

}