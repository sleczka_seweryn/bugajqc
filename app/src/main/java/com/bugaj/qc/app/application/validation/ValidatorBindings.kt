package com.bugaj.qc.app.application.validation

import android.widget.EditText
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener

@BindingAdapter("validator")
fun setValidator(view: ValidatorLayout, value: ValidationData?) {
    view.setValidationData(value)
}

@BindingAdapter("error")
fun setValidator(view: ValidatorLayout, value: String?) {
    view.setValidationData(ValidationData(message = value))
}

@BindingAdapter("onFocusLost")
fun onFocusLost(view: EditText, value: Boolean?) { }

@InverseBindingAdapter(attribute = "onFocusLost", event = "onFocusLostAttrChanged")
fun getItem(view: EditText): Boolean? {
    return view.isFocused
}

@BindingAdapter(value = ["onFocusLostAttrChanged"], requireAll = false)
fun bindSelectedItemAttrChanged(view: EditText, listener: InverseBindingListener) {
    view.setOnFocusChangeListener { v, hasFocus -> listener.onChange() }
}