package com.bugaj.qc.app.service

import androidx.lifecycle.LiveData
import com.bugaj.qc.app.provider.room.ImageEntity

interface ImagesRepository {

    suspend fun load(id: String): ImageEntity?

    suspend fun save(entity: ImageEntity)

    fun observeReportImages(reportId: String): LiveData<List<ImageEntity>>

    suspend fun deleteImage(imageId: String)

    suspend fun findWhereReportId(reportId: String): List<ImageEntity>

}