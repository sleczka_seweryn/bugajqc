package com.bugaj.qc.app.admin

import javax.inject.Inject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.bugaj.qc.app.R
import com.bugaj.qc.app.admin.navigation.AdminAuthArgs
import com.bugaj.qc.app.application.arch.BaseViewModel
import com.bugaj.qc.app.application.common.StringRepository
import com.bugaj.qc.app.application.navigation.DismissDialogAction
import com.bugaj.qc.app.application.navigation.StartActivityAction
import com.bugaj.qc.app.service.AuthService
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.lang.IllegalStateException

@HiltViewModel
class AdminAuthDialogViewModel @Inject constructor (
    private val strings: StringRepository,
    private val authService: AuthService,
    private val savedStateHandle: SavedStateHandle
) : BaseViewModel() {

    private val redirectAction: StartActivityAction = savedStateHandle[AdminAuthArgs.ARG_REDIRECT_ACTION]
        ?: throw IllegalStateException("redirect action not found")

    val password = MutableLiveData<String>().apply { value = null }

    val authError = MutableLiveData<String>().apply { value = null }

    fun onClickSubmit() {
        viewModelScope.launch {
            showLoader()
            if( authService.login(password.value ?: "")) {
                navigate(redirectAction)
                navigate(DismissDialogAction())
            } else {
                authError.value = strings[R.string.error_wrong_password]
                hideLoader()
            }
        }
    }

    fun onClickCancel() {
        navigate(DismissDialogAction())
    }

}