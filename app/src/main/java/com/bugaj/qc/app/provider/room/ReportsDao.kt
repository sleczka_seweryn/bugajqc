package com.bugaj.qc.app.provider.room

import androidx.lifecycle.LiveData
import androidx.room.*
import java.util.*

@Dao
interface ReportsDao {

    @Query("SELECT * FROM reports")
    fun observeReports(): LiveData<List<ReportEntity>>

    @Query("SELECT * FROM reports WHERE isDocumentSend = 0")
    fun observeReportsWhereStateNew(): LiveData<List<ReportEntity>>

    @Query("SELECT * FROM reports WHERE isDocumentCreated = 1 AND isDocumentSend = 1")
    fun observeReportsWhereStateSend(): LiveData<List<ReportEntity>>

    @Query("SELECT * FROM reports WHERE id = :id")
    fun observeReportById(id: String): LiveData<ReportEntity?>

    @Query("SELECT * FROM reports")
    suspend fun getReports(): List<ReportEntity>

    @Query("SELECT * FROM reports WHERE id = :id")
    suspend fun getReportById(id: String): ReportEntity?

    @Query("SELECT * FROM reports WHERE isDocumentSend = 1 AND creationDate BETWEEN :from AND :to")
    suspend fun getReportsWhereCreationDateBetween(from: Date, to: Date): List<ReportEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertReport(report: ReportEntity)

    @Update
    suspend fun updateReport(report: ReportEntity): Int

    @Delete
    suspend fun deleteReport(report: ReportEntity)

    @Query("DELETE FROM reports WHERE id = :id")
    suspend fun deleteReportWhereId(id: String)

}