package com.bugaj.qc.app.loading

import com.bugaj.qc.app.application.arch.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class LoadingViewModel @Inject constructor () : BaseViewModel()