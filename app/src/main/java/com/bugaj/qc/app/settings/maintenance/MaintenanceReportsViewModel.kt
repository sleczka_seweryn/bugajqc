package com.bugaj.qc.app.settings.maintenance

import androidx.lifecycle.viewModelScope
import com.bugaj.qc.app.application.arch.BaseViewModel
import com.bugaj.qc.app.application.navigation.DismissDialogAction
import com.bugaj.qc.app.service.MaintenanceService
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MaintenanceReportsViewModel @Inject constructor (
    private val maintenanceService: MaintenanceService
) : BaseViewModel() {

    fun onClickConfirm() {
        viewModelScope.launch {
            showLoader()
            maintenanceService.deleteOldReports()
            hideLoader()
            navigate(DismissDialogAction())
        }
    }

    fun onClickCancel() {
        navigate(DismissDialogAction())
    }

}