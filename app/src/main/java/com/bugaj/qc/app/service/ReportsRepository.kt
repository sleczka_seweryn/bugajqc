package com.bugaj.qc.app.service

import androidx.lifecycle.LiveData
import com.bugaj.qc.app.dashboard.adapter.ReportFilterType
import com.bugaj.qc.app.provider.room.ReportEntity
import java.util.*

interface ReportsRepository {

    fun observeReports(): LiveData<List<ReportEntity>>

    fun observeReports(filterType: ReportFilterType): LiveData<List<ReportEntity>>

    suspend fun getReports(): List<ReportEntity>

    fun observeReport(id: String): LiveData<ReportEntity?>

    suspend fun getReport(id: String): ReportEntity?

    suspend fun saveReport(reportEntity: ReportEntity)

    suspend fun updateReport(reportEntity: ReportEntity): Int

    suspend fun deleteReport(reportEntity: ReportEntity)

    suspend fun deleteReportWhereId(id: String)

    suspend fun findWhereCreationDateBetween(from: Date, to: Date): List<ReportEntity>

}