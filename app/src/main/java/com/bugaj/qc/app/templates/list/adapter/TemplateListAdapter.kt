package com.bugaj.qc.app.templates.list.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.bugaj.qc.app.templates.list.TemplatesListViewModel

class TemplateListAdapter constructor(
    private val viewModel: TemplatesListViewModel
) : ListAdapter<TemplateItem, TemplateViewHolder>(TemplateDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TemplateViewHolder {
        return TemplateViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: TemplateViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(viewModel, item)
    }
}