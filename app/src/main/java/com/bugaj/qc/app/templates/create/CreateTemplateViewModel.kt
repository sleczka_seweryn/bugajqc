package com.bugaj.qc.app.templates.create

import javax.inject.Inject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.bugaj.qc.app.R
import com.bugaj.qc.app.application.arch.BaseViewModel
import com.bugaj.qc.app.application.common.StringRepository
import com.bugaj.qc.app.application.navigation.FinishActivityAction
import com.bugaj.qc.app.application.validation.LengthValidator
import com.bugaj.qc.app.application.validation.NotEmptyValidator
import com.bugaj.qc.app.application.validation.OptionalValidator
import com.bugaj.qc.app.application.validation.ValidationManager
import com.bugaj.qc.app.provider.room.TemplateEntity
import com.bugaj.qc.app.service.TemplateRepository
import com.bugaj.qc.app.templates.navigation.CreateTemplateArgs
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.util.*

@HiltViewModel
class CreateTemplateViewModel @Inject constructor(
    private val strings: StringRepository,
    private val templateRepository: TemplateRepository,
    private val validationManager: ValidationManager,
    private val savedStateHandle: SavedStateHandle
) : BaseViewModel() {

    private val fieldId: String? = savedStateHandle.get<String>(CreateTemplateArgs.ARG_FIELD_ID)

    private val _title = MutableLiveData<String>().apply { value = strings[R.string.add_field_template_title] }
    val title: LiveData<String> = _title

    val labelPl = MutableLiveData<String>().apply { value = null }

    val labelEn = MutableLiveData<String>().apply { value = null }

    val order = MutableLiveData<String>().apply { value = null }

    val labelPlValidation = validationManager.addValidator(labelPl, NotEmptyValidator(LengthValidator(min = 3, max = 30)))

    val labelPlOnFocusLost = labelPlValidation.bind()

    val labelEnValidation = validationManager.addValidator(labelEn, NotEmptyValidator(LengthValidator(min = 3, max = 30)))

    val labelEnOnFocusLost = labelEnValidation.bind()

    val orderValidation = validationManager.addValidator(order, OptionalValidator())

    val orderOnFocusLost = labelEnValidation.bind()

    init {
        viewModelScope.launch {
            if(fieldId != null) {
                templateRepository.load(fieldId)?.let { entity ->
                    labelPl.value = entity.labelPl
                    labelEn.value = entity.labelEn
                    order.value = entity.order.toString()
                    _title.value = strings[R.string.edit_field_template_title]
                }
            }
        }
    }

    fun onClickSave() {
        if(validationManager.validate()) {
            viewModelScope.launch {
                showLoader()
                val entity = TemplateEntity(
                    id = fieldId ?: UUID.randomUUID().toString(),
                    labelPl = labelPl.value!!,
                    labelEn = labelEn.value!!,
                    order = order.value?.toIntOrNull() ?: (templateRepository.getLastOrder() + 1)
                )
                templateRepository.save(entity)
                navigate(FinishActivityAction())
                hideLoader()
            }
        }
    }

    fun onClickBack() {
        navigate(FinishActivityAction())
    }

}