package com.bugaj.qc.app.application.common

data class SpinnerItem constructor (
    val text: String,
    val code: String,
    var isSelected: Boolean = false,
    var isEnabled: Boolean = true,
) {

    override fun toString(): String {
        return text;
    }

}