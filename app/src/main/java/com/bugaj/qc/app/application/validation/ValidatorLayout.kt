package com.bugaj.qc.app.application.validation

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.TextView
import com.bugaj.qc.app.R
import com.bugaj.qc.app.application.common.DropdownView

class ValidatorLayout @JvmOverloads constructor (
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private lateinit var validationView: ValidationView

    private var errorTextView: TextView

    private var container: FrameLayout

    init {
        val view = LayoutInflater.from(context).inflate(R.layout.validator_layout_view, this, false)
        errorTextView = view.findViewById(R.id.error_text_view)
        container = view.findViewById(R.id.container)
        addView(view)
    }

    override fun addView(child: View?, index: Int, params: ViewGroup.LayoutParams?) {
        if(child is EditText) {
            validationView = EditTextValidationView(child)
            container.addView(child, index, params)
        } else if (child is DropdownView) {
            validationView = DropdownValidationView(child)
            container.addView(child, index, params)
        } else if (child is Button) {
            validationView = ButtonValidationView(child)
            container.addView(child, index, params)
        }else {
            super.addView(child, index, params)
        }
    }

    fun setValidationData(data: ValidationData?) {
        validationView.setError(data?.message)
        if(data?.hasError() == true) {
            errorTextView.visibility = VISIBLE
            errorTextView.text = data.message
        } else {
            errorTextView.visibility = GONE
        }
        if(data?.useFocus == true) {
            validationView.focus()
        }
    }

}