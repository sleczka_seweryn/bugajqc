package com.bugaj.qc.app.provider.room

import android.content.Context
import android.content.res.Configuration
import android.content.res.Resources
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.bugaj.qc.app.R
import com.bugaj.qc.app.settings.model.Settings
import dagger.hilt.android.qualifiers.ApplicationContext
import java.util.*
import java.util.concurrent.Executors
import javax.inject.Inject


class BugajDatabaseCallback @Inject constructor(
    @ApplicationContext private val context: Context
) : RoomDatabase.Callback() {

    private lateinit var dataBase: BugajDatabase

    private val resPL = getResourceWithLocale(Locale("pl"))

    private val resEN = getResourceWithLocale(Locale.UK)

    fun setDataBase(dataBase: BugajDatabase) {
        this.dataBase = dataBase
    }

    override fun onCreate(db: SupportSQLiteDatabase) {

        Executors.newSingleThreadScheduledExecutor().execute(Runnable {
            val settingsDao = dataBase.settingsDao()

            settingsDao.insertBlocking(SettingEntity(Settings.SERVER_HOST.name, Settings.SERVER_HOST.defaultValue))

            val templateDao = dataBase.templateDao()
            templateDao.insertBlocking(arrayListOf<TemplateEntity>().apply {
                add(TemplateEntity(
                        labelPl = resPL.getString(R.string.report_param_mechanical_damage),
                        labelEn = resEN.getString(R.string.report_param_mechanical_damage),
                        order = 0
                    ))
                add(TemplateEntity(
                    labelPl = resPL.getString(R.string.report_param_greening),
                    labelEn = resEN.getString(R.string.report_param_greening),
                    order = 1
                ))
                add(TemplateEntity(
                    labelPl = resPL.getString(R.string.report_param_growth_cracks),
                    labelEn = resEN.getString(R.string.report_param_growth_cracks),
                    order = 2
                ))
                add(TemplateEntity(
                    labelPl = resPL.getString(R.string.report_param_pest_damages),
                    labelEn = resEN.getString(R.string.report_param_pest_damages),
                    order = 3
                ))
                add(TemplateEntity(
                    labelPl = resPL.getString(R.string.report_param_common_scarb),
                    labelEn = resEN.getString(R.string.report_param_common_scarb),
                    order = 4
                ))
                add(TemplateEntity(
                    labelPl = resPL.getString(R.string.report_param_bruising),
                    labelEn = resEN.getString(R.string.report_param_bruising),
                    order = 5
                ))
                add(TemplateEntity(
                    labelPl = resPL.getString(R.string.report_param_internal),
                    labelEn = resEN.getString(R.string.report_param_internal),
                    order = 6
                ))
                add(TemplateEntity(
                    labelPl = resPL.getString(R.string.report_param_white_coating_damages),
                    labelEn = resEN.getString(R.string.report_param_white_coating_damages),
                    order = 7
                ))
                add(TemplateEntity(
                    labelPl = resPL.getString(R.string.report_param_rot),
                    labelEn = resEN.getString(R.string.report_param_rot),
                    order = 8
                ))
                add(TemplateEntity(
                    labelPl = resPL.getString(R.string.report_param_stones),
                    labelEn = resEN.getString(R.string.report_param_stones),
                    order = 9
                ))
                add(TemplateEntity(
                    labelPl = resPL.getString(R.string.report_param_oversize),
                    labelEn = resEN.getString(R.string.report_param_oversize),
                    order = 10
                ))
                add(TemplateEntity(
                    labelPl = resPL.getString(R.string.report_param_other),
                    labelEn = resEN.getString(R.string.report_param_other),
                    order = 11
                ))
            })
        })
    }

    private fun getResourceWithLocale(locale: Locale): Resources {
        var conf = context.resources.configuration
        conf = Configuration(conf)
        conf.setLocale(locale)
        val localizedContext = context.createConfigurationContext(conf)
        return localizedContext.resources
    }

}