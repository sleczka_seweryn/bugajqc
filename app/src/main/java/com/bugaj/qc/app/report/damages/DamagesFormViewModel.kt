package com.bugaj.qc.app.report.damages

import javax.inject.Inject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.bugaj.qc.app.R
import com.bugaj.qc.app.application.arch.BaseViewModel
import com.bugaj.qc.app.application.common.StringRepository
import com.bugaj.qc.app.application.navigation.FinishActivityAction
import com.bugaj.qc.app.provider.room.DamageEntity
import com.bugaj.qc.app.provider.room.ReportEntity
import com.bugaj.qc.app.provider.room.TemplateEntity
import com.bugaj.qc.app.report.damages.adapter.DamageFormField
import com.bugaj.qc.app.report.damages.model.formatDamageValue
import com.bugaj.qc.app.report.damages.navigation.DamagesFormArgs
import com.bugaj.qc.app.report.damages.navigation.DisplayDamageInputDialogAction
import com.bugaj.qc.app.report.images.navigation.DisplayReportAddImagesActivityAction
import com.bugaj.qc.app.service.DamagesRepository
import com.bugaj.qc.app.service.ReportsRepository
import com.bugaj.qc.app.service.TemplateRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import kotlin.collections.ArrayList

@HiltViewModel
class DamagesFormViewModel @Inject constructor(
    private val stringRepository: StringRepository,
    private val reportsRepository: ReportsRepository,
    private val templatesRepository: TemplateRepository,
    private val damagesRepository: DamagesRepository,
    private val savedStateHandle: SavedStateHandle
) : BaseViewModel() {

    private val reportId: String = savedStateHandle.get<String>(DamagesFormArgs.ARG_REPORT_ID)
        ?: throw IllegalArgumentException("reportId not found")

    private val isOpenedForEdit: Boolean = savedStateHandle.get<Boolean?>(DamagesFormArgs.ARG_OPENED_FOR_EDIT) == true

    private val finishAfterCommit: Boolean = savedStateHandle.get<Boolean?>(DamagesFormArgs.ARG_FINISH_AFTER_COMMIT) == true

    private lateinit var report: ReportEntity

    private val _title = MutableLiveData<String>().apply { value = null }
    val title: LiveData<String> = _title

    private val _formFields = MutableLiveData<List<DamageFormField>>()
    val formFields: LiveData<List<DamageFormField>> = _formFields

    private val _error = MutableLiveData<String>().apply { value = null }
    val error: LiveData<String> = _error

    private val _totalDamages = MutableLiveData<String>().apply { value = "0kg" }
    val totalDamages: LiveData<String> = _totalDamages

    init {
        viewModelScope.launch {
            showLoader()

            report = reportsRepository.getReport(reportId) ?: throw IllegalArgumentException("report not found")

            if(isOpenedForEdit) {
                _title.value = stringRepository[R.string.damages_edit_form_title]
            } else {
                _title.value = stringRepository[R.string.damages_fill_form_title]
            }

            val reportFields: List<DamageEntity> = damagesRepository.listByReportId(reportId)
            val templateFields: List<TemplateEntity> = templatesRepository.list()

            val fields = ArrayList<DamageEntity>()

            fields.addAll(reportFields)

            templateFields.forEach { entity ->
                val found = fields.find { it.fieldId == entity.id }
                if(found == null) {
                    fields.add(
                        DamageEntity(
                            fieldId = entity.id,
                            reportId = reportId,
                            labelPl = entity.labelPl,
                            labelEn = entity.labelEn,
                            order = entity.order,
                            value = null
                        )
                    )
                } else {
                    found.update(entity)
                }
            }

            _formFields.value = fields.map { DamageFormField(
                entityId = it.id,
                fieldId = it.fieldId,
                labelPl = it.labelPl,
                labelEn = it.labelEn,
                order = it.order
            ).apply {
                setValue(it.value)
            } }.sortedBy {
                it.order
            }

            updateTotalDamages()

            hideLoader()
        }
    }

    fun onClickSave() {
        viewModelScope.launch {
            showLoader()
            val damagesList = _formFields.value!!.map { DamageEntity(
                id = it.entityId,
                fieldId = it.fieldId,
                reportId = reportId,
                labelPl = it.labelPl,
                labelEn = it.labelEn,
                order = it.order,
                value = it.getValue()
            ) }
            if(isOpenedForEdit) {
                damagesRepository.updateAll(damagesList)
            } else {
                damagesRepository.insertAll(damagesList)
            }
            hideLoader()
        }
        if(finishAfterCommit) {
            navigate(FinishActivityAction())
        } else {
            navigate(DisplayReportAddImagesActivityAction(
                reportId = reportId
            ))
        }
    }

    fun onClickEdit(field: DamageFormField) {
        navigate(
            DisplayDamageInputDialogAction(
                fieldId = field.fieldId,
                label = field.labelPl,
                actualValue = field.getValue()
            )
        )
    }

    fun onValueChanged(fieldId: String, value: Float?) {
        _formFields.value
            ?.find { it.fieldId == fieldId }
            ?.setValue(value)
        updateTotalDamages()
    }

    fun onClickBack() {
        navigate(FinishActivityAction())
    }

    fun formatDamageValue(value: Float): String = formatDamageValue(value, report.getSamplesCategory().weight)

    private fun updateTotalDamages() {
        val total: Float = _formFields.value?.map { it.getValue() ?: 0f }?.sum() ?: 0f
        _totalDamages.value = formatDamageValue(total, report.getSamplesCategory().weight)
        if(total > report.getSamplesCategory().weight) {
            _error.value = stringRepository[R.string.error_total_damages_over_limit]
        } else {
            _error.value = null
        }
    }

}