package com.bugaj.qc.app.report.details.images

import android.content.Context
import android.graphics.BitmapFactory
import android.util.TypedValue
import android.widget.ImageView
import androidx.annotation.Dimension
import com.bugaj.qc.app.provider.room.ImageEntity

class ImageDecoder constructor(
    private val context: Context
) {

    fun loadImageForView(imageView: ImageView, image: ImageEntity) {
        val targetW: Int = dpToPx(context, 100).toInt()
        val targetH: Int = dpToPx(context, 100).toInt()

        val bmOptions = BitmapFactory.Options().apply {
            inJustDecodeBounds = true

            BitmapFactory.decodeFile(image.imageUrl, this)

            val photoW: Int = outWidth
            val photoH: Int = outHeight

            val scaleFactor: Int = Math.max(1, Math.min(photoW / targetW, photoH / targetH))

            inJustDecodeBounds = false
            inSampleSize = scaleFactor
            inPurgeable = true
        }
        BitmapFactory.decodeFile(image.imageUrl, bmOptions)?.also { bitmap ->
            imageView.setImageBitmap(bitmap)
        }
    }

    fun dpToPx(context: Context, @Dimension(unit = Dimension.DP) dp: Int): Float {
        val r = context.resources
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            dp.toFloat(),
            r.displayMetrics
        )
    }

}