package com.bugaj.qc.app.service

import java.io.IOException
import kotlin.jvm.Throws

interface UploadService {

    @Throws(IOException::class)
    suspend fun sendReport(reportId: String)

}