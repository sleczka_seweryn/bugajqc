package com.bugaj.qc.app.provider.room

import androidx.lifecycle.LiveData
import com.bugaj.qc.app.service.DamagesRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class RoomDamagesRepository @Inject constructor(
    private val damagesDao: DamagesDao,
    private val ioDispatcher: CoroutineDispatcher
) : DamagesRepository {

    override suspend fun find(id: String): DamageEntity? = withContext(ioDispatcher) {
        damagesDao.find(id)
    }

    override suspend fun listByReportId(reportId: String): List<DamageEntity> = withContext(ioDispatcher) {
        damagesDao.listByReportId(reportId)
    }

    override fun observeWhereReportId(reportId: String): LiveData<List<DamageEntity>> {
        return damagesDao.observeWhereReportId(reportId)
    }

    override suspend fun insert(image: DamageEntity) = withContext(ioDispatcher) {
        damagesDao.insert(image)
    }

    override suspend fun delete(id: String) = withContext(ioDispatcher) {
        damagesDao.delete(id)
    }

    override suspend fun insertAll(dataList: List<DamageEntity>) = withContext(ioDispatcher) {
        damagesDao.insertAll(dataList)
    }

    override suspend fun updateAll(dataList: List<DamageEntity>) = withContext(ioDispatcher) {
        damagesDao.updateAll(dataList)
    }

}