package com.bugaj.qc.app.settings.option.change

import android.content.Context
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.bugaj.qc.app.R
import com.bugaj.qc.app.application.navigation.ActivityNavigateManager
import com.bugaj.qc.app.dashboard.adapter.ReportListAdapter
import com.bugaj.qc.app.databinding.OptionChangeActivityBinding
import com.bugaj.qc.app.settings.option.change.adapter.SettingsOptionAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OptionChangeActivity : AppCompatActivity() {

    private lateinit var binding: OptionChangeActivityBinding

    private val viewModel by viewModels<OptionChangeViewModel>()

    private lateinit var settingsAdapter: SettingsOptionAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.option_change_activity)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        settingsAdapter = SettingsOptionAdapter(viewModel)
        binding.reportsList.adapter = settingsAdapter

        viewModel.navigateAction.observe(this) {
            it?.accept(ActivityNavigateManager(this))
        }
    }

}