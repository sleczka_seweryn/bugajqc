package com.bugaj.qc.app.application.common

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import com.bugaj.qc.app.databinding.SettingItemBinding

class SettingView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private var binding: SettingItemBinding

    init {
        val inflater = LayoutInflater.from(context)
        binding = SettingItemBinding.inflate(inflater)
        binding.icon.visibility = GONE
        addView(binding.root)
    }

    fun setLabel(text: String) {
        binding.label.text = text
    }

    fun setValue(text: String) {
        binding.value.text = text
    }

    fun setIcon(iconResId: Int?) {
        if(iconResId == null) {
            binding.icon.visibility = GONE
        } else {
            binding.icon.visibility = VISIBLE
            binding.icon.setImageResource(iconResId)
        }
    }

    override fun setOnClickListener(l: OnClickListener?) {
        binding.layout.setOnClickListener(l)
    }

}