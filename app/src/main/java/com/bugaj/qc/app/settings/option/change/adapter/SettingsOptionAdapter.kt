package com.bugaj.qc.app.settings.option.change.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.bugaj.qc.app.settings.option.change.OptionChangeViewModel

class SettingsOptionAdapter constructor(
    private val viewModel: OptionChangeViewModel
) : ListAdapter<SettingsOptionItem, SettingsOptionViewHolder>(SettingsOptionDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SettingsOptionViewHolder {
        return SettingsOptionViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: SettingsOptionViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(viewModel, item)
    }
}