package com.bugaj.qc.app.settings.maintenance.navigation

import android.content.Context
import com.bugaj.qc.app.application.navigation.DisplayDialogAction
import com.bugaj.qc.app.settings.maintenance.MaintenanceReportsDialog

class DisplayMaintenanceReportAction constructor() : DisplayDialogAction() {

    override fun createDialog(context: Context) = MaintenanceReportsDialog()

}