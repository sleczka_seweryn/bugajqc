package com.bugaj.qc.app.report.damages.adapter

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

data class DamageFormField constructor (
    val entityId: String,
    val fieldId: String,
    val labelPl: String,
    val labelEn: String,
    val order: Int
) {

    private val _value = MutableLiveData<Float?>().apply { value = null }

    fun setValue(value: Float?) {
        _value.value = value
    }

    fun getValue(): Float? = _value.value

    fun getValueLiveData(): LiveData<Float?> = _value

}