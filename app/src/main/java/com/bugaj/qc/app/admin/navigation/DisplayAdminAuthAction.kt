package com.bugaj.qc.app.admin.navigation

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.bugaj.qc.app.admin.AdminAuthDialogFragment
import com.bugaj.qc.app.application.navigation.DisplayDialogAction
import com.bugaj.qc.app.application.navigation.StartActivityAction

class DisplayAdminAuthAction constructor (
    val redirectAction: StartActivityAction
): DisplayDialogAction() {

    override fun createDialog(context: Context): DialogFragment = AdminAuthDialogFragment().apply {
        arguments = Bundle().apply {
            putSerializable(AdminAuthArgs.ARG_REDIRECT_ACTION, redirectAction)
        }
    }

}