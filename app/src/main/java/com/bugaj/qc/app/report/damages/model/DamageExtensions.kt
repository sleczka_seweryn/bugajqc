package com.bugaj.qc.app.report.damages.model

import com.bugaj.qc.app.provider.room.DamageEntity
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*

fun formatDamageValue(value: Float, total: Float): String {
    if(value == 0f) {
        return "0kg"
    }
    val decimalFormat = DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH))
    decimalFormat.maximumFractionDigits = 2
    decimalFormat.minimumFractionDigits = 2
    val damageValue = decimalFormat.format(value)
    val percentage = decimalFormat.format(((value / total) * 100).toFloat())
    return "${damageValue}kg(${percentage}%)".replace('.',',')
}

fun DamageEntity.formatDamage(total: Float): String? {
    if(value == 0f || value == null) {
        return null
    }
    val decimalFormat = DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH))
    decimalFormat.maximumFractionDigits = 2
    decimalFormat.minimumFractionDigits = 2
    val percentage = decimalFormat.format((((value?:0f) / total) * 100).toFloat())
    return "${percentage}%".replace('.',',')
}

fun formatDamage(value: Float?, total: Float): String? {
    if(value == null) {
        return "0%"
    }
    if(value == 0f) {
        return null
    }
    val decimalFormat = DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH))
    decimalFormat.maximumFractionDigits = 2
    decimalFormat.minimumFractionDigits = 2
    val percentage = decimalFormat.format((((value?:0f) / total) * 100).toFloat())
    return "${percentage}%".replace('.',',')
}
