package com.bugaj.qc.app.loading

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import com.bugaj.qc.app.application.navigation.DialogNavigateManager
import com.bugaj.qc.app.databinding.DocumentCreateDialogFragmentBinding
import com.bugaj.qc.app.databinding.LoadingDialogBinding
import com.bugaj.qc.app.report.document.CreateDocumentViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoadingDialog constructor() : DialogFragment() {

    private val viewModel by viewModels<LoadingViewModel>()

    private lateinit var binding: LoadingDialogBinding

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let { activity ->

            val inflater = requireActivity().layoutInflater;
            binding = LoadingDialogBinding.inflate(inflater)
            binding.lifecycleOwner = this
            binding.viewModel = viewModel

            val builder = AlertDialog.Builder(activity)
            builder.setView(binding.root)
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

}