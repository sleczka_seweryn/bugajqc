package com.bugaj.qc.app.provider.room

import androidx.room.*
import java.util.*

@Entity(
    tableName = "damages",
    foreignKeys = [ForeignKey(
        entity = ReportEntity::class,
        parentColumns = ["id"],
        childColumns = ["reportId"],
        onDelete = ForeignKey.CASCADE,
    )],
    indices = [Index(value = ["reportId"], unique = true)]
)
class DamageEntity constructor(
    @PrimaryKey @ColumnInfo(name = "id") var id: String = UUID.randomUUID().toString(),
    @ColumnInfo(name = "fieldId") var fieldId: String,
    @ColumnInfo(name = "reportId") var reportId: String,
    @ColumnInfo(name = "labelPl") var labelPl: String,
    @ColumnInfo(name = "labelEn") var labelEn: String,
    @ColumnInfo(name = "value") var value: Float?,
    @ColumnInfo(name = "order") var order: Int
) {

    fun update(templateEntity: TemplateEntity) {
        labelPl = templateEntity.labelPl
        labelEn = templateEntity.labelEn
    }

}