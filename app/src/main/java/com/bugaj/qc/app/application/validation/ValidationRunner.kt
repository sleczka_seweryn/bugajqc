package com.bugaj.qc.app.application.validation

import androidx.lifecycle.LiveData
import com.bugaj.qc.app.application.common.SpinnerItem
import com.bugaj.qc.app.application.common.StringRepository

interface ValidationRunner {

    fun isValid(): Boolean

    fun validate(): String?

}

class ValidationRunnerString constructor (
    private val strings: StringRepository,
    private val liveData: LiveData<String>,
    private val validator: Validator<String>
) : ValidationRunner {

    override fun isValid(): Boolean {
        return validate()?.isEmpty() ?: true
    }

    override fun validate(): String? {
        return validator.validate(strings, liveData.value)
    }
}

class ValidationRunnerSpinner constructor (
    private val strings: StringRepository,
    private val liveData: LiveData<SpinnerItem>,
    private val validator: Validator<SpinnerItem>
) : ValidationRunner {

    override fun isValid(): Boolean {
        return validate()?.isEmpty() ?: true
    }

    override fun validate(): String? {
        return validator.validate(strings, liveData.value)
    }
}