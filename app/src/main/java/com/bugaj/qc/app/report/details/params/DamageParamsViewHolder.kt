package com.bugaj.qc.app.report.details.params

import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bugaj.qc.app.databinding.DamageParamItemBinding
import com.bugaj.qc.app.report.details.ReportDetailsViewModel

class DamageParamsViewHolder private constructor(
    val binding: DamageParamItemBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(viewModel: ReportDetailsViewModel, item: DamageParamItem) {
        binding.viewModel = viewModel
        binding.item = item
        binding.executePendingBindings()
        binding.label.text = item.label
        if(item.bold) {
            binding.label.setTypeface(binding.label.typeface, Typeface.BOLD)
        } else {
            binding.label.setTypeface(binding.label.typeface, Typeface.NORMAL)
        }
    }

    companion object {

        fun from(parent: ViewGroup): DamageParamsViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = DamageParamItemBinding.inflate(layoutInflater, parent, false)
            return DamageParamsViewHolder(binding)
        }

    }

}