package com.bugaj.qc.app.templates.create

import android.content.Context
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.bugaj.qc.app.R
import com.bugaj.qc.app.application.navigation.ActivityNavigateManager
import com.bugaj.qc.app.databinding.TemplateCreateActivityBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CreateTemplateActivity : AppCompatActivity() {

    private lateinit var binding: TemplateCreateActivityBinding

    private val viewModel by viewModels<CreateTemplateViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(
            this, R.layout.template_create_activity)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        viewModel.isLoading.observe(this, Observer {
            if(it == true) {
                hideKeyboard(this)
            }
        })

        viewModel.navigateAction.observe(this) {
            it?.accept(ActivityNavigateManager(this))
        }
    }

    override fun onBackPressed() {
        viewModel.onClickBack()
    }

    private fun hideKeyboard(context: Context) {
        val manager: InputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        manager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
    }

}