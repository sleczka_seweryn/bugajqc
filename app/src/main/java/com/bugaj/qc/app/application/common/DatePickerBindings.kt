package com.bugaj.qc.app.application.common

import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import androidx.appcompat.widget.AppCompatSpinner
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import java.util.*


@BindingAdapter("minDate")
fun minDate(view: DatePickerField, value:Date?) {
    value?.let { view.setMinDate(it) }
}

@BindingAdapter("maxDate")
fun maxDate(view: DatePickerField, value: Date?) {
    value?.let { view.setMaxDate(it) }
}

@BindingAdapter("date")
fun setItem(view: DatePickerField, value: Date?) {
    if(value?.time != view.getDate()?.time) {
        view.setDate(value)
    }
}

@InverseBindingAdapter(attribute = "date", event = "dateAttrChanged")
fun getItem(view: DatePickerField): Date? {
    return view.getDate()
}

@BindingAdapter(value = ["dateAttrChanged"], requireAll = false)
fun bindSelectedItemAttrChanged(view: DatePickerField, listener: InverseBindingListener) {
    view.setOnDateChanged{ listener.onChange() }
}
