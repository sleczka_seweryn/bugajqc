package com.bugaj.qc.app.settings.host

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.bugaj.qc.app.application.arch.BaseViewModel
import com.bugaj.qc.app.application.navigation.FinishActivityAction
import com.bugaj.qc.app.application.validation.NotEmptyValidator
import com.bugaj.qc.app.application.validation.ValidationManager
import com.bugaj.qc.app.provider.room.SettingEntity
import com.bugaj.qc.app.service.SettingsRepository
import com.bugaj.qc.app.settings.model.Settings
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HostSettingsViewModel @Inject constructor(
    private val validationManager: ValidationManager,
    private val settingsRepository: SettingsRepository
) : BaseViewModel() {

    val serverAddress = MutableLiveData<String>().apply { value = "" }

    val serverAddressValidator = validationManager.addValidator(serverAddress, NotEmptyValidator())//UrlValidator()

    init {
        viewModelScope.launch {
            serverAddress.value = settingsRepository.getSetting(Settings.SERVER_HOST.name)?.value
        }
    }

    fun onClickChange() {
        if(validationManager.validate()) {
            viewModelScope.launch {
                showLoader()
                settingsRepository.saveSetting(SettingEntity(
                    name = Settings.SERVER_HOST.name,
                    value = serverAddress.value!!
                ))
                navigate(FinishActivityAction())
            }
        }
    }

    fun onClickBack() {
        navigate(FinishActivityAction())
    }

}