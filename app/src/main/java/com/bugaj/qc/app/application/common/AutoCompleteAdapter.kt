package com.bugaj.qc.app.application.common

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import com.bugaj.qc.app.R
import java.lang.IllegalStateException

class AutoCompleteAdapter constructor(
    context: Context
) : ArrayAdapter<SpinnerItem>(context, R.layout.spinner_dropdown_item) {

    override fun getItemId(position: Int): Long = getItem(position).hashCode().toLong()

    fun update(data: List<SpinnerItem>) {
        clear()
        addAll(data)
        notifyDataSetChanged()
    }

    fun seSelectedItemPosition(position: Int) {
        getDataList().forEach {
            it.isSelected = false
        }
        getDataList()[position].isSelected = true
    }

    fun getItemPosition(item: SpinnerItem): Int {
        getDataList().forEachIndexed { index, data ->
            if(item.code == data.code) {
                return index
            }
        }
        return 0
    }

    fun getItemWhereText(text: String) : SpinnerItem? {
        return getDataList().find { it.text == text }
    }

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return createView(position, convertView, parent)
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return createView(position, convertView, parent)
    }

    private fun createView(position: Int, convertView: View?, parent: ViewGroup): View {
        val inflater: LayoutInflater = LayoutInflater.from(context)
        val row: View = inflater.inflate(
            R.layout.spinner_dropdown_item, parent,
            false
        )
        val label = row.findViewById(R.id.label) as TextView
        val icon = row.findViewById(R.id.icon) as ImageView
        val item = getItem(position) ?: throw IllegalStateException("item not found")
        label.text = item.text
        label.setTextColor(ContextCompat.getColor(context, R.color.text_dark))
        label.typeface = Typeface.DEFAULT
        icon.visibility = View.INVISIBLE
        return row
    }

    private fun getDataList(): List<SpinnerItem> {
        return arrayListOf<SpinnerItem>().apply {
            for(i in 0 until count ) {
                add(getItem(i) ?: throw IllegalStateException("item not found"))
            }
        }
    }

}