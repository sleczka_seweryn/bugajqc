package com.bugaj.qc.app.provider.room

import androidx.room.*

@Dao
interface SuppliersDao {

    @Query("SELECT * FROM suppliers WHERE name = :name")
    fun load(name: String): SupplierEntity?

    @Query("SELECT * FROM suppliers")
    suspend fun list(): List<SupplierEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(setting: SupplierEntity)

}