package com.bugaj.qc.app.settings.option.change

import javax.inject.Inject
import androidx.lifecycle.*
import com.bugaj.qc.app.application.arch.BaseViewModel
import com.bugaj.qc.app.application.common.StringRepository
import com.bugaj.qc.app.application.navigation.FinishActivityAction
import com.bugaj.qc.app.service.SettingsRepository
import com.bugaj.qc.app.settings.edit.navigation.DisplayEditSettingsArgs
import com.bugaj.qc.app.settings.model.Settings
import com.bugaj.qc.app.settings.option.change.adapter.SettingsOptionItem
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.lang.IllegalStateException

@HiltViewModel
class OptionChangeViewModel @Inject constructor(
    private val strings: StringRepository,
    private val settingsRepository: SettingsRepository,
    private val savedStateHandle: SavedStateHandle
) : BaseViewModel() {

    private val settingType: Settings = savedStateHandle[DisplayEditSettingsArgs.SettingTypeArg]
        ?: throw IllegalStateException("SettingTypeArg action not found")

    val settingLabel = MutableLiveData<String>().apply {
        value = strings[settingType.labelResId]
    }

    private val _items = MutableLiveData<List<SettingsOptionItem>>()
    val items: LiveData<List<SettingsOptionItem>> = _items

    init {
        viewModelScope.launch {
            showLoader()
            settingsRepository.getSetting(settingType.name)?.let { setting ->
                _items.value = arrayListOf<SettingsOptionItem>().apply{
                    settingType.options.forEach { option ->
                        add(
                            SettingsOptionItem(
                            name = strings[option.labelResId],
                            value = option.value,
                            isSelected = option.value == setting.value
                        ))
                    }
                }
            }
            hideLoader()
        }
    }

    fun onSelectOption(value: String) {
        _items.value = arrayListOf<SettingsOptionItem>().apply {
            _items.value?.forEach { item ->
                add(
                    SettingsOptionItem(
                        name = item.name,
                        value = item.value,
                        isSelected = item.value == value
                ))
            }
        }
        viewModelScope.launch {
            settingsRepository.getSetting(settingType.name)?.let { settingEntity ->
                settingEntity.value = value
                settingsRepository.saveSetting(settingEntity)
            }
        }
    }

    fun onClickBack() {
        navigate(FinishActivityAction())
    }

}