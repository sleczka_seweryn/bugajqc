package com.bugaj.qc.app.service

interface AuthService {
    suspend fun login(password: String): Boolean
}