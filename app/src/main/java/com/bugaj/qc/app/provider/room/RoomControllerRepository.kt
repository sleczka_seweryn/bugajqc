package com.bugaj.qc.app.provider.room

import com.bugaj.qc.app.service.ControllerRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class RoomControllerRepository @Inject constructor(
    private val controllerDao: ControllerDao,
    private val ioDispatcher: CoroutineDispatcher
) : ControllerRepository {

    override suspend fun load(name: String): ControllerEntity? = withContext(ioDispatcher) {
        controllerDao.load(name)
    }

    override suspend fun list(): List<ControllerEntity> = withContext(ioDispatcher) {
        controllerDao.list()
    }

    override suspend fun save(entity: ControllerEntity)  = withContext(ioDispatcher) {
        controllerDao.insert(entity)
    }
}