package com.bugaj.qc.app.application.common

import android.app.DatePickerDialog
import android.content.Context
import android.util.AttributeSet
import android.widget.DatePicker
import androidx.appcompat.widget.AppCompatTextView
import java.text.SimpleDateFormat
import java.util.*

class DatePickerField @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : AppCompatTextView(context, attrs, defStyleAttr), DatePickerDialog.OnDateSetListener {

    private var selectedDate: Date? = Date()

    private var minDate: Date? = null

    private var maxDate: Date? = null

    private var onDateChangedListener: (()->Unit)? =  null

    init {
        isClickable = true
        isFocusable = true
        setOnClickListener { displayDatePicker() }
        displayDate(selectedDate)
    }

    private fun displayDatePicker() {
        val calendar = Calendar.getInstance().apply {
            setDate(selectedDate ?: Date() )
        }
        val day = calendar[Calendar.DAY_OF_MONTH]
        val month = calendar[Calendar.MONTH]
        val year = calendar[Calendar.YEAR]
        val dialog = DatePickerDialog(context, this, year, month, day)
        dialog.datePicker.let { datePicker ->
            maxDate?.let { datePicker.maxDate = it.time }
            minDate?.let { datePicker.minDate = it.time }
        }
        dialog.show()
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        val calendar = Calendar.getInstance().apply {
            set(year, month, dayOfMonth)
        }
        selectedDate = calendar.time
        displayDate(selectedDate)
        onDateChangedListener?.invoke()
    }

    private fun displayDate(selectedDate: Date?) {
        if(selectedDate == null) {
            text = ""
        } else {
            text = SimpleDateFormat("dd.MM.YYYY", Locale.getDefault()).format(selectedDate)
        }
    }

    fun setMaxDate(date: Date) {
        maxDate = date
    }

    fun setMinDate(date: Date) {
        minDate = date
    }

    fun setOnDateChanged(listener: ()->Unit) {
        onDateChangedListener = listener
    }

    fun setDate(date: Date?) {
        selectedDate = date
        displayDate(selectedDate)
    }

    fun getDate(): Date? = selectedDate

}