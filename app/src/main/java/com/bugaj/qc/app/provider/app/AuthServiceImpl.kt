package com.bugaj.qc.app.provider.app

import com.bugaj.qc.app.service.AuthService
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class AuthServiceImpl @Inject constructor(
    private val ioDispatcher: CoroutineDispatcher
) : AuthService {

    private val adminPassword = "09092020"

    override suspend fun login(password: String): Boolean = withContext(ioDispatcher) {
        Thread.sleep(500)
        password.equals(adminPassword)
    }

}