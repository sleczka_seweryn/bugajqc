package com.bugaj.qc.app.application.validation

import com.bugaj.qc.app.R
import com.bugaj.qc.app.application.common.StringRepository

class LengthValidator constructor (
    private val min: Int,
    private val max: Int,
    private val validator: (Validator<String>)? = null
) : Validator<String> {

    override fun validate(strings: StringRepository, value: String?): String? {
        if(value?.length ?: 0 < min ) {
            return strings[R.string.error_min_length] + " " + min
        }
        if(value?.length ?: 0 > max ) {
            return strings[R.string.error_max_length] + " " + max
        }
        return validator?.validate(strings, value)
    }
}