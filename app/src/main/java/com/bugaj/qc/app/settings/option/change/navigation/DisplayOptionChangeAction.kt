package com.bugaj.qc.app.settings.option.change.navigation

import android.content.Context
import android.content.Intent
import com.bugaj.qc.app.application.navigation.StartActivityAction
import com.bugaj.qc.app.settings.model.Settings
import com.bugaj.qc.app.settings.option.change.OptionChangeActivity

class DisplayOptionChangeAction constructor(
    private val settingType: Settings
) : StartActivityAction(closeParent = false) {

    override fun createIntent(context: Context): Intent = Intent(context, OptionChangeActivity::class.java).apply {
        putExtra(DisplayOptionChangeArgs.SettingTypeArg, settingType)
    }

}