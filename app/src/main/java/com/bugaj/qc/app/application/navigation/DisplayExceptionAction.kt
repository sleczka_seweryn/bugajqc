package com.bugaj.qc.app.application.navigation

class DisplayExceptionAction constructor(
    val message: String
) : NavigateAction {

    override fun accept(navigation: NavigateManager) {
        navigation.visit(this)
    }
}