package com.bugaj.qc.app.templates.list.adapter

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView

@BindingAdapter("items")
fun setItems(listView: RecyclerView, items: List<TemplateItem>?) {
    items?.let {
        (listView.adapter as TemplateListAdapter).submitList(items)
    }
}
