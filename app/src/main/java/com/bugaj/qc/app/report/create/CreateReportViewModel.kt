package com.bugaj.qc.app.report.create

import androidx.lifecycle.*
import com.bugaj.qc.app.R
import com.bugaj.qc.app.application.arch.BaseViewModel
import com.bugaj.qc.app.application.common.AutoCompleteValue
import com.bugaj.qc.app.application.common.SpinnerItem
import com.bugaj.qc.app.application.common.StringRepository
import com.bugaj.qc.app.application.navigation.FinishActivityAction
import com.bugaj.qc.app.application.validation.*
import com.bugaj.qc.app.provider.room.ControllerEntity
import com.bugaj.qc.app.provider.room.ReportEntity
import com.bugaj.qc.app.provider.room.SupplierEntity
import com.bugaj.qc.app.provider.room.VarietyEntity
import com.bugaj.qc.app.report.create.navigation.CreateReportArgs
import com.bugaj.qc.app.report.create.samples.Samples
import com.bugaj.qc.app.report.damages.navigation.DisplayCreateDamagesFormAction
import com.bugaj.qc.app.service.ControllerRepository
import com.bugaj.qc.app.service.ReportsRepository
import com.bugaj.qc.app.service.SuppliersRepository
import com.bugaj.qc.app.service.VarietiesRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*
import javax.inject.Inject

@HiltViewModel
class CreateReportViewModel @Inject constructor(
    private val stringRepository: StringRepository,
    private val reportsRepository: ReportsRepository,
    private val suppliersRepository: SuppliersRepository,
    private val varietiesRepository: VarietiesRepository,
    private val controllerRepository: ControllerRepository,
    private val validator: ValidationManager,
    private val savedStateHandle: SavedStateHandle
) : BaseViewModel() {

    private val reportId: String? = savedStateHandle[CreateReportArgs.ARG_REPORT_ID]

    val title = MutableLiveData<String>(stringRepository[R.string.report_form_create_report])

    private val samplesTypes: List<SpinnerItem> = arrayListOf<SpinnerItem>().apply {
        add(
            SpinnerItem(
                text = stringRepository[R.string.spinner_default_label],
                code = "default",
                isEnabled = false
            )
        )
        Samples.values().forEach { sample ->
            add(
                SpinnerItem(
                    text = stringRepository[sample.titleRasId],
                    code = sample.name
                )
            )
        }
    }

    val deliveryNumberValue = MutableLiveData<String>()
    val deliveryNumberValidation = validator.addValidator(
        deliveryNumberValue, NotEmptyValidator(
            LengthValidator(
                min = 1,
                max = 4
            )
        )
    )
    val deliveryNumberFocusChange = deliveryNumberValidation.bind()

    val minDate: LiveData<Date> = MutableLiveData<Date>(nowMinusYear())
    val maxDate: LiveData<Date> = MutableLiveData<Date>(Date())
    val selectedDate = MutableLiveData<Date>(Date())

    private val _supplierItems = MutableLiveData<List<SpinnerItem>>(arrayListOf())
    val supplierItems: LiveData<List<SpinnerItem>> = _supplierItems

    val supplierValue: MutableLiveData<AutoCompleteValue> = MutableLiveData(AutoCompleteValue())
    private val supplierValueText: LiveData<String> = supplierValue.map { it.text }
    val supplierValidation = validator.addValidator(
        supplierValueText, NotEmptyValidator(
            LengthValidator(
                min = 3,
                max = 40
            )
        )
    )
    val supplierFocusChange = supplierValidation.bind()

    private val _varietyItems = MutableLiveData<List<SpinnerItem>>(arrayListOf())
    val varietyItems: LiveData<List<SpinnerItem>> = _varietyItems

    val varietyValue: MutableLiveData<AutoCompleteValue> = MutableLiveData(AutoCompleteValue())
    private val varietyValueText: LiveData<String> = varietyValue.map { it.text }
    val varietyValidator = validator.addValidator(
        varietyValueText, NotEmptyValidator(
            LengthValidator(
                min = 3,
                max = 40
            )
        )
    )
    val varietyFocusLost = varietyValidator.bind()

    val controlledSamplesData: LiveData<List<SpinnerItem>> = MutableLiveData(samplesTypes)
    val controlledSamplesItem = MutableLiveData<SpinnerItem>().apply {
        if(reportId == null) {
            value = samplesTypes[0]
        }
    }

    private val controlledSamplesItemText: LiveData<String> = controlledSamplesItem.map { it.code }
    val controlledSamplesValidator = validator.addValidator(controlledSamplesItemText, SelectedValueValidator())

    val sizeMin = MutableLiveData<String>().apply { value = "40" }
    val sizeMax = MutableLiveData<String>().apply { value = "80" }

    val sizeMinValidation = validator.addValidator(sizeMin, NotEmptyValidator(NumberValidator(min = 20f, max = 80f)))
    val sizeMinFocusLost = sizeMinValidation.bind()

    val sizeMaxValidation = validator.addValidator(sizeMax, NotEmptyValidator(NumberValidator(min = 20f, max = 80f)))
    val sizeMaxFocusLost = sizeMaxValidation.bind()

    val temperatureMin = MutableLiveData<String>()
    val temperatureMax = MutableLiveData<String>()

    val temperatureMinValidation = validator.addValidator(temperatureMin, NotEmptyValidator(NumberValidator(min = -30f, max = 40f)))
    val temperatureMinFocusLost = temperatureMinValidation.bind()

    val temperatureMaxValidation = validator.addValidator(temperatureMax, NotEmptyValidator(NumberValidator(min = -30f, max = 40f)))
    val temperatureMaxFocusLost = temperatureMaxValidation.bind()

    private val _controllerItems = MutableLiveData<List<SpinnerItem>>(arrayListOf())
    val controllerItems: LiveData<List<SpinnerItem>> = _controllerItems

    val controllerValue: MutableLiveData<AutoCompleteValue> = MutableLiveData(AutoCompleteValue())
    private val controllerValueText: LiveData<String> = controllerValue.map { it.text }
    val controllerValidation = validator.addValidator(controllerValueText, NotEmptyValidator(LengthValidator(min = 3, max = 50)))
    val controllerFocusLost = controllerValidation.bind()

    init {

        val formatter = DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale("pl")))
        val formatter2 = DecimalFormat("#.#", DecimalFormatSymbols.getInstance(Locale("pl")))

        viewModelScope.launch {
            showLoader()
            _supplierItems.value = suppliersRepository.list().map {
                SpinnerItem(it.name, it.name)
            }
            _varietyItems.value = varietiesRepository.list().map {
                SpinnerItem(it.name, it.name)
            }
            _controllerItems.value = controllerRepository.list().map {
                SpinnerItem(it.name, it.name)
            }
            reportId?.let {
                val reportEntity = reportsRepository.getReport(reportId)
                if(reportEntity != null) {
                    deliveryNumberValue.value = reportEntity.deliveryNumber
                    selectedDate.value = reportEntity.date
                    supplierValue.value = AutoCompleteValue(reportEntity.supplier)
                    varietyValue.value = AutoCompleteValue(reportEntity.variety)
                    controlledSamplesItem.value = samplesTypes.find { it.code == reportEntity.controlledSamples }
                    sizeMin.value = formatter.format(reportEntity.sizeMin)
                    sizeMax.value = formatter.format(reportEntity.sizeMax)
                    temperatureMin.value = formatter2.format(reportEntity.temperatureMin)
                    temperatureMax.value = formatter2.format(reportEntity.temperatureMax)
                    controllerValue.value = AutoCompleteValue(reportEntity.controller)
                    title.value = stringRepository[R.string.report_form_edit_report]
                }
            }
            hideLoader()
        }
    }

    fun onClickSave() {
        if(validator.validate()) {
            val report = ReportEntity (
                id = reportId ?: UUID.randomUUID().toString(),
                date = selectedDate.value!!,
                creationDate = Date(),
                controlledSamples = controlledSamplesItem.value!!.code,
                deliveryNumber = deliveryNumberValue.value!!,
                supplier = supplierValue.value!!.text,
                variety = varietyValue.value!!.text,
                controller = controllerValue.value!!.text,
                sizeMin = parseSize(sizeMin.value),
                sizeMax = parseSize(sizeMax.value),
                temperatureMin = parseTemp(temperatureMin.value),
                temperatureMax = parseTemp(temperatureMax.value)
            )
            viewModelScope.launch {
                showLoader()
                if(reportId == null) {
                    reportsRepository.saveReport(report)
                } else {
                    reportsRepository.updateReport(report)
                }
                saveSupplierValue()
                saveVarietyValue()
                saveControllerValue()
                hideLoader()
                if(reportId == null) {
                    navigate(DisplayCreateDamagesFormAction(reportId = report.id))
                } else {
                    navigate(FinishActivityAction())
                }
            }
        }
    }

    private fun parseSize(value: String?) : Int {
        if(value == null) return 0
        val decimalFormat = DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale("pl")))
        return decimalFormat.parse(value)?.toInt() ?: 0
    }

    private fun parseTemp(value: String?): Float {
        if(value == null) return 0f
        val decimalFormat = DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale("pl")))
        decimalFormat.maximumFractionDigits = 1
        return decimalFormat.parse(value)?.toFloat() ?: 0f
    }

    private suspend fun saveSupplierValue() {
        val supplierItem = supplierValue.value?.item
        if (supplierItem == null) {
            supplierValue.value?.text?.let { supplierName ->
                suppliersRepository.save(SupplierEntity(supplierName))
            }
        } else {
            suppliersRepository.load(supplierItem.text)?.let { entity ->
                entity.incrementIndex()
                suppliersRepository.save(entity)
            }
        }
    }

    private suspend fun saveVarietyValue() {
        val varietyItem = varietyValue.value?.item
        if (varietyItem == null) {
            varietyValue.value?.text?.let { varietyName ->
                varietiesRepository.save(VarietyEntity(varietyName))
            }
        } else {
            varietiesRepository.load(varietyItem.text)?.let { entity ->
                entity.incrementIndex()
                varietiesRepository.save(entity)
            }
        }
    }

    private suspend fun saveControllerValue() {
        val controllerItem = controllerValue.value?.item
        if (controllerItem == null) {
            controllerValue.value?.text?.let { controllerName ->
                controllerRepository.save(ControllerEntity(controllerName))
            }
        } else {
            controllerRepository.load(controllerItem.text)?.let { entity ->
                entity.incrementIndex()
                controllerRepository.save(entity)
            }
        }
    }

    private fun nowMinusYear(): Date {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.YEAR, -1)
        return calendar.time
    }

    fun onClickBack() {
        navigate(FinishActivityAction())
    }

}