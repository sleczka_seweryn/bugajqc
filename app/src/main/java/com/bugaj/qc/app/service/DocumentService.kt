package com.bugaj.qc.app.service

import java.io.File

interface DocumentService {

    suspend fun generateReport(reportId: String): File

}