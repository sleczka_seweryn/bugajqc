package com.bugaj.qc.app.splash

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import com.bugaj.qc.app.R
import com.bugaj.qc.app.application.navigation.ActivityNavigateManager
import com.bugaj.qc.app.databinding.SplashActivityBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SplashActivity : AppCompatActivity() {

    private lateinit var binding: SplashActivityBinding

    private val viewModel by viewModels<SplashViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(
            this, R.layout.splash_activity)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        viewModel.navigateAction.observe(this) {
            it?.accept(ActivityNavigateManager(this))
        }
    }
}