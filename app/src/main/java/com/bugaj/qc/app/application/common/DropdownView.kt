package com.bugaj.qc.app.application.common

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.AdapterView
import androidx.appcompat.widget.AppCompatSpinner

class DropdownView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : AppCompatSpinner(context, attrs, defStyleAttr), AdapterView.OnItemSelectedListener {

    private val adapter: SpinnerAdapter = SpinnerAdapter(context)

    private var onItemChangeListener: (()->Unit)? = null

    init {
        onItemSelectedListener = this
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        adapter.seSelectedItemPosition(position)
        onItemChangeListener?.invoke()
    }

    override fun onNothingSelected(parent: AdapterView<*>?) { }

    fun setData(data: List<SpinnerItem>) {
        setAdapter(adapter)
        adapter.update(data)
        setSelection(0)
    }

    fun setItem(item: SpinnerItem) {
        val position = adapter.getItemPosition(item)
        setSelection(position)
    }

    override fun getSelectedItem(): SpinnerItem {
        return adapter.getItem(selectedItemPosition)
    }

    fun setOnItemChangeListener(onChange: ()->Unit) {
        onItemChangeListener = onChange
    }

    fun setHasError(hasError: Boolean) {
        adapter.setHasError(hasError)
    }

}