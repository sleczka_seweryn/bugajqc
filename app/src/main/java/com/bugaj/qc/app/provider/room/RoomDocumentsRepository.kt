package com.bugaj.qc.app.provider.room

import androidx.lifecycle.LiveData
import com.bugaj.qc.app.service.DocumentsRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class RoomDocumentsRepository @Inject constructor (
    private val documentsDao: DocumentsDao,
    private val ioDispatcher: CoroutineDispatcher
): DocumentsRepository {

    override fun observeDocument(reportId: String): LiveData<DocumentEntity> {
        return documentsDao.observeDocument(reportId)
    }

    override suspend fun find(id: String) = withContext(ioDispatcher) {
        documentsDao.find(id)
    }

    override suspend fun findByReportId(reportId: String) = withContext(ioDispatcher) {
        documentsDao.findByReportId(reportId)
    }

    override suspend fun insert(entity: DocumentEntity) = withContext(ioDispatcher) {
        documentsDao.insert(entity)
    }

    override suspend fun delete(id: String) = withContext(ioDispatcher) {
        documentsDao.delete(id)
    }

}