package com.bugaj.qc.app.application.validation

import com.bugaj.qc.app.application.common.StringRepository

class OptionalValidator constructor(
    private val validator: (Validator<String>)? = null
) : Validator<String> {

    override fun validate(strings: StringRepository, value: String?): String? {
        if(value?.isNotEmpty() == true) {
            return validator?.validate(strings,value)
        }
        return null
    }
}