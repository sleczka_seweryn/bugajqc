package com.bugaj.qc.app.dashboard.navigation

import android.content.Context
import android.content.Intent
import com.bugaj.qc.app.application.navigation.StartActivityAction
import com.bugaj.qc.app.dashboard.DashboardActivity

class DisplayDashboardAction : StartActivityAction() {

    override fun createIntent(context: Context) = Intent(context, DashboardActivity::class.java)

}