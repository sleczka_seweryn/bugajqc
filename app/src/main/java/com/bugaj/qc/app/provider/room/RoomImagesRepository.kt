package com.bugaj.qc.app.provider.room

import androidx.lifecycle.LiveData
import com.bugaj.qc.app.service.ImagesRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class RoomImagesRepository @Inject constructor(
    private val imagesDao: ImagesDao,
    private val ioDispatcher: CoroutineDispatcher
) : ImagesRepository {

    override suspend fun load(id: String): ImageEntity? = withContext(ioDispatcher) {
        try {
            val data = imagesDao.find(id)
            if (data != null) {
                return@withContext data
            } else {
                return@withContext null
            }
        } catch (e: Exception) {
            return@withContext null
        }
    }

    override suspend fun save(entity: ImageEntity) = withContext(ioDispatcher) {
        imagesDao.insert(entity)
    }

    override fun observeReportImages(reportId: String): LiveData<List<ImageEntity>> {
        return imagesDao.observeReportImages(reportId)
    }

    override suspend fun deleteImage(imageId: String) = withContext(ioDispatcher) {
        imagesDao.delete(imageId)
    }

    override suspend fun findWhereReportId(reportId: String): List<ImageEntity> = withContext(ioDispatcher) {
        imagesDao.findWhereReportId(reportId)
    }

}