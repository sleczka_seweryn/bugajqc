package com.bugaj.qc.app.application.navigation

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import androidx.core.content.FileProvider
import com.bugaj.qc.app.application.files.FileProviderConst
import java.io.File

class ViewPdfFileAction constructor (
    private val file: File
) : StartActivityAction(closeParent = false) {

    override fun createIntent(context: Context): Intent = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        Intent().apply {
            action = Intent.ACTION_VIEW
            data = FileProvider.getUriForFile(context, FileProviderConst.AUTHORITY, file)
            addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        }
    } else {
        val intent =  Intent()
        intent.action = Intent.ACTION_VIEW
        Intent.createChooser(intent, "Open File").apply {
            setDataAndType(Uri.parse(file.absolutePath), "application/pdf")
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        }
    }

}