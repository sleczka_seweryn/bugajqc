package com.bugaj.qc.app.application.navigation

import android.content.Context
import androidx.fragment.app.Fragment

abstract class FragmentTransactionAction constructor() : NavigateAction {

    abstract fun createFragment(context: Context): Fragment

    override fun accept(navigation: NavigateManager) {
        navigation.visit(this)
    }

    open fun getTag(): String = this::class.simpleName ?: this::class.toString()

}