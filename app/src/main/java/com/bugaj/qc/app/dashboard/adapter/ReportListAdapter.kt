package com.bugaj.qc.app.dashboard.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.bugaj.qc.app.dashboard.DashboardViewModel

class ReportListAdapter constructor(
    private val viewModel: DashboardViewModel
) : ListAdapter<ReportItem, ReportViewHolder>(ReportDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReportViewHolder {
        return ReportViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ReportViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(viewModel, item)
    }
}