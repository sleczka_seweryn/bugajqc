package com.bugaj.qc.app.dashboard

import androidx.lifecycle.*
import com.bugaj.qc.app.application.arch.BaseViewModel
import com.bugaj.qc.app.application.common.StringRepository
import com.bugaj.qc.app.application.navigation.ViewPdfFileAction
import com.bugaj.qc.app.dashboard.adapter.ReportFilterType
import com.bugaj.qc.app.dashboard.adapter.ReportItem
import com.bugaj.qc.app.dashboard.adapter.ReportMapper
import com.bugaj.qc.app.provider.room.ReportEntity
import com.bugaj.qc.app.report.create.navigation.DisplayCreateReportAction
import com.bugaj.qc.app.report.details.navigation.DisplayReportDetailsAction
import com.bugaj.qc.app.report.document.navigation.DisplayCreateDocumentDialogAction
import com.bugaj.qc.app.report.send.navigation.DisplayReportSendDialogAction
import com.bugaj.qc.app.service.DocumentsRepository
import com.bugaj.qc.app.service.ReportsRepository
import com.bugaj.qc.app.settings.navigation.DisplaySettingsAction
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.io.File
import javax.inject.Inject

@HiltViewModel
class DashboardViewModel @Inject constructor(
    private val strings: StringRepository,
    private val reportsRepository: ReportsRepository,
    private val documentsRepository: DocumentsRepository
) : BaseViewModel() {

    private val _forceUpdate = MutableLiveData(false)

    private val _items: LiveData<List<ReportItem>> = _forceUpdate.switchMap { forceUpdate ->
        reportsRepository.observeReports(filterType.value ?: ReportFilterType.NEWS)
            .distinctUntilChanged()
            .switchMap {
                mapReportItems(it)
            }
    }

    val items: LiveData<List<ReportItem>> = _items

    private val filterType = MutableLiveData<ReportFilterType>(ReportFilterType.NEWS)

    val isEmpty: LiveData<Boolean> = Transformations.map(_items) { it.isEmpty() }

    init {
        load(true)
    }

    fun load(forceUpdate: Boolean) {
        _forceUpdate.value = forceUpdate
    }

    fun setFilterType(filter: ReportFilterType) {
        filterType.value = filter
        _forceUpdate.value = false
    }

    private fun mapReportItems(reports: List<ReportEntity>) : LiveData<List<ReportItem>> {
        val mapper = ReportMapper(strings)
        val result = MutableLiveData<List<ReportItem>>()
        result.value = reports.map { mapper.map(it) }
        return result
    }

    fun onClickCreateReport() {
        navigate(DisplayCreateReportAction())
    }

    fun onClickReportItem(reportId: String) {
        navigate(DisplayReportDetailsAction(reportId))
    }

    fun onClickSettingsButton() {
        navigate(DisplaySettingsAction())
    }

    fun onClickView(reportId: String) {
        viewModelScope.launch {
            documentsRepository.findByReportId(reportId)?.let {
                navigate(ViewPdfFileAction(File(it.documentUrl)))
            }
        }
    }

    fun onClickCreatePdf(reportId: String) {
        navigate(DisplayCreateDocumentDialogAction(
            reportId = reportId
        ))
    }

    fun onClickSend(reportId: String) {
        navigate(DisplayReportSendDialogAction(
            reportId = reportId
        ))
    }

}