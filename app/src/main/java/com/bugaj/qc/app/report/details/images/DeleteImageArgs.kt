package com.bugaj.qc.app.report.details.images

object DeleteImageArgs {

    const val ARG_IMAGE_ID_TO_DELETE: String = "imageId"

}