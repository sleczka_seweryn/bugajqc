package com.bugaj.qc.app.service

import com.bugaj.qc.app.provider.room.SupplierEntity

interface SuppliersRepository {

    suspend fun load(name: String): SupplierEntity?

    suspend fun list(): List<SupplierEntity>

    suspend fun save(entity: SupplierEntity)

}