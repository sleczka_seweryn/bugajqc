package com.bugaj.qc.app.application.validation

import android.webkit.URLUtil
import com.bugaj.qc.app.R
import com.bugaj.qc.app.application.common.StringRepository

class UrlValidator constructor (
    private val validator: (Validator<String>)? = null
) : Validator<String> {

    override fun validate(strings: StringRepository, value: String?): String? {
        if(!URLUtil.isValidUrl(value)) {
            return strings[R.string.error_incorrect_url]
        }
        return validator?.validate(strings, value)
    }
}