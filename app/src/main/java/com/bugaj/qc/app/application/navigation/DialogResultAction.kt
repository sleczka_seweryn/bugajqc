package com.bugaj.qc.app.application.navigation

import android.content.Intent

class DialogResultAction(
    val requestCode: Int,
    val resultIntent: Intent
): NavigateAction {

    override fun accept(navigation: NavigateManager) {
        navigation.visit(this)
    }
}