package com.bugaj.qc.app.provider.room

import androidx.room.*

@Dao
interface VarietiesDao {

    @Query("SELECT * FROM varieties WHERE name = :name")
    fun load(name: String): VarietyEntity?

    @Query("SELECT * FROM varieties")
    suspend fun list(): List<VarietyEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(setting: VarietyEntity)

}