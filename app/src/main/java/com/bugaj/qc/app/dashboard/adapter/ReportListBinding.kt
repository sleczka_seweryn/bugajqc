package com.bugaj.qc.app.dashboard.adapter

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView

@BindingAdapter("items")
fun setItems(listView: RecyclerView, items: List<ReportItem>?) {
    items?.let {
        (listView.adapter as ReportListAdapter).submitList(items)
    }
}
