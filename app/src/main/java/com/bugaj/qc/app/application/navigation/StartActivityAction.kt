package com.bugaj.qc.app.application.navigation

import android.content.Context
import android.content.Intent

abstract class StartActivityAction constructor(
    val closeParent: Boolean = true
) : NavigateAction {

    abstract fun createIntent(context: Context): Intent

    override fun accept(navigation: NavigateManager) {
        navigation.visit(this)
    }

}