package com.bugaj.qc.app.provider.room

import androidx.room.*

@Dao
interface ControllerDao {

    @Query("SELECT * FROM controllers WHERE name = :name")
    fun load(name: String): ControllerEntity?

    @Query("SELECT * FROM controllers")
    suspend fun list(): List<ControllerEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entity: ControllerEntity)

}