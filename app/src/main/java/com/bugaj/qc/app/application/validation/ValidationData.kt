package com.bugaj.qc.app.application.validation

data class ValidationData constructor (
    val message: String?,
    val useFocus: Boolean = false
) {

    fun hasError(): Boolean = message?.isNotEmpty() ?: false

    fun focus(): ValidationData {
        return ValidationData(
            message = message,
            useFocus = true
        )
    }

}