package com.bugaj.qc.app.settings.navigation

import android.content.Context
import android.content.Intent
import com.bugaj.qc.app.application.navigation.StartActivityAction
import com.bugaj.qc.app.settings.SettingsActivity

class DisplaySettingsAction : StartActivityAction(
    closeParent = false
) {

    override fun createIntent(context: Context) = Intent(context, SettingsActivity::class.java)
}