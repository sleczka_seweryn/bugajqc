package com.bugaj.qc.app.application.injection

import com.bugaj.qc.app.provider.app.AuthServiceImpl
import com.bugaj.qc.app.provider.app.DocumentServiceImpl
import com.bugaj.qc.app.provider.app.MaintenanceServiceImpl
import com.bugaj.qc.app.provider.app.SplashServiceImpl
import com.bugaj.qc.app.provider.ftp.FTPUploadService
import com.bugaj.qc.app.provider.room.*
import com.bugaj.qc.app.service.*
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class ServiceModule {

    @Binds
    abstract fun bindSplashService(service: SplashServiceImpl): SplashService

    @Binds
    abstract fun bindAuthService(service: AuthServiceImpl): AuthService

    @Binds
    abstract fun bindSettingsRepository(repository: RoomSettingsRepository): SettingsRepository

    @Binds
    abstract fun bindReportRepository(repository: RoomReportsRepository): ReportsRepository

    @Binds
    abstract fun bindImagesRepository(repository: RoomImagesRepository): ImagesRepository

    @Binds
    abstract fun bindSuppliersRepository(repository: RoomSuppliersRepository): SuppliersRepository

    @Binds
    abstract fun bindVarietiesRepository(repository: RoomVarietiesRepository): VarietiesRepository

    @Binds
    abstract fun bindTemplateRepository(repository: RoomTemplateRepository): TemplateRepository

    @Binds
    abstract fun bindRoomDamagesRepository(repository: RoomDamagesRepository): DamagesRepository

    @Binds
    abstract fun bindRoomControllerRepository(repository: RoomControllerRepository): ControllerRepository

    @Binds
    abstract fun bindDocumentsRepository(repository: RoomDocumentsRepository): DocumentsRepository

    @Binds
    abstract fun bindDocumentService(service: DocumentServiceImpl): DocumentService

    @Binds
    abstract fun bindMaintenanceService(service: MaintenanceServiceImpl): MaintenanceService

    @Binds
    abstract fun bindUploadService(service: FTPUploadService): UploadService

}