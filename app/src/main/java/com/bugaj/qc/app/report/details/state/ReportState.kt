package com.bugaj.qc.app.report.details.state

enum class ReportState {
    DAMAGE_REPORT_NOT_FILLED,
    PHOTOS_NOT_ATTACHED,
    DOCUMENT_NOT_CREATED,
    DOCUMENT_NOT_SEND,
    DOCUMENT_SEND
}