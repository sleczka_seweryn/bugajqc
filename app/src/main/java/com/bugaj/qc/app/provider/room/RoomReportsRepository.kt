package com.bugaj.qc.app.provider.room

import androidx.lifecycle.LiveData
import com.bugaj.qc.app.dashboard.adapter.ReportFilterType
import com.bugaj.qc.app.service.ReportsRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import java.util.*
import javax.inject.Inject

class RoomReportsRepository @Inject constructor (
    private val reportsDao: ReportsDao,
    private val ioDispatcher: CoroutineDispatcher
) : ReportsRepository {

    override fun observeReports(): LiveData<List<ReportEntity>> {
        return reportsDao.observeReports()
    }

    override fun observeReports(filterType: ReportFilterType): LiveData<List<ReportEntity>> {
        if(filterType == ReportFilterType.NEWS) {
            return reportsDao.observeReportsWhereStateNew()
        } else {
            return reportsDao.observeReportsWhereStateSend()
        }
    }

    override suspend fun getReports(): List<ReportEntity> = withContext(ioDispatcher) {
        reportsDao.getReports()
    }

    override fun observeReport(id: String): LiveData<ReportEntity?> {
        return reportsDao.observeReportById(id)
    }

    override suspend fun getReport(id: String): ReportEntity? = withContext(ioDispatcher) {
        try {
            val setting = reportsDao.getReportById(id)
            if (setting != null) {
                return@withContext setting
            } else {
                return@withContext null
            }
        } catch (e: Exception) {
            return@withContext null
        }
    }

    override suspend fun saveReport(reportEntity: ReportEntity) = withContext(ioDispatcher) {
        reportsDao.insertReport(reportEntity)
    }

    override suspend fun updateReport(reportEntity: ReportEntity) = withContext(ioDispatcher) {
        reportsDao.updateReport(reportEntity)
    }

    override suspend fun deleteReport(reportEntity: ReportEntity) = withContext(ioDispatcher) {
        reportsDao.deleteReport(reportEntity)
    }

    override suspend fun deleteReportWhereId(id: String) = withContext(ioDispatcher) {
        reportsDao.deleteReportWhereId(id)
    }

    override suspend fun findWhereCreationDateBetween(from: Date, to: Date) = withContext(ioDispatcher) {
        reportsDao.getReportsWhereCreationDateBetween(from, to)
    }
}