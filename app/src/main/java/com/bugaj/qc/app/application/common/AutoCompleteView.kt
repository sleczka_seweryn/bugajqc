package com.bugaj.qc.app.application.common

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.View
import android.widget.AdapterView
import androidx.appcompat.widget.AppCompatAutoCompleteTextView

class AutoCompleteView @JvmOverloads constructor (
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : AppCompatAutoCompleteTextView(context, attrs, defStyleAttr), AdapterView.OnItemClickListener,
    TextWatcher {

    private val adapter: AutoCompleteAdapter = AutoCompleteAdapter(context)

    private var onItemChangeListener: (()->Unit)? = null

    init {
        onItemClickListener = this
        addTextChangedListener(this)
        threshold = 0
    }

    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        adapter.seSelectedItemPosition(position)
        onItemChangeListener?.invoke()
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {  }

    override fun afterTextChanged(s: Editable?) {
        onItemChangeListener?.invoke()
    }

    override fun onTextChanged(
        text: CharSequence?,
        start: Int,
        lengthBefore: Int,
        lengthAfter: Int
    ) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter)
    }

    fun setData(data: List<SpinnerItem>) {
        setAdapter(adapter)
        adapter.update(data)
    }

    fun setItem(item: AutoCompleteValue) {
        setText(item.text)
    }

    fun getSelectedItem(): AutoCompleteValue {
        return AutoCompleteValue(
            text = text.toString(),
            item = adapter.getItemWhereText(text.toString())
        )
    }

    fun setOnItemChangeListener(onChange: ()->Unit) {
        onItemChangeListener = onChange
    }

}