package com.bugaj.qc.app.provider.app

import android.content.Context
import android.content.res.Configuration
import android.content.res.Resources
import android.graphics.*
import android.graphics.pdf.PdfDocument
import android.os.Environment
import android.util.DisplayMetrics
import android.util.Log
import com.bugaj.qc.app.R
import com.bugaj.qc.app.provider.room.DamageEntity
import com.bugaj.qc.app.provider.room.DocumentEntity
import com.bugaj.qc.app.provider.room.ImageEntity
import com.bugaj.qc.app.provider.room.ReportEntity
import com.bugaj.qc.app.report.damages.model.formatDamage
import com.bugaj.qc.app.service.*
import com.bugaj.qc.app.settings.model.ImageResolutionTypes
import com.bugaj.qc.app.settings.model.Settings
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*
import javax.inject.Inject


class DocumentServiceImpl @Inject constructor(
    @ApplicationContext private val context: Context,
    private val settingsRepository: SettingsRepository,
    private val damagesRepository: DamagesRepository,
    private val documentsRepository: DocumentsRepository,
    private val imagesRepository: ImagesRepository,
    private val reportsRepository: ReportsRepository,
    private val ioDispatcher: CoroutineDispatcher
): DocumentService {

    private val width = 595
    private val height = 842
    private val margin = 56
    private val cellWidth = width / 18
    private val cellHeight = height / 24

    private val paintTableBorder = Paint().apply {
        style = Paint.Style.STROKE
        strokeJoin = Paint.Join.ROUND
        color = Color.BLACK
        strokeWidth = 0.5f
    }

    private val paintGreyBackground = Paint().apply {
        color = Color.BLACK
        alpha = (0.1f * 255f).toInt()
        style = Paint.Style.FILL
    }

    private val paintText16Bold = Paint().apply {
        style = Paint.Style.FILL
        color = Color.BLACK
        textSize = 16f
        typeface = Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD)
    }

    private val paintText16Normal = Paint().apply {
        style = Paint.Style.FILL
        color = Color.BLACK
        textSize = 14f
        typeface = Typeface.create(Typeface.SANS_SERIF, Typeface.NORMAL)
    }

    private val paintText14Bold = Paint().apply {
        style = Paint.Style.FILL
        color = Color.BLACK
        textSize = 14f
        typeface = Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD)
    }

    private val paintText14Normal = Paint().apply {
        style = Paint.Style.FILL
        color = Color.BLACK
        textSize = 14f
        typeface = Typeface.create(Typeface.SANS_SERIF, Typeface.NORMAL)
    }

    private val paintText12Bold = Paint().apply {
        style = Paint.Style.FILL
        color = Color.BLACK
        textSize = 12f
        typeface = Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD)
    }

    private val paintText12Italic = Paint().apply {
        style = Paint.Style.FILL
        color = Color.BLACK
        textSize = 12f
        typeface = Typeface.create(Typeface.SANS_SERIF, Typeface.ITALIC)
    }

    private val paintText10Normal = Paint().apply {
        style = Paint.Style.FILL
        color = Color.BLACK
        textSize = 10f
        typeface = Typeface.create(Typeface.SANS_SERIF, Typeface.NORMAL)
    }

    private val paintText10Bold = Paint().apply {
        style = Paint.Style.FILL
        color = Color.BLACK
        textSize = 10f
        typeface = Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD)
    }

    private val paintText10Italic = Paint().apply {
        style = Paint.Style.FILL
        color = Color.BLACK
        textSize = 10f
        typeface = Typeface.create(Typeface.SANS_SERIF, Typeface.ITALIC)
    }

    private val resPL = getResourceWithLocale(Locale("pl"))

    private val resEN = getResourceWithLocale(Locale.UK)

    override suspend fun generateReport(reportId: String) = withContext(ioDispatcher) {
        val report = reportsRepository.getReport(reportId) ?: throw IllegalStateException("reportId not found")
        val damages = damagesRepository.listByReportId(reportId)
        val images = imagesRepository.findWhereReportId(report.id)
        val resolutionType = settingsRepository.getSetting(Settings.IMAGE_RESOLUTION.name)?.value?: ImageResolutionTypes.X1024.name

        val file = createDocumentFile(report)
        val stream = FileOutputStream(file);

        try {

            val document = PdfDocument()

            val page = document.startPage(
                PdfDocument.PageInfo
                    .Builder(width, height, 1)
                    .create()
            )

            page.canvas.apply {
                drawLogo(1.4f)
                drawCompanyDetails(paintText10Bold)
                drawCenteredTitle(paintText14Bold, paintText14Normal)
                drawLocationAndDate(report)
                drawBasicDataTable(report)
                val offset = drawDamagesTable(damages, report.getSamplesCategory().weight)
                drawPicturesTable(offset, images, resolutionType)
            }

            document.finishPage(page)
            document.writeTo(stream)
            document.close()

        } finally {
            stream.flush();
            stream.close();
        }

        documentsRepository.insert(
            DocumentEntity(
                reportId = reportId,
                documentUrl = file.absolutePath
            )
        )

        report.isDocumentCreated = true
        report.isDocumentSend = false
        reportsRepository.updateReport(report)

        return@withContext file
    }

    @Throws(IOException::class)
    private fun createDocumentFile(report: ReportEntity): File {
        val storageDir: File? = context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)
        val fileName = "LP" + report.deliveryNumber + "-" + report.formatDate()
        val file = File(storageDir, "$fileName.pdf")
        file.createNewFile()
        return file
    }

    private fun dpToPx(dp: Int): Int {
        return (dp * (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)).toInt()
    }

    private fun Canvas.drawLogo(scale: Float) {
        drawBitmap(
            BitmapFactory.decodeResource(
                context.resources,
                R.drawable.report_icon
            ),
            Rect(0, 0, dpToPx(123), dpToPx(36)),
            Rect(
                margin, margin,
                (scale * 1f * margin.toFloat()).toInt() + margin,
                (scale * 0.29f * margin.toFloat()).toInt() + margin
            ),
            Paint()
        )
    }

    private fun Canvas.drawCompanyDetails(textPaint: Paint) {
        val topOffset = margin.toFloat() + 0.7f * margin
        val lineHeight = textPaint.textSize * 1.3f
        drawText("Bugaj sp. z o.o.", margin.toFloat(), topOffset, textPaint)
        drawText("Bugaj Zakrzewski 5", margin.toFloat(), topOffset + lineHeight, textPaint)
        drawText("97-512 Kodrąb", margin.toFloat(), topOffset + (lineHeight * 2), textPaint)
        drawText("NIP: PL7722405253", margin.toFloat(), topOffset + (lineHeight * 3), textPaint)
    }

    private fun Canvas.drawCenteredTitle(titlePaint: Paint, subTitlePaint: Paint) {
        val topOffset = margin.toFloat() + 1.65f * margin
        drawTextCenter("Raport z kontroli jakości", width / 2f, topOffset, titlePaint)
        drawTextCenter(
            "QC report",
            width / 2f,
            topOffset + titlePaint.textSize * 1.5f,
            subTitlePaint
        )
    }

    private fun Canvas.drawLocationAndDate(reportEntity: ReportEntity) {
        val text = "Bugaj Zakrzewski, " + reportEntity.displayDate()
        val bounds = Rect()
        paintText10Normal.getTextBounds(text, 0, text.length, bounds)
        val x: Float = (width-margin).toFloat() - bounds.width() - bounds.left
        val y: Float = margin.toFloat() + bounds.height() - bounds.bottom
        drawText(text, x, y, paintText10Normal)
    }

    private fun Canvas.drawBasicDataTable(reportEntity: ReportEntity) {
        val topOffset = 5.5f * cellHeight
        val cellH = cellHeight * 0.8f

        val tStart = margin.toFloat()
        val tSEnd = (width-margin).toFloat()

        val C1W = 2.2f
        val C2W = 2.2f
        val C3W = 2.85f
        val C4W = 1.9f
        val C5W = 2.5f

        fun drawWLine(top: Float) {
            drawPath(Path().apply {
                moveTo(tStart, top)
                lineTo(tSEnd, top)
            }, paintTableBorder)
        }

        fun drawHLine(top: Float, left: Float, cellHScale: Float) {
            drawPath(Path().apply {
                moveTo(left, top)
                lineTo(left, top + cellHScale * cellH)
            }, paintTableBorder)
        }

        drawWLine(topOffset)
        drawWLine(topOffset + cellH)
        drawWLine(topOffset + 2f * cellH)
        drawWLine(topOffset + 3f * cellH)

        drawHLine(topOffset, margin.toFloat(), 3f)
        drawHLine(topOffset, margin.toFloat() + cellWidth * C1W, 3f)
        drawHLine(topOffset, margin.toFloat() + cellWidth * (C1W + C2W), 1f)
        drawHLine(topOffset, margin.toFloat() + cellWidth * (C1W + C2W + C3W), 1f)
        drawHLine(topOffset, margin.toFloat() + cellWidth * (C1W + C2W + C3W + C4W), 3f)
        drawHLine(topOffset, margin.toFloat() + cellWidth * (C1W + C2W + C3W + C4W + C5W), 3f)
        drawHLine(topOffset, ((width - margin).toFloat()), 3f)

        drawHLine(topOffset + 2f * cellH, margin.toFloat() + cellWidth * (C1W + C2W), 1f)
        drawHLine(topOffset + 2f * cellH, margin.toFloat() + cellWidth * (C1W + C2W + C3W), 1f)

        drawTwoLineTextBox(
            "Data", "Date", RectF(
                tStart, topOffset, tStart + (C1W * cellWidth), topOffset + cellH
            ), paintText10Bold, paintText10Italic
        )

        drawSingleLineTextBox(
            reportEntity.displayDate(), RectF(
                tStart + C1W * cellWidth,
                topOffset,
                tStart + ((C1W + C2W) * cellWidth),
                topOffset + cellH
            ), paintText10Normal
        )

        drawTwoLineTextBox(
            "Dostawca", "Supplier", RectF(
                tStart, topOffset + cellH, tStart + (C1W * cellWidth), topOffset + (2 * cellH)
            ), paintText10Bold, paintText10Italic
        )

        drawSingleLineTextBox(
            reportEntity.supplier, RectF(
                tStart + C1W * cellWidth,
                topOffset + cellH,
                tStart + ((C1W + C2W) * cellWidth),
                topOffset + (2 * cellH)
            ), paintText10Normal
        )

        drawTwoLineTextBox(
            "Kontroler", "Controller", RectF(
                tStart, topOffset + (2 * cellH), tStart + (C1W * cellWidth), topOffset + (3 * cellH)
            ), paintText10Bold, paintText10Italic
        )

        drawTwoLineTextBox(
            reportEntity.getControllerName(), reportEntity.getControllerSurname(), RectF(
                tStart + C1W * cellWidth,
                topOffset + (2 * cellH),
                tStart + ((C1W + C2W) * cellWidth),
                topOffset + (3 * cellH)
            ), paintText10Normal, paintText10Normal
        )

        drawTwoLineTextBox(
            "Ilość sprawdzona", "Controlled samples", RectF(
                tStart + (C1W + C2W) * cellWidth,
                topOffset,
                tStart + (C1W + C2W + C3W) * cellWidth,
                topOffset + cellH
            ), paintText10Bold, paintText10Italic
        )

        drawSingleLineTextBox(
            reportEntity.displayControlledSamplesNum(), RectF(
                tStart + (C1W + C2W + C3W) * cellWidth,
                topOffset,
                tStart + (C1W + C2W + C3W + C4W) * cellWidth,
                topOffset + cellH
            ), paintText10Normal
        )

        drawTwoLineTextBox(
            "Kalibracja", "Size", RectF(
                tStart + (C1W + C2W) * cellWidth,
                topOffset + (2f * cellH),
                tStart + (C1W + C2W + C3W) * cellWidth,
                topOffset + (3f * cellH)
            ), paintText10Bold, paintText10Italic
        )

        drawSingleLineTextBox(
            reportEntity.displaySize(), RectF(
                tStart + (C1W + C2W + C3W) * cellWidth,
                topOffset + (2f * cellH),
                tStart + (C1W + C2W + C3W + C4W) * cellWidth,
                topOffset + (3f * cellH)
            ), paintText10Normal
        )

        drawTwoLineTextBox(
            "LP", "Delivery number", RectF(
                tStart + (C1W + C2W + C3W + C4W) * cellWidth,
                topOffset,
                tStart + (C1W + C2W + C3W + C4W + C5W) * cellWidth,
                topOffset + cellH
            ), paintText10Bold, paintText10Italic
        )

        drawSingleLineTextBox(
            reportEntity.deliveryNumber, RectF(
                tStart + (C1W + C2W + C3W + C4W + C5W) * cellWidth,
                topOffset,
                tSEnd,
                topOffset + cellH
            ), paintText10Normal
        )

        drawTwoLineTextBox(
            "Odmiana", "Variety", RectF(
                tStart + (C1W + C2W + C3W + C4W) * cellWidth,
                topOffset + (1f * cellH),
                tStart + (C1W + C2W + C3W + C4W + C5W) * cellWidth,
                topOffset + (2f * cellH)
            ), paintText10Bold, paintText10Italic
        )

        drawSingleLineTextBox(
            reportEntity.variety, RectF(
                tStart + (C1W + C2W + C3W + C4W + C5W) * cellWidth,
                topOffset + (1f * cellH),
                tSEnd,
                topOffset + (2f * cellH)
            ), paintText10Normal
        )

        drawTwoLineTextBox(
            "Temperatura", "Temperature", RectF(
                tStart + (C1W + C2W + C3W + C4W) * cellWidth,
                topOffset + (2f * cellH),
                tStart + (C1W + C2W + C3W + C4W + C5W) * cellWidth,
                topOffset + (3f * cellH)
            ), paintText10Bold, paintText10Italic
        )

        drawSingleLineTextBox(
            reportEntity.displayTemp(), RectF(
                tStart + (C1W + C2W + C3W + C4W + C5W) * cellWidth,
                topOffset + (2f * cellH),
                tSEnd,
                topOffset + (3f * cellH)
            ), paintText10Normal
        )
    }

    private fun Canvas.drawPicturesTable(
        topOffset: Float,
        images: List<ImageEntity>,
        resolutionType: String
    ) {
        val cellH = cellHeight * 0.4f
        val tStart = margin.toFloat()
        val tSEnd = (width-margin).toFloat()
        val vEnd = (height-margin).toFloat()

        val rect1 = RectF(
            tStart, topOffset + cellHeight * 0.1f, tSEnd, topOffset + (cellHeight * 0.1f) + cellH
        )

        val imageFieldHeight = ((vEnd - rect1.bottom) / 2)
        val imageFieldWidth = ((tSEnd - tStart) / 2)

        val img1Rect = RectF(
            tStart, rect1.bottom, tStart + imageFieldWidth, rect1.bottom + imageFieldHeight
        )

        val img2Rect = RectF(
            tStart + imageFieldWidth, rect1.bottom, tSEnd, rect1.bottom + imageFieldHeight
        )

        val img3Rect = RectF(
            tStart, img1Rect.bottom, tStart + imageFieldWidth, img1Rect.bottom + imageFieldHeight
        )

        val img4Rect = RectF(
            tStart + imageFieldWidth, img2Rect.bottom, tSEnd, img2Rect.bottom + imageFieldHeight
        )

        val imagesRect = arrayListOf<RectF>().apply {
            add(img1Rect)
            add(img2Rect)
            add(img3Rect)
            add(img4Rect)
        }

        drawPath(Path().apply {
            moveTo(rect1.left, rect1.bottom)
            lineTo(rect1.left, rect1.top)
            lineTo(rect1.right, rect1.top)
            lineTo(rect1.right, rect1.bottom)
            lineTo(rect1.left, rect1.bottom)
            lineTo(img3Rect.left, img3Rect.bottom)
            lineTo(img4Rect.right, img3Rect.bottom)
            lineTo(img2Rect.right, img2Rect.top)
            moveTo(img1Rect.left, img1Rect.bottom)
            lineTo(img2Rect.right, img2Rect.bottom)
            moveTo(img1Rect.right, img1Rect.top)
            lineTo(img3Rect.right, img3Rect.bottom)
        }, paintTableBorder)

        val text1 = "ZDJĘCIA /"
        val text2 = "PICTURES"
        val gap = 2.5f

        val text1Bounds = Rect()
        paintText10Bold.getTextBounds(text1, 0, text1.length, text1Bounds)

        val text2Bounds = Rect()
        paintText10Normal.getTextBounds(text2, 0, text2.length, text2Bounds)

        val t1X = rect1.centerX() - 24f - text1Bounds.width() / 2f - text1Bounds.left
        val t1Y = rect1.centerY() + text2Bounds.height() / 2f - text2Bounds.bottom
        drawText(text1, t1X, t1Y, paintText10Bold)

        val t2X = rect1.centerX() + 26f - text1Bounds.width() / 2f - text2Bounds.left
        val t2Y = rect1.centerY() + text2Bounds.height() / 2f - text2Bounds.bottom
        drawText(text2, t2X, t2Y, paintText10Normal)

        for (i in images.indices) {
            if(i < 4) {
                drawImage(images[i].imageUrl, imagesRect[i], resolutionType)
            }
        }
    }

    private fun Canvas.drawImage(imageUrl: String, rectF: RectF, resolutionType: String) {
        val paddingY = 2f
        val paddingX = 2f
        val imageDestination = Rect()
        imageDestination.top = (rectF.top + paddingY).toInt()
        imageDestination.left = (rectF.left + paddingX).toInt()
        imageDestination.bottom = (rectF.bottom - paddingY).toInt()
        imageDestination.right = (rectF.right - paddingX).toInt()
        val imageBounds = Rect()

        val imageType = ImageResolutionTypes.valueOf(resolutionType)

        val bmOptions = BitmapFactory.Options().apply {
            inJustDecodeBounds = true

            BitmapFactory.decodeFile(imageUrl, this)

            val targetW = imageType.width
            val targetH = imageType.height

            val scaleFactor: Int = Math.max(1, Math.min(outWidth / targetW, outHeight / targetH))

            inPurgeable = true
            inSampleSize = Math.round(1.0 / scaleFactor).toInt()

            BitmapFactory.decodeFile(imageUrl, this)

            imageBounds.left = 0
            imageBounds.top = 0
            imageBounds.right = outWidth
            imageBounds.bottom = outHeight

            inJustDecodeBounds = false
        }

        val scale: Float = if(imageBounds.right > imageBounds.bottom)
            imageType.width.toFloat() / imageBounds.width().toFloat()
        else
            imageType.height.toFloat() / imageBounds.height().toFloat()

        imageBounds.right = Math.round(imageBounds.right * scale)
        imageBounds.bottom = Math.round(imageBounds.bottom * scale)

        val resizedBitmap = Bitmap.createScaledBitmap(
            BitmapFactory.decodeFile(imageUrl, bmOptions),
            imageBounds.right,
            imageBounds.bottom,
            false
        )

        imageBounds.right = resizedBitmap.width
        imageBounds.bottom = resizedBitmap.height

        val sf: Float = imageBounds.height().toFloat() / imageBounds.width().toFloat()
        if(sf >= 1) {
            val x: Float = imageDestination.height().toFloat()
            val y: Float = x / sf
            val yPadding = (imageDestination.width() - y) / 2
            imageDestination.left += yPadding.toInt()
            imageDestination.right -= yPadding.toInt()

        } else {
            val y: Float = imageDestination.width().toFloat()
            val x: Float = y * sf
            val xPadding = (imageDestination.height() - x) / 2
            val destinationHeight = imageDestination.height()
            imageDestination.top += xPadding.toInt()
            imageDestination.bottom -= xPadding.toInt()

            if(imageDestination.height() > destinationHeight) {
                val cropH = (imageDestination.height() - destinationHeight) / 2.0f
                val cropW = (cropH * sf) / 2.0f
                imageDestination.left += cropW.toInt()
                imageDestination.right -= cropW.toInt()
                imageDestination.top += cropH.toInt()
                imageDestination.bottom -= cropH.toInt()
            }
        }

        drawBitmap(
            resizedBitmap,
            imageBounds,
            imageDestination,
            Paint()
        )
    }

    private fun Canvas.drawDamagesTable(damages: List<DamageEntity>, total: Float): Float {
        val topOffset = 8f * cellHeight
        val cellH = cellHeight * 0.4f

        val valueCellWidth = cellWidth * 3.2f

        val tStart = margin.toFloat()
        val tSEnd = (width-margin).toFloat()
        val paddingLeft = 6f

        val tableSize = damages.size + 1
        val table: List<List<RectF>> = arrayListOf<List<RectF>>().apply{
            for (i in 0 until tableSize) {
                add(arrayListOf<RectF>().apply {
                    add(
                        RectF(
                            tStart,
                            topOffset + i * cellH,
                            tSEnd - valueCellWidth,
                            topOffset + (i + 1) * cellH
                        )
                    )
                    add(
                        RectF(
                            tSEnd - valueCellWidth,
                            topOffset + i * cellH,
                            tSEnd,
                            topOffset + (i + 1) * cellH
                        )
                    )
                })
            }
        }

        var damagesSum: Float = 0f
        for(i in table.indices) {
            val rect1 = table[i][0]
            val rect2 = table[i][1]

            val text1 = if(i == damages.size) "Suma wszystkich uszkodzeń /" else damages[i].labelPl + " /"
            val text2 = if(i == damages.size) "Total damages" else damages[i].labelEn
            val value = if(i == damages.size) {
                formatDamage(damagesSum, total)
            } else {
                damagesSum += damages[i].value ?: 0f
                damages[i].formatDamage(total)
            }
            if(i == table.size - 1) {
                //draw grey
                drawPath(Path().apply {
                    moveTo(rect1.left, rect1.top)
                    lineTo(rect2.right, rect2.top)
                    lineTo(rect2.right, rect2.bottom)
                    lineTo(rect1.left, rect2.bottom)
                    close()
                }, paintGreyBackground)
                //draw bottom line
                drawPath(Path().apply {
                    moveTo(rect1.left, rect1.bottom)
                    lineTo(rect2.right, rect2.bottom)
                }, paintTableBorder)
            }
            //draw top line
            drawPath(Path().apply {
                moveTo(rect1.left, rect1.top)
                lineTo(rect2.right, rect2.top)
            }, paintTableBorder)
            //left line
            drawPath(Path().apply {
                moveTo(rect1.left, rect1.top)
                lineTo(rect1.left, rect1.bottom)
            }, paintTableBorder)
            //draw middle line
            drawPath(Path().apply {
                moveTo(rect2.left, rect2.top)
                lineTo(rect2.left, rect2.bottom)
            }, paintTableBorder)
            //draw right line
            drawPath(Path().apply {
                moveTo(rect2.right, rect2.top)
                lineTo(rect2.right, rect2.bottom)
            }, paintTableBorder)
            //draw label text
            val text1Bounds = Rect()
            paintText10Bold.getTextBounds(text1, 0, text1.length, text1Bounds)
            val t1X = rect1.left + paddingLeft - text1Bounds.left
            val t1Y = rect1.centerY() + text1Bounds.height() / 2f - text1Bounds.bottom
            drawText(text1, t1X, t1Y, paintText10Bold)

            val text2Bounds = Rect()
            paintText10Normal.getTextBounds(text2, 0, text2.length, text2Bounds)
            val t2X = t1X + text1Bounds.width() + 2.5f
            val t2Y = rect1.centerY() + text2Bounds.height() / 2f - text2Bounds.bottom
            drawText(text2, t2X, t2Y, paintText10Normal)

            //draw value text
            if(value != null && value != "0%") {
                val textValueBounds = Rect()
                paintText10Bold.getTextBounds(value, 0, value.length, textValueBounds)
                val valX = rect2.centerX() - textValueBounds.width() / 2f - textValueBounds.left
                val valY = rect2.centerY() + textValueBounds.height() / 2f - textValueBounds.bottom
                drawText(value, valX, valY, paintText10Bold)
            }
        }

        return table.last()[0].bottom
    }

    private fun Canvas.drawSingleLineTextBox(text1: String, rect: RectF, textPaint: Paint) {
        val paddingLeft = 6f
        val bounds = Rect()
        textPaint.textAlign = Paint.Align.LEFT
        textPaint.getTextBounds(text1, 0, text1.length, bounds)
        val x: Float = rect.left- bounds.left + paddingLeft
        val y: Float = rect.centerY() + bounds.height() / 2f - bounds.bottom
        drawText(text1, x, y, textPaint)
    }

    private fun Canvas.drawTwoLineTextBox(
        text1: String,
        text2: String?,
        rect: RectF,
        textPaint1: Paint,
        textPaint2: Paint
    ) {

        val paddingLeft = 6f
        val gap = 0.8f

        fun drawTextAlignLeft(text: String, position: PointF, paint: Paint) {
            val bounds = Rect()
            paint.textAlign = Paint.Align.LEFT
            paint.getTextBounds(text, 0, text.length, bounds)
            val x: Float = position.x - bounds.left
            val y: Float = position.y + bounds.height() / 2f - bounds.bottom
            drawText(text, x, y, paint)
        }

        drawTextAlignLeft(
            text1, PointF(
                rect.left.toFloat() + paddingLeft,
                (rect.top.toFloat() + rect.height() / 4f) + gap
            ), textPaint1
        )

        text2?.let {
            drawTextAlignLeft(
                text2, PointF(
                    rect.left.toFloat() + paddingLeft,
                    (rect.top.toFloat() + rect.height() * 3f / 4f) - gap
                ), textPaint2
            )
        }
    }

    private fun Canvas.drawTextCenter(text: String, centerX: Float, centerY: Float, paint: Paint) {
        val rect = Rect()
        paint.textAlign = Paint.Align.LEFT
        paint.getTextBounds(text, 0, text.length, rect)
        val x: Float = centerX - rect.width() / 2f - rect.left
        val y: Float = centerY + rect.height() / 2f - rect.bottom
        drawText(text, x, y, paint)
    }

    private fun getResourceWithLocale(locale: Locale): Resources {
        var conf = context.resources.configuration
        conf = Configuration(conf)
        conf.setLocale(locale)
        val localizedContext = context.createConfigurationContext(conf)
        return localizedContext.resources
    }

}

fun Canvas.drawMargin(width: Int, height: Int, margin: Int) {

    val paint = Paint()
    paint.strokeWidth = 4f
    paint.color = Color.RED
    paint.style = Paint.Style.STROKE

    drawPath(Path().apply {
        moveTo(margin.toFloat(), margin.toFloat())
        lineTo(margin.toFloat(), (height - margin).toFloat())
        lineTo((width - margin).toFloat(), (height - margin).toFloat())
        lineTo((width - margin).toFloat(), margin.toFloat())
        close()
    }, paint)

}

fun Canvas.drawGuideLines(width: Int, height: Int, stepX: Int, stepY: Int) {

    val paint = Paint()
    paint.strokeWidth = 2f
    paint.strokeMiter = 1f
    paint.color = Color.BLUE

    for(positionX in 0 until width step stepX) {
        drawPath(Path().apply {
            moveTo(positionX.toFloat(), 0f)
            lineTo(positionX.toFloat(), height.toFloat())
        }, paint)
    }

    for(positionY in 0 until height step stepY) {
        drawPath(Path().apply {
            moveTo(0f, positionY.toFloat())
            lineTo(width.toFloat(), positionY.toFloat())
        }, paint)
    }

}




