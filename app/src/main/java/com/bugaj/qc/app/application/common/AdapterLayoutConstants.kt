package com.bugaj.qc.app.application.common

import com.bugaj.qc.app.R

object AdapterLayoutConstants {

    const val adapter_layout_list_position: Int = R.id.adapter_layout_list_position

    const val adapter_layout_list_view_type: Int = R.id.adapter_layout_list_view_type

    const val adapter_layout_list_holder: Int = R.id.adapter_layout_list_holder

}