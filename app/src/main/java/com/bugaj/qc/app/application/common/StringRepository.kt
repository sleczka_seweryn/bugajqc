package com.bugaj.qc.app.application.common

interface StringRepository {

    operator fun get(stringId: Int): String

}