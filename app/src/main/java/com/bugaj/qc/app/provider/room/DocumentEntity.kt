package com.bugaj.qc.app.provider.room

import androidx.room.*
import androidx.room.ForeignKey.CASCADE
import java.util.*

@Entity(tableName = "documents", foreignKeys = [ForeignKey(
    entity = ReportEntity::class,
    parentColumns = ["id"],
    childColumns = ["reportId"],
    onDelete = CASCADE
)], indices = [Index(value = ["reportId"], unique = false)])
class DocumentEntity constructor (
    @PrimaryKey @ColumnInfo(name = "id") var id: String = UUID.randomUUID().toString(),
    @ColumnInfo(name = "reportId")  var reportId: String,
    @ColumnInfo(name = "documentUrl") var documentUrl: String
) {

    fun getFileName(): String {
        return "${reportId}.pdf"
    }

}