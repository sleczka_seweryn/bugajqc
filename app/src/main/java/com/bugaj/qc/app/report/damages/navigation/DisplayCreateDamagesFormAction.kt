package com.bugaj.qc.app.report.damages.navigation

import android.content.Context
import android.content.Intent
import com.bugaj.qc.app.application.navigation.StartActivityAction
import com.bugaj.qc.app.report.damages.DamagesFormActivity

class DisplayCreateDamagesFormAction constructor (
    private val reportId: String,
    private val shouldCloseParent: Boolean = true
) : StartActivityAction(closeParent = shouldCloseParent) {

    override fun createIntent(context: Context) = Intent(context, DamagesFormActivity::class.java).apply {
        putExtra(DamagesFormArgs.ARG_REPORT_ID, reportId)
        putExtra(DamagesFormArgs.ARG_OPENED_FOR_EDIT, false)
        putExtra(DamagesFormArgs.ARG_FINISH_AFTER_COMMIT, !shouldCloseParent)
    }

}