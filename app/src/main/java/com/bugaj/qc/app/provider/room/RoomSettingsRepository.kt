package com.bugaj.qc.app.provider.room

import androidx.lifecycle.LiveData
import com.bugaj.qc.app.service.SettingsRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class RoomSettingsRepository @Inject constructor(
    private val settingsDao: SettingsDao,
    private val ioDispatcher: CoroutineDispatcher
) : SettingsRepository {

    override fun observeSettings(): LiveData<List<SettingEntity>> {
        return settingsDao.observeSettings()
    }

    override fun observeSetting(name: String): LiveData<SettingEntity> {
        return settingsDao.observeSettingByName(name)
    }

    override suspend fun getSettings(): List<SettingEntity> = withContext(ioDispatcher) {
        settingsDao.getSettings()
    }

    override suspend fun getSetting(name: String): SettingEntity?  = withContext(ioDispatcher) {
        try {
            val setting = settingsDao.getSettingByName(name)
            if (setting != null) {
                return@withContext setting
            } else {
                return@withContext null
            }
        } catch (e: Exception) {
            return@withContext null
        }
    }

    override suspend fun saveSetting(settingEntity: SettingEntity)  = withContext(ioDispatcher) {
        settingsDao.insertSetting(settingEntity)
    }

    override fun observeSettingValue(name: String): LiveData<String> {
        return settingsDao.observeSettingValue(name)
    }
}