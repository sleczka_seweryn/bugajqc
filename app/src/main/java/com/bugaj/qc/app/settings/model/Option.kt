package com.bugaj.qc.app.settings.model

data class Option constructor(
    val labelResId: Int,
    val value: String
)