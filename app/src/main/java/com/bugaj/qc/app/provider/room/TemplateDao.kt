package com.bugaj.qc.app.provider.room

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface TemplateDao {

    @Query("SELECT * FROM templates ORDER BY `order` ASC")
    fun observeList(): LiveData<List<TemplateEntity>>

    @Query("SELECT * FROM templates WHERE id = :id")
    fun load(id: String): TemplateEntity?

    @Query("SELECT * FROM templates ORDER BY `order` ASC")
    suspend fun list(): List<TemplateEntity>

    @Query("SELECT templates.`order` FROM templates ORDER BY `order` DESC LIMIT 1")
    suspend fun getLastOrder(): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(template: TemplateEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertBlocking(templates: List<TemplateEntity>)

    @Update
    suspend fun update(template: TemplateEntity)

    @Query("SELECT * FROM templates WHERE labelPl = :labelPL")
    suspend fun findWhereLabelPl(labelPL: String): TemplateEntity?

    @Delete
    suspend fun delete(report: TemplateEntity)

    @Query("DELETE FROM templates WHERE id = :id")
    suspend fun delete(id: String)

}