package com.bugaj.qc.app.provider.app

import android.content.Context
import android.content.SharedPreferences
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Build
import com.bugaj.qc.app.BuildConfig
import com.bugaj.qc.app.R
import com.bugaj.qc.app.provider.room.SettingEntity
import com.bugaj.qc.app.provider.room.SettingsDao
import com.bugaj.qc.app.provider.room.TemplateDao
import com.bugaj.qc.app.provider.room.TemplateEntity
import com.bugaj.qc.app.service.SplashService
import com.bugaj.qc.app.settings.model.Settings
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.RuntimeException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class SplashServiceImpl @Inject constructor (
    @ApplicationContext private val context: Context,
    private val sharedPreferences: SharedPreferences,
    private val settingsDao: SettingsDao,
    private val templateDao: TemplateDao
) : SplashService {

    private val resPL = getResourceWithLocale(Locale("pl"))

    private val resEN = getResourceWithLocale(Locale.UK)

    override suspend fun loadApplication(): Boolean {
        return withContext(Dispatchers.IO) {
            if(sharedPreferences.getBoolean(KEY_SHOULD_RESTORE_DATABASE, true)) {
                sharedPreferences.edit().putBoolean(KEY_SHOULD_RESTORE_DATABASE, false).apply()

                settingsDao.insertBlocking(SettingEntity(Settings.SERVER_HOST.name, Settings.SERVER_HOST.defaultValue))
                settingsDao.insertBlocking(SettingEntity(Settings.SERVER_PORT.name, Settings.SERVER_PORT.defaultValue))
                settingsDao.insertBlocking(SettingEntity(Settings.FTP_USERNAME.name, Settings.FTP_USERNAME.defaultValue))
                settingsDao.insertBlocking(SettingEntity(Settings.FTP_PASSWORD.name, Settings.FTP_PASSWORD.defaultValue))
                settingsDao.insertBlocking(SettingEntity(Settings.IMAGE_RESOLUTION.name, Settings.IMAGE_RESOLUTION.defaultValue))

                templateDao.insertBlocking(arrayListOf<TemplateEntity>().apply {
                    add(
                        TemplateEntity(
                        labelPl = resPL.getString(R.string.report_param_mechanical_damage),
                        labelEn = resEN.getString(R.string.report_param_mechanical_damage),
                        order = 0
                    )
                    )
                    add(
                        TemplateEntity(
                        labelPl = resPL.getString(R.string.report_param_greening),
                        labelEn = resEN.getString(R.string.report_param_greening),
                        order = 1
                    )
                    )
                    add(
                        TemplateEntity(
                        labelPl = resPL.getString(R.string.report_param_growth_cracks),
                        labelEn = resEN.getString(R.string.report_param_growth_cracks),
                        order = 2
                    )
                    )
                    add(
                        TemplateEntity(
                        labelPl = resPL.getString(R.string.report_param_pest_damages),
                        labelEn = resEN.getString(R.string.report_param_pest_damages),
                        order = 3
                    )
                    )
                    add(
                        TemplateEntity(
                        labelPl = resPL.getString(R.string.report_param_common_scarb),
                        labelEn = resEN.getString(R.string.report_param_common_scarb),
                        order = 4
                    )
                    )
                    add(
                        TemplateEntity(
                        labelPl = resPL.getString(R.string.report_param_bruising),
                        labelEn = resEN.getString(R.string.report_param_bruising),
                        order = 5
                    )
                    )
                    add(
                        TemplateEntity(
                        labelPl = resPL.getString(R.string.report_param_internal),
                        labelEn = resEN.getString(R.string.report_param_internal),
                        order = 6
                    )
                    )
                    add(
                        TemplateEntity(
                        labelPl = resPL.getString(R.string.report_param_white_coating_damages),
                        labelEn = resEN.getString(R.string.report_param_white_coating_damages),
                        order = 7
                    )
                    )
                    add(
                        TemplateEntity(
                        labelPl = resPL.getString(R.string.report_param_rot),
                        labelEn = resEN.getString(R.string.report_param_rot),
                        order = 8
                    )
                    )
                    add(
                        TemplateEntity(
                        labelPl = resPL.getString(R.string.report_param_stones),
                        labelEn = resEN.getString(R.string.report_param_stones),
                        order = 9
                    )
                    )
                    add(
                        TemplateEntity(
                        labelPl = resPL.getString(R.string.report_param_oversize),
                        labelEn = resEN.getString(R.string.report_param_oversize),
                        order = 10
                    )
                    )
                    add(
                        TemplateEntity(
                        labelPl = resPL.getString(R.string.report_param_other),
                        labelEn = resEN.getString(R.string.report_param_other),
                        order = 11
                    )
                    )
                })
            }

            val lastVersionCode = sharedPreferences.getInt(KEY_LAST_VERSION_CODE, 0)

            if(lastVersionCode == 0 && BuildConfig.VERSION_CODE <= 6) {
                templateDao.findWhereLabelPl(resPL.getString(R.string.report_param_common_scarb))?.let {
                    it.labelEn = resEN.getString(R.string.report_param_common_scarb)
                    templateDao.update(it)
                }
                templateDao.findWhereLabelPl(resPL.getString(R.string.report_param_internal))?.let {
                    it.labelEn = resEN.getString(R.string.report_param_internal)
                    templateDao.update(it)
                }
            }

            if(lastVersionCode == 6 && BuildConfig.VERSION_CODE <= 7) {
                settingsDao.insertBlocking(SettingEntity(
                    Settings.IMAGE_RESOLUTION.name,
                    Settings.IMAGE_RESOLUTION.defaultValue
                ))
            }

            if(lastVersionCode != BuildConfig.VERSION_CODE && BuildConfig.VERSION_CODE <= 10) {
                settingsDao.insertBlocking(SettingEntity(Settings.SERVER_HOST.name, Settings.SERVER_HOST.defaultValue))
                settingsDao.insertBlocking(SettingEntity(Settings.SERVER_PORT.name, Settings.SERVER_PORT.defaultValue))
                settingsDao.insertBlocking(SettingEntity(Settings.FTP_USERNAME.name, Settings.FTP_USERNAME.defaultValue))
                settingsDao.insertBlocking(SettingEntity(Settings.FTP_PASSWORD.name, Settings.FTP_PASSWORD.defaultValue))
                settingsDao.insertBlocking(SettingEntity(Settings.STORE_IMAGES_IN_GALLERY.name, Settings.STORE_IMAGES_IN_GALLERY.defaultValue))
            }

            if(lastVersionCode != BuildConfig.VERSION_CODE) {
                sharedPreferences.edit()
                    .putInt(KEY_LAST_VERSION_CODE, BuildConfig.VERSION_CODE)
                    .apply()
            }

            Thread.sleep(600)

            return@withContext true
        }
    }

    private fun getResourceWithLocale(locale: Locale): Resources {
        var conf = context.resources.configuration
        conf = Configuration(conf)
        conf.setLocale(locale)
        val localizedContext = context.createConfigurationContext(conf)
        return localizedContext.resources
    }

    override fun getAppVersion(): String {
         return BuildConfig.VERSION_NAME
    }

    override fun checkLicense(): Boolean {
        if(DO_CHECK_LICENSE) {
            val format = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            val licenseDate = format.parse(LICENSE_DATE)
            return licenseDate?.after(Date()) ?: false
        }
        return true
    }

    companion object {

        const val LICENSE_DATE = "2020-10-15"

        const val DO_CHECK_LICENSE = false

        const val KEY_SHOULD_RESTORE_DATABASE = "com.bugaj.qc.SHOULD_RESTORE_DATABASE"

        const val KEY_LAST_VERSION_CODE = "com.bugaj.qc.LAST_VERSION_CODE"

    }

}