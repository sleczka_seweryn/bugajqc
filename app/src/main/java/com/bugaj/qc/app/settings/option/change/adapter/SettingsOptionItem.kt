package com.bugaj.qc.app.settings.option.change.adapter

data class SettingsOptionItem(
    val name: String,
    val value: String,
    val isSelected: Boolean
)