package com.bugaj.qc.app.provider.room

import androidx.room.Dao
import androidx.room.Query

@Dao
interface ReportWithImagesDao {

    @Query("SELECT * FROM reports WHERE id = :id")
    fun loadReportWithImages(id: String): ReportWithImages?

}