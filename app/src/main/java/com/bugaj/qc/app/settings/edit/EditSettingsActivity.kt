package com.bugaj.qc.app.settings.edit

import android.content.Context
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.bugaj.qc.app.R
import com.bugaj.qc.app.application.navigation.ActivityNavigateManager
import com.bugaj.qc.app.databinding.EditSettingsActivityBinding
import com.bugaj.qc.app.databinding.HostSettingsActivityBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class EditSettingsActivity : AppCompatActivity() {

    private lateinit var binding: EditSettingsActivityBinding

    private val viewModel by viewModels<EditSettingsViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(
            this, R.layout.edit_settings_activity)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        viewModel.navigateAction.observe(this) {
            it?.accept(ActivityNavigateManager(this))
        }

        viewModel.inputType.observe(this) {
            binding.input.inputType = it
        }

        viewModel.isLoading.observe(this, Observer {
            if(it == true) {
                hideKeyboard(this)
            }
        })
    }

    private fun hideKeyboard(context: Context) {
        val manager: InputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        manager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
    }

}