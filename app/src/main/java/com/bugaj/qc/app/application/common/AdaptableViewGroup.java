package com.bugaj.qc.app.application.common;

import android.view.View;
import android.view.ViewGroup;

public interface AdaptableViewGroup {

    View getView(int index);

    void addView(View itemView);

    void removeViews(int positionStart, int itemCount);

    void removeAllViews();

    int getChildCount();

    void removeView(View child);

    ViewGroup viewGroup();

}