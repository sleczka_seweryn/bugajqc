package com.bugaj.qc.app.admin

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.bugaj.qc.app.application.navigation.DialogNavigateManager
import com.bugaj.qc.app.application.navigation.DismissDialogAction
import com.bugaj.qc.app.databinding.AdminAuthDialogActivityBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AdminAuthDialogFragment : DialogFragment(), View.OnKeyListener {

    private val viewModel by viewModels<AdminAuthDialogViewModel>()

    private lateinit var binding: AdminAuthDialogActivityBinding

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let { activity ->

            val inflater = requireActivity().layoutInflater;
            binding = AdminAuthDialogActivityBinding.inflate(inflater)
            binding.lifecycleOwner = this
            binding.viewModel = viewModel
            binding.root.setOnKeyListener(this)
            showKeyboard(activity, binding.input)
            isCancelable = false

            binding.input.setOnEditorActionListener { _, actionId, _ ->
                if(actionId == EditorInfo.IME_ACTION_NEXT) {
                    viewModel.onClickSubmit()
                    return@setOnEditorActionListener true
                }
                false
            }

            binding.cancelButton.setOnClickListener {
                hideKeyboard(activity, binding.input)
                viewModel.onClickCancel()
            }

            viewModel.navigateAction.observe(this) {
                it?.accept(DialogNavigateManager(this, activity))
            }

            viewModel.isLoading.observe(this, Observer {
                if(it == true) {
                    hideKeyboard(activity, binding.input)
                } else {
                    showKeyboard(activity, binding.input)
                }
            })

            val builder = AlertDialog.Builder(activity)
            builder.setView(binding.root)
            builder.setCancelable(false)
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    private fun hideKeyboard(context: Context, editText: EditText) {
        val manager: InputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        manager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
    }

    private fun showKeyboard(context: Context, editText: EditText) {
        val manager: InputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        manager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
        editText.requestFocus()
    }

    override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            hideKeyboard(requireContext(), binding.input)
            viewModel.onClickCancel()
            true
        } else false
    }

}