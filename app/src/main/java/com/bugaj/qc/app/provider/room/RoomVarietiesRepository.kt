package com.bugaj.qc.app.provider.room

import com.bugaj.qc.app.service.VarietiesRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class RoomVarietiesRepository @Inject constructor(
    private val varietiesDao: VarietiesDao,
    private val ioDispatcher: CoroutineDispatcher
) : VarietiesRepository {

    override suspend fun load(name: String): VarietyEntity? = withContext(ioDispatcher) {
        varietiesDao.load(name)
    }

    override suspend fun list(): List<VarietyEntity> = withContext(ioDispatcher) {
        varietiesDao.list()
    }

    override suspend fun save(entity: VarietyEntity) = withContext(ioDispatcher) {
        varietiesDao.insert(entity)
    }
}