package com.bugaj.qc.app.report.images.navigation

import android.content.Context
import android.content.Intent
import com.bugaj.qc.app.application.navigation.StartActivityAction
import com.bugaj.qc.app.report.images.ReportAddImagesActivity

class DisplayReportAddImagesActivityAction constructor(
    private val reportId: String
) : StartActivityAction() {

    override fun createIntent(context: Context) = Intent(context, ReportAddImagesActivity::class.java).apply {
        putExtra(ReportAddImagesArgs.ARG_REPORT_ID, reportId)
    }

}