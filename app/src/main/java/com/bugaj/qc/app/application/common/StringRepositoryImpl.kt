package com.bugaj.qc.app.application.common

import android.content.Context

class StringRepositoryImpl constructor(
    val context: Context
) : StringRepository {

    override fun get(stringId: Int): String {
        return context.getString(stringId)
    }

}