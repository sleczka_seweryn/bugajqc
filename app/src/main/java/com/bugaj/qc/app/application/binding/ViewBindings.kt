package com.bugaj.qc.app.application.binding

import android.view.View
import androidx.databinding.BindingAdapter

@BindingAdapter("visibility")
fun setVisibility(view: View, value: Boolean?) {
    view.visibility = (if (value == true) View.VISIBLE else View.GONE)
}