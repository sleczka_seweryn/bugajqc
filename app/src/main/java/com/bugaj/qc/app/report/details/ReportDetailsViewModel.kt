package com.bugaj.qc.app.report.details

import javax.inject.Inject
import androidx.lifecycle.*
import androidx.lifecycle.Observer
import com.bugaj.qc.app.R
import com.bugaj.qc.app.application.arch.BaseViewModel
import com.bugaj.qc.app.application.common.StringRepository
import com.bugaj.qc.app.application.navigation.FinishActivityAction
import com.bugaj.qc.app.application.navigation.SharePdfFileAction
import com.bugaj.qc.app.application.navigation.ViewPdfFileAction
import com.bugaj.qc.app.provider.room.DamageEntity
import com.bugaj.qc.app.provider.room.DocumentEntity
import com.bugaj.qc.app.provider.room.ImageEntity
import com.bugaj.qc.app.provider.room.ReportEntity
import com.bugaj.qc.app.report.create.navigation.DisplayCreateReportAction
import com.bugaj.qc.app.report.create.samples.Samples
import com.bugaj.qc.app.report.damages.model.formatDamageValue
import com.bugaj.qc.app.report.damages.navigation.DisplayCreateDamagesFormAction
import com.bugaj.qc.app.report.damages.navigation.DisplayEditDamagesFormAction
import com.bugaj.qc.app.report.delete.navigation.DisplayReportDeleteDialogAction
import com.bugaj.qc.app.report.details.images.DisplayDeleteImageDialogAction
import com.bugaj.qc.app.report.details.navigation.ReportDetailsArgs
import com.bugaj.qc.app.report.details.params.DamageParamItem
import com.bugaj.qc.app.report.details.state.ReportState
import com.bugaj.qc.app.report.document.navigation.DisplayCreateDocumentDialogAction
import com.bugaj.qc.app.report.send.navigation.DisplayReportSendDialogAction
import com.bugaj.qc.app.service.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.io.File
import java.lang.IllegalArgumentException
import java.text.SimpleDateFormat
import java.util.*

@HiltViewModel
class ReportDetailsViewModel @Inject constructor (
    private val strings: StringRepository,
    private val reportsRepository: ReportsRepository,
    private val imagesRepository: ImagesRepository,
    private val damagesRepository: DamagesRepository,
    private val documentsRepository: DocumentsRepository,
    private val savedStateHandle: SavedStateHandle
) : BaseViewModel() {

    val reportId: String = savedStateHandle.get<String>(ReportDetailsArgs.ARG_REPORT_ID) ?: throw IllegalArgumentException("reportId not found")

    private var reportEntity: LiveData<ReportEntity?> = reportsRepository.observeReport(reportId)

    val deliveryNumber: LiveData<String?> = reportEntity.map { it?.deliveryNumber }

    val supplier: LiveData<String?> = reportEntity.map { it?.supplier }

    val variety: LiveData<String?> = reportEntity.map { it?.variety }

    val date: LiveData<String?> = reportEntity.map { it?.let { item -> SimpleDateFormat("dd.MM.YYYY", Locale.getDefault() ).format(item.date) } }

    val size: LiveData<String?> = reportEntity.map { it?.displaySize() }

    val controller: LiveData<String?> = reportEntity.map { it?.controller }

    val temperature: LiveData<String?> = reportEntity.map { it?.displayTemp() }

    val samples: LiveData<String?> = reportEntity.map { it?.let { item -> strings[Samples.valueOf(item.controlledSamples).titleRasId] } }

    val isReportFinished: LiveData<Boolean> = reportEntity.map { false }

    private val damages: LiveData<List<DamageEntity>> = damagesRepository.observeWhereReportId(reportId)

    val images: LiveData<List<ImageEntity>> = imagesRepository.observeReportImages(reportId)

    val damageItems: LiveData<List<DamageParamItem>> = MediatorLiveData<List<DamageParamItem>>().apply {
        addSource(reportEntity, Observer { value = convertDamageItems() })
        addSource(damages, Observer { value = convertDamageItems() })
    }

    val document: LiveData<DocumentEntity> = documentsRepository.observeDocument(reportId)

    val isDamageFormFilled: LiveData<Boolean> = damages.map { it.isNotEmpty() }

    val isDamageFormEmpty: LiveData<Boolean> = damageItems.map { it.isEmpty() }

    val arePhotosFilled: LiveData<Boolean> = images.map { it.size >= 3 }

    val canAddMorePhotos: LiveData<Boolean> = images.map { it.size < 4 }

    val reportState: LiveData<ReportState> = MediatorLiveData<ReportState>().apply {
        addSource(reportEntity, Observer { value = resolveReportState() })
        addSource(isDamageFormFilled, Observer { value = resolveReportState() })
        addSource(arePhotosFilled, Observer { value = resolveReportState() })
    }

    val canEdit: LiveData<Boolean> = MediatorLiveData<Boolean>().apply {
        value = true
        addSource(document, Observer { value = document.value == null })
    }

    init {
        reportEntity.observeForever{
            if(it == null) {
                navigate(FinishActivityAction())
            }
        }
    }

    private fun convertDamageItems(): List<DamageParamItem> {
        val report = reportEntity.value
        val list = damages.value
        return if(report != null && list != null) {
            val arrayList = arrayListOf<DamageParamItem>()
            val damageParams = list.filter { it.value ?: 0f > 0 }
                .map { DamageParamItem (
                    label = it.labelPl,
                    value = formatDamageValue(
                        value = it.value?:0f,
                        total = report.getSamplesCategory().weight
                    )
                ) }
            arrayList.addAll(damageParams)

            val total: Float = list.map { it.value ?: 0f }.sum()
            if(damageParams.size > 1) {
                arrayList.add( DamageParamItem(
                    label = strings[R.string.total_damages_label],
                    bold = true,
                    value = formatDamageValue(
                        value = total,
                        total = report.getSamplesCategory().weight,
                    )
                ))
            }

            arrayList
        } else {
            arrayListOf()
        }
    }

    fun onClickChangeBasicData() {
        navigate(DisplayCreateReportAction(
            reportId = reportId
        ))
    }

    fun onClickChangeDamages() {
        navigate(DisplayEditDamagesFormAction(
            reportId = reportId
        ))
    }

    fun onClickFillDamages() {
        navigate(DisplayCreateDamagesFormAction(
            reportId = reportId,
            shouldCloseParent = false
        ))
    }

    fun onClickCreateDocument() {
        navigate(DisplayCreateDocumentDialogAction(
            reportId = reportId
        ))
    }

    fun onClickShare() {
        document.value?.let {
            navigate(SharePdfFileAction(File(it.documentUrl)))
        }
    }

    fun onClickSend() {
        navigate(DisplayReportSendDialogAction(
            reportId = reportId
        ))
    }

    fun onClickView() {
        document.value?.let {
            navigate(ViewPdfFileAction(File(it.documentUrl)))
        }
    }

    fun onClickDelete() {
        navigate(DisplayReportDeleteDialogAction(
            reportId = reportId
        ))
    }

    fun onClickBackButton() {
        navigate(FinishActivityAction())
    }

    fun onSaveImageEntity(imageEntity: ImageEntity) {
        viewModelScope.launch {
            showLoader()
            imagesRepository.save(imageEntity)
            hideLoader()
        }
    }

    fun onClickRemoveImage(position: Int) {
        images.value?.get(position)?.let { imageEntity ->
            navigate(DisplayDeleteImageDialogAction(
                imageId = imageEntity.id
            ))
        }
    }

    private fun resolveReportState(): ReportState {
        val report = reportEntity.value
        val damageFormFilled = isDamageFormFilled.value ?: false
        val photosFilled = arePhotosFilled.value ?: false
        return if(report == null || !damageFormFilled) {
            ReportState.DAMAGE_REPORT_NOT_FILLED
        } else if(!photosFilled) {
            ReportState.PHOTOS_NOT_ATTACHED
        } else if(!report.isDocumentCreated) {
            ReportState.DOCUMENT_NOT_CREATED
        } else if(!report.isDocumentSend) {
            ReportState.DOCUMENT_NOT_SEND
        } else {
            ReportState.DOCUMENT_SEND
        }
    }

    fun isDocumentCreated(): Boolean {
        return document.value != null
    }

}