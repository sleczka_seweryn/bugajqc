package com.bugaj.qc.app.settings.edit.navigation

import android.content.Context
import android.content.Intent
import com.bugaj.qc.app.application.navigation.StartActivityAction
import com.bugaj.qc.app.settings.edit.EditSettingsActivity
import com.bugaj.qc.app.settings.model.Settings

class DisplayEditSettingsAction constructor(
    private val settingType: Settings
) : StartActivityAction(closeParent = false) {

    override fun createIntent(context: Context): Intent = Intent(context, EditSettingsActivity::class.java).apply {
        putExtra(DisplayEditSettingsArgs.SettingTypeArg, settingType)
    }

}