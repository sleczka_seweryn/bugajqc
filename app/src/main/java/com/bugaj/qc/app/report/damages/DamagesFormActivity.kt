package com.bugaj.qc.app.report.damages

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.inputmethod.EditorInfo
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.bugaj.qc.app.R
import com.bugaj.qc.app.application.navigation.ActivityNavigateManager
import com.bugaj.qc.app.application.navigation.DialogResultHandler
import com.bugaj.qc.app.application.navigation.RequestCodes
import com.bugaj.qc.app.databinding.DamagesFormActivityBinding
import com.bugaj.qc.app.loading.navigation.DismissLoadingDialogAction
import com.bugaj.qc.app.loading.navigation.DisplayLoadingDialogAction
import com.bugaj.qc.app.report.damages.adapter.DamageFormAdapter
import com.bugaj.qc.app.report.damages.navigation.DamagesFormArgs
import dagger.hilt.android.AndroidEntryPoint
import java.lang.IllegalStateException

@AndroidEntryPoint
class DamagesFormActivity : AppCompatActivity(), DialogResultHandler {

    private lateinit var binding: DamagesFormActivityBinding

    private val viewModel by viewModels<DamagesFormViewModel>()

    private lateinit var adapter: DamageFormAdapter

    private lateinit var navigateManager: ActivityNavigateManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(
            this, R.layout.damages_form_activity)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        navigateManager = ActivityNavigateManager(this)

        viewModel.navigateAction.observe(this) {
            it?.accept(navigateManager)
        }

        adapter = DamageFormAdapter(viewModel)
        binding.damageAdapterForm.adapter = adapter

        viewModel.formFields.observe(this, Observer {
            adapter.submitList(it)
        })

        viewModel.isLoading.observe(this, Observer {
            if(it == true) {
                viewModel.navigate(DisplayLoadingDialogAction())
            } else {
                viewModel.navigate(DismissLoadingDialogAction())
            }
        })
    }

    override fun onDialogResult(requestCode: Int, data: Intent) {
        if(requestCode == RequestCodes.EDIT_DAMAGE_FIELD) {
            val fieldId = data.getStringExtra(DamagesFormArgs.ARG_FIELD_ID) ?: throw IllegalStateException("ARG_FIELD_ID not found")
            val fieldValue = data.getSerializableExtra(DamagesFormArgs.ARG_ACTUAL_VALUE) as? Float
            viewModel.onValueChanged(fieldId, fieldValue)
        }
    }

    override fun onBackPressed() {
        viewModel.onClickBack()
    }

}