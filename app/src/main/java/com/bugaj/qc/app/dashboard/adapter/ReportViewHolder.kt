package com.bugaj.qc.app.dashboard.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bugaj.qc.app.R
import com.bugaj.qc.app.dashboard.DashboardViewModel
import com.bugaj.qc.app.databinding.ReportItemBinding

class ReportViewHolder private constructor(
    val binding: ReportItemBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(viewModel: DashboardViewModel, item: ReportItem) {

        val context = binding.root.context

        binding.viewModel = viewModel
        binding.item = item
        binding.executePendingBindings()

        when {
            item.state.isDamageFormComplete && item.state.isDocumentSend -> {
                binding.actionButton.visibility = View.VISIBLE
                binding.actionButton.text = context.getString(R.string.view)
                binding.stateIconError.visibility = View.GONE
                binding.stateIconSuccess.visibility = View.VISIBLE
                binding.stateLabel.text = context.getString(R.string.raport_state_finished)

                binding.actionButton.setOnClickListener {
                    viewModel.onClickView(item.id)
                }

                binding.actionButton.setCompoundDrawablesWithIntrinsicBounds(null,null,null,null)
            }
           item.state.isDamageFormComplete && item.state.isDocumentGenerated -> {
               binding.actionButton.visibility = View.VISIBLE
               binding.actionButton.text = context.getString(R.string.send)
               binding.stateIconError.visibility = View.VISIBLE
               binding.stateIconError.setImageResource(R.drawable.ic_sync_problem_24)
               binding.stateIconSuccess.visibility = View.GONE
               binding.stateLabel.text = context.getString(R.string.raport_state_not_send)

               binding.actionButton.setOnClickListener {
                   viewModel.onClickSend(item.id)
               }

               val drawable = ContextCompat.getDrawable(context, R.drawable.ic_round_cloud_upload_24w)
               binding.actionButton.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null)

            }
            item.state.isDamageFormComplete -> {
                binding.actionButton.visibility = View.GONE
                binding.stateIconError.visibility = View.VISIBLE
                binding.stateIconError.setImageResource(R.drawable.ic_round_note_add_24w)
                binding.stateIconSuccess.visibility = View.GONE
                binding.stateLabel.text = context.getString(R.string.generate)
            }

        }

    }

    companion object {

        fun from(parent: ViewGroup): ReportViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = ReportItemBinding.inflate(layoutInflater, parent, false)
            return ReportViewHolder(binding)
        }

    }

}