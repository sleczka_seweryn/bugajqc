package com.bugaj.qc.app.report.details.params

import androidx.recyclerview.widget.DiffUtil

class DamageParamDiffCallback : DiffUtil.ItemCallback<DamageParamItem>() {

    override fun areItemsTheSame(oldItem: DamageParamItem, newItem: DamageParamItem): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: DamageParamItem, newItem: DamageParamItem): Boolean {
        return oldItem == newItem
    }

}