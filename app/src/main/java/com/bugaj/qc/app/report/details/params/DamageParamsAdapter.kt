package com.bugaj.qc.app.report.details.params

import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.bugaj.qc.app.report.details.ReportDetailsViewModel

class DamageParamsAdapter constructor(
    private val viewModel: ReportDetailsViewModel
) : ListAdapter<DamageParamItem, DamageParamsViewHolder>(DamageParamDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DamageParamsViewHolder {
        return DamageParamsViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: DamageParamsViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(viewModel, item)
    }
}