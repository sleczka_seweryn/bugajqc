package com.bugaj.qc.app.report.create.samples

import com.bugaj.qc.app.R

enum class Samples constructor(
    val titleRasId: Int,
    val weight: Float,
    val number: Int
) {

    Category1(
        titleRasId = R.string.controlled_samples_category_1,
        weight = 25f,
        number = 1
    ),

    Category2(
        titleRasId = R.string.controlled_samples_category_2,
        weight = 50f,
        number = 2
    ),

    Category3(
        titleRasId = R.string.controlled_samples_category_3,
        weight = 75f,
        number = 3
    ),

    Category4(
        titleRasId = R.string.controlled_samples_category_4,
        weight = 100f,
        number = 4
    )

}