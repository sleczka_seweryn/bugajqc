package com.bugaj.qc.app.provider.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

/**
 * Note that exportSchema should be true in production databases.
 */
@Database(entities = [
    SettingEntity::class,
    ReportEntity::class,
    ImageEntity::class,
    SupplierEntity::class,
    VarietyEntity::class,
    TemplateEntity::class,
    DamageEntity::class,
    DocumentEntity::class,
    ControllerEntity::class
], version = 2, exportSchema = false)
@TypeConverters(Converters::class)
abstract class BugajDatabase : RoomDatabase() {

    abstract fun settingsDao(): SettingsDao

    abstract fun reportsDao(): ReportsDao

    abstract fun imagesDao(): ImagesDao

    abstract fun suppliersDao(): SuppliersDao

    abstract fun varietiesDao(): VarietiesDao

    abstract fun templateDao(): TemplateDao

    abstract fun reportWithImagesDao(): ReportWithImagesDao

    abstract fun damagesDao(): DamagesDao

    abstract fun documentsDao(): DocumentsDao

    abstract fun controllerDao(): ControllerDao

}