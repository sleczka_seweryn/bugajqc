package com.bugaj.qc.app.application.injection

import android.content.Context
import android.content.SharedPreferences
import androidx.room.Room
import com.bugaj.qc.app.application.common.StringRepository
import com.bugaj.qc.app.application.common.StringRepositoryImpl
import com.bugaj.qc.app.provider.room.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppModule {

    @Singleton
    @Provides
    fun provideStringRepository(@ApplicationContext context: Context): StringRepository {
        return StringRepositoryImpl(context)
    }

    @Singleton
    @Provides
    fun provideSharedPreferences(@ApplicationContext context: Context): SharedPreferences {
        return context.getSharedPreferences("BugajQC-config", Context.MODE_PRIVATE)
    }

    @Singleton
    @Provides
    fun provideIoDispatcher(): CoroutineDispatcher = Dispatchers.IO

    @Singleton
    @Provides
    fun provideDataBase(@ApplicationContext context: Context, callback: BugajDatabaseCallback): BugajDatabase {
        val database = Room.databaseBuilder(
            context.applicationContext,
            BugajDatabase::class.java,
            "BugajQC-reports.db"
        ).fallbackToDestructiveMigration()
            .build()
        return database;
    }

    @Provides
    fun provideSettingsDao(database: BugajDatabase): SettingsDao {
        return database.settingsDao()
    }

    @Provides
    fun provideReportsDao(database: BugajDatabase): ReportsDao {
        return database.reportsDao()
    }

    @Provides
    fun provideImagesDao(database: BugajDatabase): ImagesDao {
        return database.imagesDao()
    }

    @Provides
    fun provideSuppliersDao(database: BugajDatabase): SuppliersDao {
        return database.suppliersDao()
    }

    @Provides
    fun provideVarietiesDao(database: BugajDatabase): VarietiesDao {
        return database.varietiesDao()
    }

    @Provides
    fun provideTemplateDao(database: BugajDatabase): TemplateDao {
        return database.templateDao()
    }

    @Provides
    fun provideReportWithImagesDao(database: BugajDatabase): ReportWithImagesDao {
        return database.reportWithImagesDao()
    }

    @Provides
    fun provideDamagesDao(database: BugajDatabase): DamagesDao {
        return database.damagesDao()
    }

    @Provides
    fun provideDocumentsDao(database: BugajDatabase): DocumentsDao {
        return database.documentsDao()
    }

    @Provides
    fun provideControllerDao(database: BugajDatabase): ControllerDao {
        return database.controllerDao()
    }

}