package com.bugaj.qc.app.application.validation

import android.content.Context
import com.bugaj.qc.app.application.common.StringRepository

interface Validator<VALUE> {

    fun validate(strings: StringRepository, value: VALUE?): String?

}