package com.bugaj.qc.app.application.navigation

import com.bugaj.qc.app.loading.navigation.DismissLoadingDialogAction
import com.bugaj.qc.app.loading.navigation.DisplayLoadingDialogAction

interface NavigateManager {

    fun visit(action: StartActivityAction)

    fun visit(action: FinishActivityAction)

    fun visit(action: DisplayExceptionAction)

    fun visit(action: FragmentTransactionAction)

    fun visit(action: DisplayDialogAction)

    fun visit(action: DismissDialogAction)

    fun visit(action: DisplayAlertDialogAction)

    fun visit(action: DialogResultAction)

    fun visit(action: DisplayLoadingDialogAction)

    fun visit(action: DismissLoadingDialogAction)

}