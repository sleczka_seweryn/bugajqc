package com.bugaj.qc.app.templates.list.adapter

data class TemplateItem constructor(
    val fieldId: String,
    val labelPl: String,
    val labelEn: String
)