package com.bugaj.qc.app.provider.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "settings")
class SettingEntity constructor(
    @PrimaryKey @ColumnInfo(name = "name") var name: String,
    @ColumnInfo(name = "value") var value: String
)