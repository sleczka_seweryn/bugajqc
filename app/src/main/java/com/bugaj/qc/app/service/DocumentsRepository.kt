package com.bugaj.qc.app.service

import androidx.lifecycle.LiveData
import com.bugaj.qc.app.provider.room.DocumentEntity

interface DocumentsRepository {

    fun observeDocument(reportId: String): LiveData<DocumentEntity>

    suspend fun find(id: String): DocumentEntity?

    suspend fun findByReportId(reportId: String): DocumentEntity?

    suspend fun insert(entity: DocumentEntity)

    suspend fun delete(id: String)

}