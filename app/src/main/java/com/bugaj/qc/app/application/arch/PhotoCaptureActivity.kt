package com.bugaj.qc.app.application.arch

import android.Manifest
import android.content.ContentResolver
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.provider.MediaStore.Images
import android.util.Log
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.lifecycle.lifecycleScope
import com.bugaj.qc.app.R
import com.bugaj.qc.app.provider.room.ImageEntity
import com.bugaj.qc.app.service.SettingsRepository
import com.bugaj.qc.app.settings.model.Settings
import kotlinx.coroutines.launch
import java.io.*
import java.util.*
import javax.inject.Inject


abstract class PhotoCaptureActivity : AppCompatActivity(), MediaScannerConnection.MediaScannerConnectionClient  {

    private var currentImage: ImageEntity? = null

    private lateinit var mediaScannerConnection: MediaScannerConnection

    @Inject lateinit var settingsRepository: SettingsRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mediaScannerConnection = MediaScannerConnection(baseContext, this)
        mediaScannerConnection.connect()
    }

    override fun onDestroy() {
        super.onDestroy()
        mediaScannerConnection.disconnect()
    }

    fun onClickAddPhoto() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkCameraPermissions()
        } else {
            displayCameraActivity()
        }
    }

    fun onClickPickFromGallery() {
        try {
            createImageFile()
            displayPickPhotoActivity()
        } catch (ex: IOException) {
            ex.printStackTrace()
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun checkCameraPermissions() {
        if (ContextCompat.checkSelfPermission(
                baseContext,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.CAMERA),
                REQUEST_CODE_CAMERA_PERMISSION
            );
        } else {
            displayCameraActivity()
        }
    }

    private fun displayCameraActivity() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    ex.printStackTrace()
                    null
                }
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                        this,
                        "com.bugaj.qc.app.fileprovider",
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(
                        takePictureIntent,
                        REQUEST_CODE_CAMERA_TAKE_PHOTO
                    )
                }
            }
        }
    }

    private fun displayPickPhotoActivity() {
        val pickPhoto = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(pickPhoto, REQUEST_CODE_PICK_PHOTO_FROM_GALLERY)
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        val imageId: String = UUID.randomUUID().toString()
        val storageDir: File? = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            imageId, ".jpg", storageDir
        ).apply {
            currentImage = ImageEntity(
                id = imageId,
                reportId = getReportId(),
                imageUrl = absolutePath
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CODE_CAMERA_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                displayCameraActivity()
            } else {
                val message = baseContext.getString(R.string.cammera_permission_demied)
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_CODE_CAMERA_TAKE_PHOTO) {
            if (resultCode == RESULT_OK) {
                currentImage?.let { imageEntity ->
                    lifecycleScope.launch {
                        if(settingsRepository.getSetting(Settings.STORE_IMAGES_IN_GALLERY.name)?.value == "TRUE") {
                            saveImage(File(imageEntity.imageUrl), imageEntity.id)
                        }
                    }
                    onSaveImageEntity(imageEntity)
                }
            }
            currentImage = null
        } else if (requestCode == REQUEST_CODE_PICK_PHOTO_FROM_GALLERY) {
            if (resultCode == RESULT_OK && data != null) {
                data.data?.let { selectedImageUri ->
                    try {
                        contentResolver.openFileDescriptor(selectedImageUri, "r")
                            ?.use { descriptor ->
                                val fd: FileDescriptor = descriptor.fileDescriptor
                                FileInputStream(fd).use { input ->
                                    FileOutputStream(currentImage?.imageUrl).use { output ->
                                        var read: Int = 0
                                        val bytes = ByteArray(4096)
                                        while (input.read(bytes).also { read = it } != -1) {
                                            output.write(bytes, 0, read)
                                        }
                                        currentImage?.let { onSaveImageEntity(it) }
                                    }
                                }
                            }
                    } catch (ex: FileNotFoundException) {
                        ex.printStackTrace()
                    }
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onScanCompleted(path: String?, uri: Uri?) {
        Log.d("onScanCompleted", "path= $path")
    }

    override fun onMediaScannerConnected() {
        Log.d("onMediaScannerConnected", "onMediaScannerConnected")
    }

    abstract fun onSaveImageEntity(imageEntity: ImageEntity)

    abstract fun getReportId(): String

    companion object {

        const val REQUEST_CODE_CAMERA_PERMISSION: Int = 1111

        const val REQUEST_CODE_CAMERA_TAKE_PHOTO: Int = 2222

        const val REQUEST_CODE_PICK_PHOTO_FROM_GALLERY: Int = 3333

    }

    private fun saveImage(file: File, @NonNull name: String) {
        val fos: OutputStream?
        fos = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val resolver: ContentResolver = contentResolver
            val contentValues = ContentValues()
            contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, "$name.jpg")
            contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpg")
            contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES)
            val imageUri: Uri? = resolver.insert(Images.Media.EXTERNAL_CONTENT_URI, contentValues)
            resolver.openOutputStream(Objects.requireNonNull(imageUri!!))
        } else {
            val imagesDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString()
            val image = File(imagesDir, "$name.jpg")
            FileOutputStream(image)
        }
        copyFile(file, fos)
        Objects.requireNonNull(fos)?.close()
    }

    private fun copyFile(srcFile: File?, dstFile: OutputStream?) {
        FileInputStream(srcFile).use { fis ->
            dstFile.use { fos ->
                var len: Int
                val buffer = ByteArray(1024)
                while (fis.read(buffer).also { len = it } > 0) {
                    fos?.write(buffer, 0, len)
                }
            }
        }
    }

}