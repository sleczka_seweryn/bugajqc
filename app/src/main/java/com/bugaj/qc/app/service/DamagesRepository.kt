package com.bugaj.qc.app.service

import androidx.lifecycle.LiveData
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.bugaj.qc.app.provider.room.DamageEntity
import com.bugaj.qc.app.provider.room.ImageEntity

interface DamagesRepository {

    suspend fun find(id: String): DamageEntity?

    suspend fun listByReportId(reportId: String): List<DamageEntity>

    fun observeWhereReportId(reportId: String): LiveData<List<DamageEntity>>

    suspend fun insert(image: DamageEntity)

    suspend fun delete(id: String)

    suspend fun insertAll(dataList: List<DamageEntity>)

    suspend fun updateAll(dataList: List<DamageEntity>)

}