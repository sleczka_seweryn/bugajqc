package com.bugaj.qc.app.dashboard.adapter

import androidx.recyclerview.widget.DiffUtil

class ReportDiffCallback : DiffUtil.ItemCallback<ReportItem>() {

    override fun areItemsTheSame(oldItem: ReportItem, newItem: ReportItem): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: ReportItem, newItem: ReportItem): Boolean {
        return oldItem == newItem
    }

}