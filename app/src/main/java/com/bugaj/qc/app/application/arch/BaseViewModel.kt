package com.bugaj.qc.app.application.arch

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bugaj.qc.app.application.navigation.NavigateAction

open class BaseViewModel : ViewModel() {

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

    private val _navigateAction = MutableLiveData<NavigateAction>()
    val navigateAction: LiveData<NavigateAction> = _navigateAction

    fun navigate(action: NavigateAction) {
        _navigateAction.value = action
    }

    fun showLoader() {
        _isLoading.value = true
    }

    fun hideLoader() {
        _isLoading.value = false
    }

}