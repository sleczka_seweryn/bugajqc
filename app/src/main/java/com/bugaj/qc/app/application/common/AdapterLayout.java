package com.bugaj.qc.app.application.common;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Provides access to some of the public API of AdapterLayout in a way that will be available for any given
 * AdapterLayout
 */
public class AdapterLayout {

    public static int getAdapterPosition(RecyclerView.ViewHolder viewHolder) {
        return getPosition(viewHolder);
    }

    public static int getLayoutPosition(RecyclerView.ViewHolder viewHolder) {
        return getPosition(viewHolder);
    }

    private static int getPosition(RecyclerView.ViewHolder viewHolder) {
        return (int) viewHolder.itemView.getTag(AdapterLayoutConstants.adapter_layout_list_position);
    }
}
