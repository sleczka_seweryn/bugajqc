package com.bugaj.qc.app.report.damages.adapter

import androidx.recyclerview.widget.DiffUtil

class DamageFormFieldDiffCallback : DiffUtil.ItemCallback<DamageFormField>() {

    override fun areItemsTheSame(oldItem: DamageFormField, newItem: DamageFormField): Boolean {
        return oldItem.fieldId == newItem.fieldId
    }

    override fun areContentsTheSame(oldItem: DamageFormField, newItem: DamageFormField): Boolean {
        return oldItem.getValue() == newItem.getValue()
    }

}