package com.bugaj.qc.app.loading.navigation

import com.bugaj.qc.app.application.navigation.NavigateAction
import com.bugaj.qc.app.application.navigation.NavigateManager

class DisplayLoadingDialogAction constructor() : NavigateAction {

    override fun accept(navigation: NavigateManager) {
        navigation.visit(this)
    }
}